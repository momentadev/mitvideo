var user = require('../../app/controllers/user.server.controller'),
    passport = require('passport');

module.exports = function(app) {
    app.route('/signin')
        .get(user.renderSignin)
        .post(passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/signin',
            failureFlash: true
        }));
    app.route('/signup')
        .get(user.renderSignup)
        .post(user.signup);
    app.get('/signout', user.signout);
    app.route('/forgot')
        .get(user.renderForgot)
        .post(user.forgot);
    app.route('/reset/:token')
        .get(user.renderReset)
        .post(user.reset);
    app.route('/uploadFiles')
         .post(user.uploadFiles);
    app.route('/uploadSharedFiles')
         .post(user.uploadSharedFiles); 
    app.route('/checkusername')
         .post(user.checkUsername);
    app.route('/checkemailid')
         .post(user.checkEmailid);                           
};
