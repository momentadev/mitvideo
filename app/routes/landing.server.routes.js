var landing = require('../controllers/landing.server.controller');


module.exports = function(app) {
    app.route('/landing')
    	.get(landing.renderLanding);
};