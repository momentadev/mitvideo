var index = require('../controllers/index.server.controller');

module.exports = function(app,io,socket) {
    app.route('/')
    	.get(index.renderIndex);
};