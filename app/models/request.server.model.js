var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var requestSchema=new Schema({
	sender:{
		type : Schema.ObjectId,
		ref: 'User'
	},
	receiver:{
		type : Schema.ObjectId,
		ref: 'User'
	}
})
mongoose.model('request',requestSchema); 