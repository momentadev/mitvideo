var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var circleSchema=new Schema({
	creator:{
		type : Schema.ObjectId,
		ref: 'User'
	},
	circleName:{
		type: String,
    	default: '',
    	trim: true
	},
	members:[{
		id:{
			type : Schema.ObjectId,
			ref:'User'
		}
	}]
})
mongoose.model('circles',circleSchema); 