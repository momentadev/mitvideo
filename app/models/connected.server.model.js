var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var connectedSchema=new Schema({
	socketId : String,
	clientId : {
		type : Schema.ObjectId,
		ref  : 'User'
	}
})
mongoose.model('connect',connectedSchema); 