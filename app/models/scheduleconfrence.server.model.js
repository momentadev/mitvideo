var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scheduleConfrenceSchema = mongoose.Schema({
		creator		 : String,
 		createddate  : Date,
		date         : Date,
      	circleid     : String,
		subject 	 : String,
	    members      :[{
						id:{
							type : Schema.ObjectId,
							ref:'User'
						}
					 }],
        eventSatus	 : {
					        type: String, 
					        default: "" 
					    }
});
mongoose.model('confrenceschedule', scheduleConfrenceSchema);
