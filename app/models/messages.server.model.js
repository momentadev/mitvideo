var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var messageSchema=new Schema({
	sender : {
		type : Schema.ObjectId,
		ref  : 'User'
	},
	senderName : String,
	senderPic : String,
	receiver : {
		type : Schema.ObjectId,
		ref  : 'User'
	},
	message : {
		type : String
	},
	date : {
		type : Date,
		default : Date.now
	},
	read : [{user : {
		type: Schema.ObjectId,
		ref: 'User'
	}
	}]
})
mongoose.model('messages',messageSchema); 