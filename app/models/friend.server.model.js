var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var friendSchema=new Schema({
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	friends:[{ id: {
		type: Schema.ObjectId,
		ref: 'User'
	}}]
})
mongoose.model('friends',friendSchema);