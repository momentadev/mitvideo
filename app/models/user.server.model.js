var mongoose = require('mongoose'),
  crypto = require('crypto'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
      type: String,
      unique: true,
      required: 'E-Mail is required',
      match: [/.+\@.+\..+/, "Please fill a valid e-mail address"]
    },
    username: {
      type: String,
      unique: true,
      required: 'Username is required',
      trim: true
    },
    password: {
      type: String,
      validate: [
      function(password) {
          return password && password.length > 6;
        }, 'Password should be longer'
      ]
    },
    companyName:{
        type: String, 
        default: "" 
    },
    designation:{
        type: String, 
        default: "" 
    },
    aboutMe:{
        type: String, 
        default: "" 
    },
    education:[],
    experience:[],
    projects:[
        {project:{
            type: String
        }}
    ],
    mobileNumber:{
        type: String, 
        default: "" ,
        match: [/[0-9]+/]
    },
    profilePicture:{
        type: String, 
        default:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACV8SURBVHja7Z2HXxtZsoX339+3M+OcIxiwyQKRM5icwWSDyBmTc5j77impRQOSkNTdUks65/3qMTvGMwP0/Vx1uqruf/7999+3OjoYDAbD7fEf/f+GFUVRVAoIwOrgt4GiKAKLoiiKwKIoisCiKIoisCiKoggsiqIILIqiKAKLoh7V9fW1urq6uhMURWBRCYHP5eWlOjs7U4eHh2p3d1etr2+o5eUV5fPNq+npaTU2Pq5GRkfVwOCgGhgYVF1d3aqjo1N1dnZJtLd3qEH9axsbm0o/r/ymElgUFZ0ADGQ8ANHFxYU6Pj4WCAEmy8vLam5uTk1NTQUANCDAaWlpVQ0Njaq+vkHV1dWrmppaVV1do6qqqlVFRaUq93qVt6JC/hpRWVllikqJCv3r+Pyenh61ubnJHwSBRVGPC9nS6uqqzniGVHNzs0CotrZOIFRVVSVgEcjov0YAMoATfv1B1N6LUJ9TUxOMqupqARv+uT09vQJJisCiMgxA5ri5uYnq952enkqm8+vXL/XzZ7uGFrKmap1B1alaI2ojR7SwqjZHIDPzeiskA+vr69cZ3h5/kAQWlW5CKYcSbmN9XbyjgYEB1d3dLZ6RP7oEAPCLRkZG1cTkpPhMa2trandvV3yoUAY4QLe2uqb6+wcEVABQ1LCqiQ1W5jDAhc+HD3Z+fs4fMoFFpbLfBECtrKyocX2gAaTGxkZVD6gES627oDDDQMo5/Tm1dbXiPeH3wgwfGh5Ws7OzAj1kWENDQwIrmON2ZlaPAcsIZFpl5eWquaVFrehylSKwqJQp8a7Uzs6OBsqc6u3tFZ8pWK7pj/X19YFoeDTq9OchzBAym+XwqqrEp6oSf0ng5gSsqiMH/t3ib+lsCzEyMiLZIEVgUS7U+fmZ2tzc0lnUhGRAgA0ABVDBU2poaHgQ0cJKoi5U1AXDbs8qHlhJaJBKtlVWLm8m8T2hCCzKFZnUpdra3lZjY2O6FGsPvqnDR2RQoSBlN6xq44GVBc8qLKyqHkZ5uVeg7fP5+LAQWFSyPKk/f/6Id9TR0SFg8mdR9dLn5I8Ge2AVbVZVl1i/KhpYGSE9Xhpc8NxQKlMEFpUAnZycqIWFBfGkYHzXBLyoRoFQo/2wSnYZWBNbGRgRWpWVylNWprq7e6TRlSKwKAeEfiiY5790ydfa2nrHjwK0EGkJq2p7YGU0rkpUVimPp0z6xfDWlCKwKJuELAAjLn19fYEu8kA2FYBUMmHlZs8qEqyMALRgxh8dEVoEFmW57Jub86n2jo4gIMzZlOOwqksUrGqSAisztNrafqqLc5aHBBYVs7DBYHJy8k7ZFwpSaV0G2uhZRYKVGVrwtLi2hsCiotTR0ZEan5hQTU1N0niJA1xXFxlY9KyswwoBI76ktFQNj4zwQSSwqMigOlZjY+MBqNTr8qRNdff0yOH5/fu3+FcTE/6MKx0N9mSUgfdhhcDWB7w9XFxc5ENJYFH3hbd+WFyHbnT0UWFFy97e3p0REmRdKA9bWlqch1WGeFahYCVRUSkd8fh3HRwc8AElsKj7wMI2gVDrWwAtLL2TuT99mNO+dSEJwLoPKyNKSz2qs6s76rU6FIGV0RBbWlqSshCG+/23gvSs7POsQsEK4fVWigmPxlyKwKLCCGuF+/v7BRD19XVpa7BXJ6ApNF5YGYHSsElnt6HeGmIVNDJj9McZcXPDvfIEVoYIBwB7z/F2EFlV+vVZ1brWs4oUpR6PmvP5BEjYpAovsbe3T3q2mpqaNdBaBGqIn+3tqk//YQMvEp97zp4uAisdBXMX84DhutcTC6vUGmR2ElZ4Y4gFgP4lhU2SccHbKikplY+lumT0h0cCLRHFxSXy6/hc/D5sasVNQNiiQRFYKS9s/cTbv3BZFT2rxHhWoWAl4a0Q+CCw4cFYBlgeDO9tlN8GPh8eWBEAVlqmGvTPcXJqSp2cnPKhJ7BST/BFUF74vap6wsoFnlUoWIWKx2AVhFYgPBpeyL6KiovlezI5OaXOuSGCwEoVYT4QxnrNvVYFDjK7owy0E1b+KA8GykWACz8TvAlWvACWwHKz9vf35dIHHOSEzwba5FmlclNo0mBVdjfgdSFwsSxFYLlSaFkwhpk5yOxez8qJzOo+sMqkTPQb93guKALLVdrY2JDrpjJy60I1YXU/PIEoLCrisDWB5S5tbW0JjNwIq9o0HmR2O6z8ZnyJfP0np3x7SGC5pAxE20J9MmDFQWYXw6osGIWFRXIBLUVgJVXYsgDPisv3bCoDq9InswoCC71aRcX6OWmT248oAispwqYFvA3MSIOdnlXUsEKgax4ft3d2eHAIrMQLQ7C48y4prQsZvHwvFWFlRIEuC9niQGAlRbg0IimZFQeZU8Kzug8rBHqyavT3nXckElgJ1fb2dhA09KySs3wvlTIrIzBviLnDldVVHiICKzHCn46dnZ28MIKeVUywMrY+oCzEehqKwEqIpqdnIvpWhBU9q3CwQhSXlOrvRY06ZU8WgeW0MCOI5XsJ3WfFQeaUhVVpqCj1SE+WDEVTBJaTGh4edsGmUC7fS8Ygc6wGezhYGcDq6enhgSKwnNOfP3/CGu1sCqVnFQ2wZO1MUbEEvi9YQUQRWAnNruhZsQyMBlZoacCOeDwnBQWFEouLLAsJLAe0ubklvpUbruKiZ5VinlWgFERW1dHRKdMRKAdzc7+r7u5uHi4Cy37hevlqfUg5yMym0Hg8KwQyLPw3Ym025gkHBwf111J55/ZvisCyRbjSyVwO0rOiZxUrsPz+VYlcUmFofHxcLSws8oARWPZqfn4+2ChKz4qwihVWRvhv1/HILUoQMq3Dw0NucCCw7JXP5xNocJCZnlW8sCoJRGFRsfw3cGUygeUosAAEelb0rGIFlRlWRmA8B88Bu90JLIdKQjOwOMjMQebYM6v78eNHvurp6WU5SGDZL6y2xcGnZ0XPyg5YwYDHW0P0Yvnm53nACCx7hbc5aGvIKM+KTaH2w0qDyhwY0alkxzuBlWhgcfleZjeFxgMrybJ0fNel4fAwrwAjsBIELA4yZ/Ygc7QlYChYIWDAV+nvPxtICSxbZHQlAwD0rOhZ2Q0s9GYVFpWo3b09HjYCy7ouLy+DN+NwkJmwsloGmmEVzLIKitT6+gYPG4FlXUjV29raAo2jHGSmZ2XNs3oQgbeFy8vLPGwElnXhDU5zc0twW0OiPSs2hbrXs7IDVsVFxQIsbiAlsGzRwcFBMLviIDPLQKue1QNg6cjPL1BT09M8bASWfcCiwU5Y2eVZmWFlAMu8xYEisCwA61DgkTTPioPMKQur4ihgRWARWLZqZ+dPRGBxkDmzB5njKQHvxw8Ci8CySxsbGwKKjPOsOMjsmGdljiICi8BKBLDoWRFWdsCKwCKwHAcWl+8RVlY8KzOsCCwCy1FgcZA5TT0rh5pCH4MVgUVg2Spc8VVbU8fle8ysnAPWDwKLwLJJe3sHAhZ6VoSVXZ7VnSgqVvkaWD7fPA8bgWVd+/v7AhQOMtOzcgJWRYVF0oe1vLzCw0Zg2QgselYcZLapBAzCKhDwsJaWOPxMYNkFrJpaLt/jILMjsCoksAispAGLg8z0rB6DlQlYgFUBgUVg2anj42PV1NR8D1r0rOhZxelZmWAlwCosEmDhbTRFYFmWsQ/rFlj0rOhZ2QMrM7B2d//wsBFY9gCrqckAFgeZOchs3bMKBaw/fwgsAssGnZ+fq7a2nwIVelb0rKx6VqGAhdjd3eVhI7Cs6+rqSnV1dso1X2wKJazsKAPNkV9QqL9Orzo6OuJhI7DsAVZnAFgcZOYgs52wugVWOYFFYNmjm5sb1dfXJwCgZ0XPyk5YCbDyC+R7eMzr6gksuzQwMOAHFgeZWQbGAKzHYFVYWKS+f/8hz87FxQUPGoFlj4aGhuSg07MirOzKrAArP7DyCSwCy179+jUmBzgpnhWbQlO2KfQxWOHtYJ7OsBqbmtX1zTUPGoFlj8bGxsMCi02hHGSOF1aI3Lzv0jZDEVi26ffvWQEEB5lTY5C5JEVgRWARWI7I55u3B1j0rDKqKTQUrO4DKyc3T/X09PKQEVj2aWVlRYACEHGQmZ6VHZmVGVi9vX08ZASWfTIuopCgZ8VBZptgZZSEoyO/eMgILPu0u7cnmRXgQ8+Ky/fsgpXxlnB8fIKHjMCyT4eHh9Llbjew6Fllnmd1P9CHhZc6FIFlm87OzuQCCsPHosFOz8pqZoXAHCGu+FpYWOQhI7Ds0+XlpWppaRV40LPiILMdsCoowG05hRJra+s8ZASWfbq+vlYdHZ0CBnpWHGS2A1YGsPBxZ2eHh4zAsk/6+ySvnnHgE+5ZcZA5ZQaZYwUWNo3ifx8ecrUMgWWzhoaGTcCiZ8VB5vgzK8muCgrV9x/58u86PT3jASOw7NXExKQ+2F6WgfSsbIEVAi0N+PlwUwOBZbtmZ2f1oS0nrJz2rFJ4kDkWWCHQNIqbmK6uuamBwLJZS0tLGhaVd2HF5Xv0rGIx2U2wMoDV0tomHilFYNmqjY1N2esO+NCzYlNoVLAqCA8rBOYIu7q6ebgILPu1t78v3e4CLA4ysynUIqwQ2d9y1ODgEA8XgWW/cD9hQ2OjwIOeFZfvWYWVACsnR17mUASW7cLtOS2trRoKVRxkZlOoZVihBwtvCX2+eR4uAssZGc2j9KzoWVmBlTFDWJBfpNbXN3iwCCxnhDUgONT0rOhZWYEVRnKwpaG4uFQdHBzyYBFYzgitDQAAPSsu37MCK0Re3g/5WcNqoAgsR7S7u6vhVCMAIqzoWcULK8S3nFzV2dXFQ0Vg2a+Dw0O1qLMrvIKuDgCLnhU9q0hNoZFgZRju+MMPd14uLy/LW2iKwIpb6D7G5RNdXT0yPoEDb5SDhBUHmePNrAArIwCtrOxv8hHf9/7+AdluSxFYMenk5ET19vUHAWDuvaJnxUFmO2BlDmxtwJhOdnaOGPHT0zM8hARWdDo6OlItLS1yqA2virCiZ+UUrILxwx8Y14G/hdvGKQIrorAKuaOjI2TrAgeZOchsxbOKBlbItILZ1rdcNTc3x0NJYIXX1NS0pT4relap61kVJjmzMmBlxLecPFWsvzfHx8c8mARWaN+qvr7hwawgm0LZFJpoWBnxNfubGhkZ5eEksB7K55vXB5tNoRxkdgesEPCz8HOGVUERWHeEOUFzFzsHma0b7CWEVdywQqAbHrG6usYDSmDdCk175nKQnhWbQpMNK8waIr5mfVNDw8M8pATWrba3t2PuYKdnRc/KaVj5gZWtWtu4RpnAMmlhcUlgQc+Kg8xughU64NGTVamfOfpYBFZQ09PTcqDpWbEp1E2wQsB4Ly4pkbfYBBaBJRoZHVWe8jJ6VhxkdhWsEGgixccDzhgSWIYGBgf9GRZhRc/KRbAygIWAz0pgEVgKXiZaGozZQXpWHGR2C6wMYOXoWFldJbAILP/lEp2dXQIIDjLTs3ITrMzAWltfJ7AILPuAxcyKg8x2wyoIrJw8WSBJYBFYEYHFMpDL95IJKwFW7nf17Vuump7hjiwCKwyw6FnRs3IDrDCWA2DhhmgCi8AKCSx6VhxkdgusEOjDIrAILMvAYmZFz8ppWIl/RWARWKGABUiwDOQgs5tgRWARWGGBVeYtJ6zYFOoqWBFYBFZkYHGQmZ6Vi2BlBtbM798EFoFlAlZ5OQeZCStXwcoAVk5urlpeXiGwCCy/cIElDjPLQHpWboLVLbDy1ObWFoFFYPk1OvpLDilhRc/KTbAygJWn/1mHh0cEFoHl15zPJ4cf0KFnxaZQt8AKkZWdIz9frPEmsAgs0db2lh9QJmBxkJmeVbJhlZP7XX3+kqWamlu4IpnAutXZ2Zmqq6sXuLAM5PI9t8AK8enzV15CQWA9VE9Pj78sJKw4yOwSWOH2Z+x0X15e5gElsO5qdnZWH2gPYUXPyhWwQmRlf5Pv8cXFBQ8ogXVXhweHqrqmRsBCz4qeVbJhhbeDn798VR2dXTycBFZodXV1yaFnGchB5mTDCqUgOtx56zOBFVbwCiIBi7ByvgwsZGYl8eVrtsy2Xl9f82ASWKGFyyqbm5uDPVlsCqVnlQxY+YGVpaampnkoCazI8vl8GgAeNoXSs0oarHA9vUc/FzTbCawosqwr1dR0m2WlwyBzCWGVGFjlWYeVmO06u5qcmuJhJLCi08LCogCBnhUHmROZWRmlYEVVldgTFIEVlW5u/lVd3d36UJfSs2JmlTBY4a0ggpemElgxa39/X3a8AyQcZCasnIYV4tOXr6q7p5eHj8CKT/PzC3LwAR8u3yOsnIQVSkFvRaW6OKfRTmDFKUzIDw+PyMEup2fFplCHYIW3gj/01/Pnzx8eOgLLmq6vb1R3d48cTnpWzKzshhXmBdHVzqvoCSzbhOVpP9s79IEuUWUcZCasbMysACv0/lEElq3Ca+be3j59YIsFIhxkJqyswAqDzfhvXFxc5OEisJxrd5iYmBBA4UADPqFgZQCIg8yZ51n52xNyw8IKJSDeBuIN9Pb2Dg8VgeW8tra2VLsuEQEVHGIcegACH5GBoWTEA3kfWly+l+6ZVZ7K0//cwqISGV7Gmz+UfQj8NQJf9/DIiLq6vOJBIrASmW3dqM3NTTU8PKxaW9tUfUODampqUn19/Wpn54+amprSh7+UnlWGXRiBLBtv+379GlM1NbXy88YfYI1NzWpiclIdHfH2GwLLBUL/DCBmCNDyeMoFNpY8Kw4yp9SFES0trQ9e2FxdMZsisNxu0uuHFBdbACT0rDKjzwpl4NjYOB9+Ais11dffL4eZTaGZcWEETHUa6QRWympubk4Vog2CTaEZcWEERrdY/hFYKauDgwPlrfDK1gd6VundZ4VWhZ7ePj70BFZqq6W1TQ4/m0LT+8IIZFgLCwt84Ams1Bb2chfow0jPKn072NHOgO/f2dk5H3gCK7W1u7urysu8Ah96Vul7YURb208+7ARW6gu9WejNAQy4fC9d1xpnK988y0ECK13KwulpOfD0rNIPVvCu8PNBgyhFYKWFDg8PZWSjuMTD5XtpuCkU+/8pAiut1NHZqQ90MQeZ0whWxmaG9fUNPuAEVnppcXFJAFDiRBlIWCUFVtjCUFlVra7ZLEpgpZuwALC2ts5vvtOzSnlY+ZfwZanx8Qk+3ARWempsbEx6sjjInPqwQu8Vvv6TkxM+2ARWmprvR0fBbaVu96zYFJoXMT59xigO7xAksNJc2OCAQ8ym0NSFFbKrXP15f3Z4LReBleba2d4RCBUVl9KzSkFYYY0MsitsmaUIrLQXLmdFi8OdLIuwshdWefbDCqBCoI2BrQwEVkYJl1gYEOIgc+pkVgi8GWxqauZDTGBlljo6AlkWPauUgZU/u8phdkVgZWiWFQAPm0LdDyu/d5WlmltapaynCKyME26SzteHl56V+2BlBhUCbwbxe3CFF0VgZaSwQrnMUy4Q4SCzezMrxMdPX1RXdw8fWgIrszU6OqoPaAHLQBfD6mvWN/m+HB0f84ElsDJb2KNUXVMrB4Kwch+ssK/90+cvamJikg8rgUVBi0tLAosielYug5W/SbS6pkZdX3MjA4FFifDSCc2kOKgcZHYPrLBNFBkW2xgILOqesJUUg9HY5sDMKvmwMkrBgYFBPpwEFhVKs7Oz+oDn+4FEWCUVVp+/fFXeigp1cXHBB5PAokKXhv+qzs4ufXh/EFZJhBVKQfz6xgZLQQKLiqiTk1NVWVklB5+eVeJhhdEbZFcjo7/4MBJYVDRaX18X+BhvDjnInBhYIT5++qyam5s5fkNgUbFoYnJSH9h8VQRosQxMCKw+f82S7RnHbBAlsKjYhBuj4Wfl6QNJWDkPK9yAg89Zp29FYFHx6eL8QtXVN8gBJqzsG2S+D6usbzly3fzv2Vk+dAQWZUUYkPZWVMqhp2dlf2YFk93fbzXAh43AouzQ5qZ/QymAwszKPlgZ/Vbt7e002Qksyk4tr6wItAAewsoeWH3Jyla1dfXqijc3E1iUvbq8vJKtDuiEJ6yseVbmt4Ijo6N8uAgsym6dnZ2r6uqamIHFzCo0rPzlYJYaGhrmw0VgUU4BC0BgZmUdVrhQAjvaCSwCi0oWsAirqGFFYBFYVDKBRVjFBCsCi8CiXAAswio8rAgsAotKNrCYWcUFKz+wvhJYBBaVMGARVjGXgbfh73AnsAgsKhHAIqwswcoPLGZYBBblPLBSBVZ5yWsKfQxWBBaBRSUCWN/zmVnF6VkRWAQWlSRgEVaxZlZ3YUVgEVhUgoBFWMVfBhqRlZ2jPn4isAgsyjFgVWlgAQgcZLYOKwKLwKIc1Hk0wKJnRWARWFRKAIuwiglWBBaBRSULWPSsHgXVfVgRWAQWlQxgEVYxZVV3gcVOdwKLShywWAZaANY3uTSVwCKwqEQAi7CyBCsCi8CiEgUsloGWYUVgEViU08CqqhFQEFbxeVZmWBnAGhwa4sNFYFF26+zsTAOrOggsloHWgfUBwBoksAgsynZdXl4GgUVYxV8GGvE1SwPrI0tCAouyTRcXl2p7e1tNTU2rtrafsgcLoGEZaB1WCFzzVVBYrLq6utXCwoLa39/nQ0dgUdEKNxAfHByo2dk5OUQ1cnFqgcrRhzlXQ4Cwir4p9DFYmaH14eMnCXy9NbW1qr9/QG7aPjk54UNJYFGG9PdfHR4eKt/8vBoYGFS1dXWSReHg4MACUgAQB5mt+1WhYOWP7GB8/vJVvf/wUeCF1TP4fjU0Nqnh4RG1trYuZTlFYGWU8Kf20tKyGh4ZVY36MBSXlKo8fTAEUPrwGwBin5W9ZWA0wDLiy9dsybwArXfvP0rg7+EPk6bmFjU+PqE2NzclI6YIrLTS6empPNzj4+OqqalFlZV5bzOovNsyj+M2zhrsscDqYWh4ffkibRDIwN6+/yD/rqLiUvEWZ2Z+q52dHcmYKQIrtYzy83O1rR9eGOXtHR2qorJKgILyTkDwPQSgCKuEeVbhQBUJVvcD2RfmEN9/+CTZ11sd+PeXeytVR0enmvP5pNS/ubnhgSCw3GeU4+3SzMyM6u7pVdU1NQIeHH7JoLAhVMMnE9cau92zihdWoQIA85ePHwRk+Joqq6pVb2+fWlxclEybIrCSYpTjTR5egw8MDspbpaLiEjnwOKSABODDG5mT2xQaq2cVDawiAcsfXyVw3yF6vN6++6DevHkvMMP3q66uQQ0Nj6jl5RV1cXHBw0RgOaOjoyO1srKqRkZGVX19g/KUeu4AyijxeCNz+nhWsYHqFlZ+YN0NAMsA2Os379RHDTT83OvrG9XY2Jh4nHwDSWBZNspHf/1Sra2tqtxbIRDIkxLvR2hAEVauagpNRBkYDaxChVE+vnn7XgD2Wf+9Qp2lNzU1q+npabW7u8s3kARWeOFPt+3tHTU5NaXaOzpVRUWlAEIgFQBUoQZRYThAEVZJH2ROFViZoWUEPK83b5F9vZcsDL9eVu5V7e0d8gYSGT4N/AwG1s3Nv/Kn2O/fv1Vvb6+sbSksKpYDnwtABbIn/D2JAKxiARZhlWqelTVgxQurUIHyEdnXy1dvBGD4OrzeStXT26fm5+fVcYZ24GcUsPb3YZQvythFbW29KikplfLOaNa8AyhzRAsrZlYp0xTqlszqMWghjNYJlI4AWPANZGWVGhoeVkvLy+r09IzASnUd6jR6dXVVpvExXoEU2zjc+QCUBkdIQIWAFctAelbJgFWokPLx3Xv16u079eLVa8nA0NtXU1snL4XgvZ6fnxNYbhcuGl1fX1ejo79U28+fAijz4QdYijSEjCCs2BSaarASYH38dBum7OvV67cS+Ht4fhsbm2WEaHdvL23eQKY0sPAWZWt7W01OTkl3MYxyAMZ/0PP9gCouuY1MhlUem0LTEVahwgDYi5ev5SN+X2FRifrZ3iHTFwcHhyk7QpRSwLq5uZaRl7m5OekoxzI7+FCAE6LQyKDMkAoBrGhhRc8qszyraJtC3QyrUPCCeY/M69nzlwKwL1+yVamnTHV1d6v5+QV5A0lg2QKoGxl5mV9YUH19/dKwWVLqCTZqwoMCgIo1jIyIBCqWgensWX3L+MwqHLDMYTSuAmDPX7xSb/Rf4+eBPsP+gUG1srrqaoC5CljXGlBYvYKO8sHBQdXc3KzKysrvHH4012EdixlShBU9K8IqMqjCBQAGeKF8BMDw1/h5VlfXyq57GPi4S4DACnhQaNZEZ29/f79qbW1T5eVegUZ+oJMcIBFAGRECVIQVB5kJq9hhZQDLHNL7Fci+ALE3gTeQ6FPs7OzSZ3VGqp5kNbEmHFgYeVlYXFR9GlAYGkaJh9JOTHJziWeG1CPASphnxabQjBxkTvUyMFpY3Y/XCA2wF6/eiP/15OkL6QPDTrCS0jJpYl1bW0tPYOEihQGdYlZWV8vB92dPfuDAOL8flmDFzIqDzPSsLMHqjTk0tIww+19Pnj6XXy/W8Br9NaaOj45TH1jr6xsyDwXgmEs8ZFZRg4qwomfFMtCxMjAsrEygChWvA+XjM8Dr2Qv5GeLl2KmDnpdjwDo4PFC9fX0CIRzkIKSMsJBVEVYcZCas7IfVmxhhFQyddSGMrAvPCJYIpAywsNjOW1EhxvkDUCUAWPSs6FnF51llsQyMFVgBWJkDftdTHdU1tTpxOXQvsK6vr9XIyEgQHCFBZRFWzKzYFMrMyr2wehUIlIp/I9vSzxKuR3MdsPCas6enV+Xrw1hiNasirNgUyjIw8bCKowwMBStz4M3ie/3fgIuBXQMsZFYCq3wbYEXPip4VYeVKgz1WWBnD2E+fvVCvX7+TMSBXAAsdsQKrcCUgPSuWgRm0fC+ty8A4gOWH1kv5mre2tpILLJ9vTkAQNrNywrPiIDM9K5aBrvKsIsHKiH+ePJdtvlZGfSwBCy36GKUJa7CHABUNdnpWbApNDVhZzarM8TIQfz95Jss0Ew4smOxoCMUhpmdFz4oGOz2rx2AlM4qBIWusKk8osHCTLQ48YcVBZhrs7ti64Ba/KhysjPjn6XM5f/FcYxYXsPBWEPenOQEselb0rDjI7F7PKhZghYIVhqexBeIfXRpOTU8nBlhI50LCip4VYUXPip5VBFgZgSwLnQXX11fOAgveVUtL60NgsSmUnhXLQHpWUcDKn2W9kabSuTmfs8DCmhgDPPSs6FkRVuk5yOwkrIzAG8PKympngTU6OvrwzWAqeFZsCuXyPZaBSfGsQgWWAmJAGl/L7u6eM8CC2V7f0CCg4CAzYUXPik2hscLqhSnQ4vDXP0/V8MioM8DCQnpAqjgWc52wYlMoy8C0GGS2Wga+CBF//fNMeSsqnQHWxMSEHFZ6VhxkJqzoWVmFlVEW4vuA27JsBda/+v9wcywuiuAgM8tADjJn7iBzvJ7Vgwh0vSPm5+ftBdbZ+blcBS9zg0n2rNgUykFmloGp51ndh5URf/39RHV1ddsLrM2tLT+c7t1yQ8+KnhXLQHpW8cIK8T8NrGh9rKiBNTMzc6ccpGfFplDCik2hVmElYzqBVcrnuoqzDVgD/QNy2SlhZRFWeWwKJazSZ5DZKqzQ2vD0xSv9z3qjdnd37QGW/hz182d7cAUyB5npWbEplE2hdsAKgXsNkWVFs0I5KmCdn1+o2ro6gQM9K3pWbArlILNdsDIDa3BoyB5gHR8fq7KyclUIGBFW9KxYBtKzsglWRvz19zPV3NJqD7C2d3YEJEVmYNGz4iAzYcWmUBtg9fzFa/W/v56osvIKe4C1tLysYVL0KLA4yMymUHpW9KxihRXir7+f6jNaJOurLANr5vfvu/4VMysOMtOzSn3PyiWwQuBGnezsXHV6emodWCOBlTIsA+lZsQykZ2U3rPzAeiE/k739fevA6uvvl4NNWHGQmbCiZ2U3rBC4aBX/bZubW9aB1d7RKU2jHGSmZ5UYz4qDzG5fvmcnrARYz1+qJzqWV1asAQtNo22tbQ+AxcyKTaHMrDjIbAes0IeF+O9f/yjfI1sbHgXW9fWNamxski53woqDzIQVB5mdgBUCQ9AzM7+tAevq6lo1NDQGgUVY0bMirGiw2w0rA1hDwyPWgIVNgFXV1bKpgZ4VPSsu3+PyPSdgJcDSJaGtwOIgMz0rZlb0rJyA1bPnLwVYHZ1d1oCFOcLKqioBBMtADjITVhxkdgJWBrBaWtusAWtvb095PGUCFMKKTaH0rOhZOQErA1itbT+tAQtLtUpLPbfAIqw4yExYcfmezbCyH1iBbQ30rOhZcZCZg8x2w8oPrCequqbWxgyLmRUHmelZ0bNyAFYI3J7z2IqZ6IEF052womfFMpCelQOwMoBV7q20yXTX4CCs6FkRVhxkdgJWtgFra2s7CCE2hXKQmZ4VB5mdgJVtwFpdXZNDycyKnhU9KzaFOgUrw3S3DKy1tXU5wIQVB5kJK3pWTsEK8X//cxpYhBUHmelZ0bOyCCss70OgD6vc+/hbws5In4ANgPkBwz3+0Rx/OAG2aKEWC9geg1punjliAVtkwEUDtuwYsrDEDjw/BNzjYLsNu6D24dNnf3yMPuyC2ltzWCwXX4cKJ7ytWMD28mE8Dwu2V1HFs+e3AQ/LW/FIhqWjP9InHB0dKU9ZufKUe1WZEfp/G+GJFJ7bKPWURY7S2ygp9USOEn8Uh7uFOnhZxm0URbrVRwDsj8fgW1BojsgAzi8IFaEB/CM/VDwE8PcfoSI0gPO+h4rHABwexKHhGxrE4d+G3o3w8H0Y4QHsj68Ab0S/7mE8DmB/CFQjAVfHx8BHgeunCNDV8cEIyRi/+D+a4v39CIL184N49yAA14fxNmR8jAzTO2B9GKHBGh6uD2D7+jaePnuhauvqIwLr/wEr6+CHgdyuWwAAAABJRU5ErkJggg=="
    },
    status: {
      type: String,
      default: 'offline'
    },
    salt: {
      type: String
    },
    created: {
      type: Date,
      default: Date.now
    },
    resetPasswordToken: {
      type: String,
      default: "" 
    },
    resetPasswordExpires: {
      type: Date,
      default: "" 
    },
    theme: {
      type: String,
      default: "" 
    },
    lastLogin: {
      type: Date,
      default: Date.now
    },
    
});

UserSchema.virtual('fullName').get(function() {
    return this.firstName + ' ' + this.lastName;
}).set(function(fullName) {
    var splitName = fullName.split(' ');
    this.firstName = splitName[0] || '';
    this.lastName = splitName[1] || '';
});

UserSchema.pre('save', function(next) {
    if (this.password) {
      this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
      this.password = this.hashPassword(this.password);
    }
  next();
});

UserSchema.methods.hashPassword = function(password) {
  return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};
UserSchema.methods.authenticate = function(password) {
  return this.password === this.hashPassword(password);
};
// UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
//     console.log(username);
//     console.log(suffix);

//     var _this = this;
//     var possibleUsername = username + (suffix || '');

//     _this.findOne({
//       username: possibleUsername
//     }, function(err, user) {
//       if (!err) {
//           if (!user) {
//             callback(possibleUsername);
//           } else {
//             return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
//           }
//       } else {
//           callback(null);
//       }
//     });
// };

UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});

mongoose.model('User', UserSchema);