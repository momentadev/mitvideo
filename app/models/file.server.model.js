var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var fileSchema=new Schema({
	fileName: {
		    type: String,
		    default: ''
	},
  	location: {
		    type: String,
		    default: ''
    },
    date:{
		    type: String,
		    default: ''
	},
    uploadedUser:{
		    type: String,
		    default: ''
	},
	sharedUser:{
		    type: String,
		    default: ''
	},
	extention:{
		    type: String,
		    default: ''
	},
	comments:{
		    type: String,
		    default: ''
	},
	uploadType:{
		    type: String,
		    default: ''
	},
	messageID:{
		    type: String,
		    default: ''
	}
});
mongoose.model('file',fileSchema); 