var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request'),
    friend = mongoose.model('friends'),
    message = mongoose.model('messages'),
    schedule = mongoose.model('confrenceschedule');
    connect = mongoose.model('connect');
    module.exports = function(io,socket){
        // Get friends details to sidebar and confrence call friends list
        socket.on('get_friends_sidebar',function(id){
            var frnds = [];
            friend.findOne({user:id}).exec(function(err,res){
                if(res!=null){
                    if(res.friends.length>=1){
                        var i=0;
                        res.friends.forEach(function(frnd){
                            //user.findOne({$and:[{_id:frnd.id},{lastLogin: {$gt: new Date(Date.now() - 24*60*60 * 1000)}}]}).exec(function(err,usr){
                            user.findOne({_id : frnd.id}).exec(function(err,usr){
                             // if(usr!=null) { 
                               // console.log("fullName",usr.fullName);
                                var contact = {
                                    _id:usr._id,
                                    username:usr.username,
                                    fullName:usr.fullName,
                                    status:usr.status,
                                    unreadMsg:0
                                }
                                message.find({$and:[{sender:usr._id},{receiver:id}]}).exec(function(err,res1){
                                    if(res1.length>=1){
                                        var k=0,msgUnreadLen=0;
                                        res1.forEach(function(msg){
                                            if(msg.read.length==0){
                                                msgUnreadLen++;
                                            }
                                            k++;
                                            if(k>=res1.length){
                                                contact.unreadMsg = msgUnreadLen;
                                                frnds.push(contact);
                                                i++;
                                                if(i>=res.friends.length){
                                                    socket.emit('friends_sidebar',frnds);
                                                }
                                            }
                                        })
                                    }else{
                                        frnds.push(contact);
                                        i++;
                                        if(i>=res.friends.length){
                                            socket.emit('friends_sidebar',frnds);
                                        }
                                    }
                                })
                            })
                        })
                    }
                    else{
                        socket.emit('friends_sidebar',frnds);
                    }
                }
                else{
                    socket.emit('friends_sidebar',frnds);
                }
            })
        })
        // send circle details to the side bar.
        socket.on('get_circles_sidebar',function(id){
            var grps=[];
            circle.find({$or:[{creator : id},{"members.id" : id}]}).exec(function(err,res){
                if(res.length>=1){
                    var i=0;
                    res.forEach(function(circ){
                        var tempCircle = {
                            _id : circ._id,
                            circleName : circ.circleName,
                            unreadMsg : 0
                        }
                        message.find({receiver:circ._id}).exec(function(err,res1){
                            if(res1.length>=1){
                                var j=0,circleMsgUnreadLen=0;
                                res1.forEach(function(msg){
                                    if(msg.read.length>=1){
                                        var found = false,k=0;
                                        msg.read.forEach(function(reader){
                                            if(reader.user == id){
                                                found = true;
                                                k++;
                                            }else{
                                                k++;
                                            }
                                            if(k>=msg.read.length){
                                                if(found == false){
                                                    circleMsgUnreadLen++;
                                                }
                                                j++;
                                                if(j>=res1.length){
                                                    tempCircle.unreadMsg = circleMsgUnreadLen;
                                                    grps.push(tempCircle);
                                                    i++;
                                                    if(i>=res.length){
                                                        socket.emit('circles_sidebar',grps)
                                                    }
                                                }
                                            }
                                        })
                                    }else{
                                        circleMsgUnreadLen++;
                                        j++;
                                        if(j>=res1.length){
                                            tempCircle.unreadMsg = circleMsgUnreadLen;
                                            grps.push(tempCircle);
                                            i++;
                                            if(i>=res.length){
                                                socket.emit('circles_sidebar',grps)
                                            }
                                        }
                                    }
                                })
                            }else{
                                grps.push(tempCircle);
                                i++;
                                if(i>=res.length){
                                    socket.emit('circles_sidebar',grps)
                                }
                            }
                        })
                    })
                }else{
                    socket.emit('circles_sidebar',res);
                }
            })
        })
        socket.on('iRead', function(data){
            message.find({receiver:data.id}).exec(function(err,res){
                if(res.length>=1){
                    res.forEach(function(msg){
                        if(!(msg.read.some(function(r){
                            return r.user === data.uid;
                        }))){
                            message.findOneAndUpdate(
                                {_id:msg._id},
                                {$push: {read: {user: data.uid}}},
                                {safe: true},
                                function(err, model) {
                                    if(err)
                                        console.log(err);
                                }
                            );
                        }
                    })
                }
            })
        })
        socket.on('getPic',function(id){
            user.findOne({_id:id}).exec(function(err,res){
                socket.emit('pic',res);
            })
        })
        socket.on('deleteAccount',function(userID){
            friend.findOne({user:userID}).exec(function(err,res){
                for (var i = 0;i < res.friends.length;i++ ) {
                    friend.update({user: res.friends[i].id},{$pull: {friends:{id:userID}}},function(err,res){
                    });    
                };
            })
            user.findOne({_id:userID}).remove().exec(function(err,res){
                if(!err){
                    friend.findOne({user : userID}).remove().exec(function(err,res){
                    })
                    schedule.remove({creator : userID}, function(err) { 
                    });
                    message.find({sender: userID}).exec(function(err,data){
                        data.forEach(function(msg){
                            message.remove({_id : msg._id}, function(err) { 
                            });    
                        })  
                    })
                   socket.emit("accountDeleted");
                }
            })
        })
        socket.on('profileDetails',function(id){
            user.findOne({_id:id}).exec(function(err,res){
                socket.emit('profDeatailsresp',res);
            })
        })
        socket.on('changeStatus',function(data){
            if(data.status == "offline"){
                user.findOneAndUpdate({_id: data.uid},{status: data.status},function(err, model) {
                    console.log("------------------------------------- disconnect sb ----------------------------",model.fullName,model.status);
                    if(err)  console.log(err);
                });
                connect.findOne({clientId : data.uid}).remove().exec()
                io.emit('statChange',data.uid);
            }else{
                user.findOneAndUpdate({_id: data.uid},{status: data.status},function(err, model) {
                     if(err)  console.log(err);
                });
                var c = new connect({
                        socketId : socket.id,
                        clientId : data.uid
                    })
                    c.save(function (err, data) {
                        if (err) console.log(err);
                    });
                io.emit('statChange',data.uid);
            }    
        })
        socket.on('changePassword',function(data){
            user.findOne({_id:data.usersid}, function(err, user) {
                if(!err){
                    user.password = data.password;
                    user.save(function(err) {
                        if(!err){   
                            socket.emit("returnChagePassword","Password Changed Successfully")
                        }
                    });
                }else{
                    socket.emit("returnChagePassword","Password  Change Failed")
                }
            });
        })
        socket.on('removeOldMsg',function(){
            message.find({date: {$lt: new Date(Date.now() - 168*60*60 * 1000)}}).exec(function(err,data){
           //message.find({date: {$gt: new Date(Date.now() - 168*60*60 * 1000)}}).exec(function(err,data){
                data.forEach(function(msg){
                    message.remove({_id : msg._id}, function(err) { 
                    });    
                })  
            })
        })
        // socket.on('chageTheme',function(colorScheme){
        //     user.update({_id:colorScheme.id}, {$set: {theme:colorScheme.cCode}},  function(err){
        //         if(!err){
        //           var themeUpdate="Theme Updated";
        //           socket.emit('updatedTheme',themeUpdate);
        //         }
        //     })
        // })
    }