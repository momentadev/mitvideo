var User = require('mongoose').model('User'),
    passport = require('passport');
var crypto = require('crypto');   
var nodemailer  = require('nodemailer');   
var formidable = require('formidable'),
    fs   = require('fs-extra'),
    path = require('path');
var mongoose = require('mongoose'); 
var file = mongoose.model('file');
var office2pdf = require('office2pdf'),
    generatePdf = office2pdf.generatePdf;
var emailExistence= require('email-existence');     
var os = require("os"); 
/*-------------------Generating Error messages based on response-------------------*/ 
var getErrorMessage = function(err) {
  var message = '';
  if (err.code) {
     var er=err.message;
      var newTxt = er.split('$');
      var newTxt2 = newTxt[1].split('_');
      switch (newTxt2[0]) {
          case 'email':
              message = 'E-Mail already exists';
              break;
          case 'username':
              message = 'Username already exists';
              break;
          default:
              message = 'Something went wrong';
      }
  } else {
      for (var errName in err.errors) {
          if (err.errors[errName].message) message = err.errors[errName].message;
      }
  }
  return message;
};
 /*-------------------Show Sign-in Form-------------------*/ 
exports.renderSignin = function(req, res, next) {
  if (!req.isAuthenticated()) {
      res.render('signin', {
          title: 'Sign-in Form',
          messages: req.flash('error') || req.flash('info')
      });
  } else {
      return res.redirect('/');
  }
};
  /*-------------------Show Sign-up Form-------------------*/ 
exports.renderSignup = function(req, res, next) {
  if (!req.isAuthenticated()) {
      res.render('signup', {
          messages: req.flash('error')
      });
  } else {
      return res.redirect('/');
  }
};
  /*-------------------Show Forgot Password Form-------------------*/ 
exports.renderForgot = function(req, res, next) {
  if (!req.isAuthenticated()) {
      res.render('forgot', {
          title: 'Forgot Form',
          messages: req.flash('error') || req.flash('info')
      });
  } else {
      return res.redirect('/');
  }
};
  /*-------------------Show Password Reset Form-------------------*/ 
exports.renderReset = function(req, res, next) {
  User.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
          $gt: Date.now()
      }
  }, function(err, user) {
      if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('/forgot');
      }
      res.render('reset', {
          user: req.params.token,
          title: 'Reset Form',
          messages: req.flash('error') || req.flash('success')
      });
  });
};
  /*-------------------Password reset operation using mail(with token)-------------------*/ 
exports.reset = function(req, res, next) {
  User.findOne({
      resetPasswordToken: req.body.tok,
      resetPasswordExpires: {
          $gt: Date.now()
      }
  }, function(err, user) {
      if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('/forgot');
      } else if (req.body.password.length < 6) {
          req.flash('error', 'Password Must Be Atleast 6 Characters ');
          return res.redirect('/forgot');
      } else if (req.body.password != req.body.confirm) {
          req.flash('error', 'Password and Confirm Password Not Equal');
          return res.redirect('/forgot');
      } else {
          user.password = req.body.password;
          user.resetPasswordToken = "";
          user.resetPasswordExpires = "";
          user.save(function(err) {
              var mailer = nodemailer.createTransport({
                  service: 'Yahoo',
                  auth: {
                      user: 'mitvideoconference@yahoo.se',
                      pass: '!@#momentaint#@!'
                  }
              });
              var templ ='<center><img src="https://212.116.73.202:3000/images/momenta_logo.png" width="150" height="70" /></center><h4>Hello '+user.firstName+'</h4><p>This is a confirmation that the password for your account ' + user.email + ' has just been changed. </p>' ;
              var mailOptions = {
                  to: user.email,
                  from: 'MIT Video Chat ✔ <mitvideoconference@yahoo.se>',
                  subject: 'Your password has been changed',
                  html:templ
              };
              mailer.sendMail(mailOptions, function(error, info) {
                  req.flash('error', 'Reset Password successfully');
                  return res.redirect('/signin');
              });
          });
      }
  });
};
  /*-------------------Forgot Password operations.if user exist send a password reset token to mail-------------------*/ 
function customToken() {
    var buffreValue = new Buffer(64);
    for (var i = 0; i < buffreValue.length; i++) {
        buffreValue[i] = Math.floor(Math.random() * 256);
    }
    var token = buffreValue.toString('base64');
    return token;
}
var getToken = customToken()

exports.forgot = function(req, res, next) {
User.findOne({
      email: req.body.useremail
  }, function(err, user) {
      if (!user) {
          req.flash('error','Account Not Found.');
          return res.redirect('/forgot');
      } else {
          crypto.randomBytes(20, function(err, buf) {
              var token = buf.toString('hex');
              user.resetPasswordToken = token;
              user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
              user.save(function(err) {
                  var mailer = nodemailer.createTransport({
                      service: 'Yahoo',
                      auth: {
                          user: 'mitvideoconference@yahoo.se',
                          pass: '!@#momentaint#@!'
                      }
                  });
                  var urls='https://' + os.networkInterfaces().eth0[0].address +':'+req.socket.localPort+'/reset/' + token;
                  var template ='<center><img src="https://212.116.73.202:3000/images/momenta_logo.png" width="150" height="70" /></center><h4>Hello '+user.firstName+'</h4><center><b>Token Expired in 20 Minutes</b></center><p>You are receiving this because you (or someone else) have requested the reset of the password for your account.<br>' +
                          'Please click on the following link, or paste this into your browser to complete the process:<br></p><center><a href='+urls+' target="_blank" style="background:#357ebd;color: white;font-weight: bold; padding: 13px 30px;text-decoration: none;font-family: Arial;position:relative;border: 2px solid;border-radius: 25px;">Reset Password</a></center> <br><br> <hr/><center><a style="font-size:0.8em; color:#3b5998; text-decoration:none;" href="https://www.facebook.com/pages/Momenta/185153844845777"> <img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/facebook_square-512.png" width="30" height="30" > </a>  <a style="font-size:0.8em; color:#55acee; text-decoration:none;" href="https://twitter.com/momentaab" > <img src="http://media.slidedb.com/images/members/4/3008/3007633/profile/Twitter_Icon_RGB_Square.png" width="30" height="30"></a></center> ' ;
                  var mailOptions = {
                      to: user.email,
                      from: 'MIT Video Chat ✔ <mitvideoconference@yahoo.se>',
                      subject: 'Reset Your Password',
                      html:template
                  };
                  mailer.sendMail(mailOptions, function(error, info) {
                      req.flash('error', 'Reset link has been sent to ' + user.email + ' .');
                      return res.redirect('/forgot');
                  });
              });
          });
      }
  });
};
  /*-------------------Sign up authentication page.check all database criteria's.if sucess send a mail -------------------*/ 
exports.signup = function(req, res, next) {
  if (!req.isAuthenticated()) {
    var user = new User(req.body);
    emailExistence.check(user.email, function(err,res){
          if(res == true){
            console.log('E-Mail Valid');
          }else{
            console.log('E-Mail Not Valid');
          }
    });
    var messages = null;
    user.username = user.username.toLowerCase();
    user.provider = 'local';
    user.save(function(err) {
      console.log("err in signup",err)
      if (err){
         throw err;
        var messages = getErrorMessage(err);
        req.flash('error', messages);
        return res.redirect('/signup');
      }else{
          var mailer = nodemailer.createTransport({
                        service: 'Yahoo',
                        auth: {
                            user: 'mitvideoconference@yahoo.se',
                            pass: '!@#momentaint#@!'
                        }
                    });
          var template ='<center><img src="https://212.116.73.202:3000/images/momenta_logo.png" width="150" height="70" /></center><center><h2>Welcome to MIT Video Chat, '+user.firstName+' '+user.lastName+'</h2></center><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin laoreet scelerisque leo, sit amet adipiscing odio. </p><br><br> <hr/><center><a style="font-size:0.8em; color:#3b5998; text-decoration:none;" href="https://www.facebook.com/pages/Momenta/185153844845777"> <img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/facebook_square-512.png" width="30" height="30" > </a>  <a style="font-size:0.8em; color:#55acee; text-decoration:none;" href="https://twitter.com/momentaab" > <img src="http://media.slidedb.com/images/members/4/3008/3007633/profile/Twitter_Icon_RGB_Square.png" width="30" height="30"></a></center> ' ;
          var mailOptions = {
              to: user.email,
              from: 'MIT Video Chat ✔ <mitvideoconference@yahoo.se>',
              subject: 'User Sign-up',
              html:template
          };  
         mailer.sendMail(mailOptions, function(error, info) {
         });
      }
      req.login(user, function(err) {
        if (err) return next(err);
        return res.redirect('/');
      });
    });
  } else {
    return res.redirect('/');
  }
};
/*-----File upload and sharing.File are converted to pdf using office2pdf module-------------------*/ 
  function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
        }
          return s4() + s4() + '-' + s4() ;
  }
exports.uploadFiles = function(req, res) {
 var file = mongoose.model('file');
 var form = new formidable.IncomingForm();
 form.parse(req, function(err, fields, files) {
     var upext = fields.uploadext;
     var loc = path.dirname(require.main.filename);
     var userID = fields.userId;
     var temp_path = files.files.path;
      var uuid = guid();
      if(fields.duplicteStatus == 0){
          var file_name = userID + "_" + files.files.name;
      }else{
          var file_name = uuid+userID + "_" + files.files.name;
      }    
     var flname = file_name.replace(/(.*)\.[^.]+$/, "$1");
     flname = flname + ".pdf";
    var new_location = loc + '/public/uploadedfiles/';
    var fileDetails = new file({
         fileName: flname,
         location: new_location,
         date: fields.datetime,
         uploadedUser: userID,
         extention:"pdf",
         sharedUser: fields.shareduserID,
         comments: fields.comments,
         uploadType:"fileUpload"
    });
    var otherFiles = new file({
        fileName: file_name,
        location: new_location,
        date: fields.datetime,
        uploadedUser: userID,
        extention:upext,
        sharedUser: fields.shareduserID,
        comments: fields.comments,
        uploadType:"fileUpload"
    });
   if(upext == "ppt" || upext == "pptx" || upext == "doc" || upext == "docx" || upext == "xls" || upext == "xlsx" ){
      fs.copy(temp_path, new_location + file_name, function(err) {
             if (err) {
                 console.error(err);
                 res.end('File Uploaded Failed.Error in Copying,' + new_location + "," + file_name + ",Upload Failed");
              }else{
                  generatePdf(new_location + file_name, function(err, result) {
                     if (!err) {
                         fs.unlink(new_location + file_name, function(err) {
                             if (!err) {

                                 fileDetails.save(function(err, data) {
                                     if (!err)
                                         console.log("DB");
                                        res.end('File Converted & Uploaded Successfully,' + new_location + "," + flname + ",File Uploaded,"+data._id);
                                 });
                             }
                         });
                      }else{
                         console.log(err, "Generated PDF Err.");
                         fs.unlink(new_location + file_name, function(err) {
                            if (!err) {
                                  res.end('File Convertion Failed.Convertion Module Not Found,' + new_location + "," + flname + ",Upload Failed");
                             }
                          });
                     }
                 });
             }
         });
      }else{
          fs.copy(temp_path, new_location + file_name, function(err) {
            if(err){
                console.log(err);
                res.end('File Uploaded Failed.Error in Copying,' + new_location + "," + file_name + ",Upload Failed");
            }else{
             otherFiles.save(function(err, data) {
                       if (!err) {
                           res.end('File Uploaded Successfully,' + new_location + "," + file_name + ",File Uploaded,"+data._id);
                       }
                   });
            }
          });
     }
  });
}
/*---------------------------Shared Files---------------------------------*/
exports.uploadSharedFiles = function(req, res) {
   var file = mongoose.model('file');
 var form = new formidable.IncomingForm();
 form.parse(req, function(err, fields, files) {
     var upext = fields.uploadext;
     var loc = path.dirname(require.main.filename);
     var userID = fields.userId;
     var temp_path = files.files.path;
      var uuid = guid();
      if(fields.duplicteStatus == 0){
          var file_name = userID + "_" + files.files.name;
      }else{
          var file_name = uuid+userID + "_" + files.files.name;
      }    
     var flname = file_name.replace(/(.*)\.[^.]+$/, "$1");
     flname = flname + ".pdf";
    var new_location = loc + '/public/uploadedfiles/';
    var fileDetails = new file({
         fileName: flname,
         location: new_location,
         date: fields.datetime,
         uploadedUser: userID,
         extention:"pdf",
         sharedUser: fields.shareduserID,
         comments: fields.comments,
         uploadType:"fileUpload"
    });
    var otherFiles = new file({
        fileName: file_name,
        location: new_location,
        date: fields.datetime,
        uploadedUser: userID,
        extention:upext,
        sharedUser: fields.shareduserID,
        comments: fields.comments,
        uploadType:"fileUpload"
    });
   if(upext == "ppt" || upext == "pptx" || upext == "doc" || upext == "docx" || upext == "xls" || upext == "xlsx" ){
      fs.copy(temp_path, new_location + file_name, function(err) {
             if (err) {
                 console.error(err);
                 res.end('File Uploaded Failed.Error in Copying,' + new_location + "," + file_name + ",Upload Failed");
              }else{
                  generatePdf(new_location + file_name, function(err, result) {
                     if (!err) {
                         fs.unlink(new_location + file_name, function(err) {
                             if (!err) {

                                 fileDetails.save(function(err, data) {
                                     if (!err)
                                         console.log("DB");
                                        res.end('File Converted & Uploaded Successfully,' + new_location + "," + flname + ",File Uploaded,"+data._id);
                                 });
                             }
                         });
                      }else{
                         console.log(err, "Generated PDF Err.");
                         fs.unlink(new_location + file_name, function(err) {
                            if (!err) {
                                  res.end('File Convertion Failed.Convertion Module Not Found,' + new_location + "," + flname + ",Upload Failed");
                             }
                          });
                     }
                 });
             }
         });
      }else{
          fs.copy(temp_path, new_location + file_name, function(err) {
            if(err){
                console.log(err);
                res.end('File Uploaded Failed.Error in Copying,' + new_location + "," + file_name + ",Upload Failed");
            }else{
             otherFiles.save(function(err, data) {
                       if (!err) {
                           res.end('File Uploaded Successfully,' + new_location + "," + file_name + ",File Uploaded,"+data._id);
                       }
                   });
            }
          });
     }
  });
}
/*-------------------Signout Function-------------------*/ 
exports.checkUsername = function(req, res) {
    User.find({username: req.body.name}).exec(function(err,result){
      if(result.length == 0){
        res.end('0');
      }else{
        res.end('1');
      }
    })    
}
exports.checkEmailid = function(req, res) {
    User.find({email: req.body.name}).exec(function(err,result){
      if(result.length == 0){
        res.end('0');
      }else{
        res.end('1');
      }
    })    
}
exports.signout = function(req, res) {
  console.log("session",req.session);
    req.session.destroy();
    //req.logout();
    res.redirect('/landing');
  
};
exports.requiresLogin = function(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.status(401).send({
      message: 'User is not logged in'
    });
  }
  next();
};
