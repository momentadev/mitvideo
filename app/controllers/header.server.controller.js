var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle=mongoose.model('circles');
var FormData = require('form-data');    

module.exports = function(io, socket) {
    // update profile deatails.
    socket.on('updateProfile',function(profile){
        user.update(
            {_id:profile._id}, 
            {$set: { 
                firstName:profile.firstName,
                lastName:profile.lastName,
                designation:profile.designation,
                companyName:profile.companyName,
                aboutMe:profile.aboutMe,
                education:[],
                experience:[],
                projects:profile.projects
            }},  
            function(err){
                if(!err){
                    var i = 0;
                    if(profile.education.length>=1){
                        profile.education.forEach(function(edu){
                            var tempEdu = {
                                course : edu.course,
                                period : edu.period,
                                university : edu.university
                            }
                            user.findOneAndUpdate(
                                {_id:profile._id},
                                {$push: {"education": tempEdu}},
                                {safe: true},
                                function(err){
                                    if(!err){
                                        i++;
                                        if(i>=profile.education.length){
                                            var j = 0;
                                            if(profile.experience.length>=1){
                                                profile.experience.forEach(function(exp){
                                                    var tempExp = {
                                                        designation : exp.designation,
                                                        period : exp.period,
                                                        company : exp.company
                                                    }
                                                    user.findOneAndUpdate(
                                                        {_id:profile._id},
                                                        {$push:{"experience": tempExp}},
                                                        {safe:true},
                                                        function(err){
                                                            if(!err){
                                                                j++;
                                                                if(j>=profile.experience.length){
                                                                    user.findOne({_id:profile._id}).exec(function(err,res){
                                                                        socket.emit('updateComplete',res);
                                                                    });
                                                                }
                                                            }else{
                                                                console.log(err);
                                                            }
                                                        }
                                                    )
                                                })
                                            }else{
                                                user.findOne({_id:profile._id}).exec(function(err,res){
                                                    socket.emit('updateComplete',res);
                                                });
                                            }
                                        }
                                    }else{
                                        console.log(err);
                                    }
                                }
                            )
                        })
                    }else if(profile.experience.length>=1){
                        var j = 0;
                        profile.experience.forEach(function(exp){
                            var tempExp = {
                                designation : exp.designation,
                                period : exp.period,
                                company : exp.company
                            }
                            user.findOneAndUpdate(
                                {_id:profile._id},
                                {$push:{"experience": tempExp}},
                                {safe:true},
                                function(err){
                                    if(!err){
                                        j++;
                                        if(j>=profile.experience.length){
                                            user.findOne({_id:profile._id}).exec(function(err,res){
                                                socket.emit('updateComplete',res);
                                            });
                                        }
                                    }else{
                                        console.log(err);
                                    }
                                }
                            )
                        })
                    }else{
                        user.findOne({_id:profile._id}).exec(function(err,res){
                            socket.emit('updateComplete',res);
                        });
                    }
                }
                else{
                    console.log(err)
                }
            }
        )
    })
    // View profile details
    socket.on('viewProfile',function(viewProfile){
        user.find({_id:viewProfile.id}).exec(function(err,res){
            socket.emit('viewProfileDetails',res);
        })    
    })
    // Update profile picture upload
    socket.on('profilePicturUpload',function(profilePicture){
        user.update({_id:profilePicture.id}, {$set: { profilePicture:profilePicture.image}},  function(err){
            if(!err){
                var profilePic="Profile Picture Updated";
                socket.emit('updatedPicture',profilePic);
            }
        })
    })
    /*------------------------   Searching the single word     ----------------------------------  */
    socket.on('txt', function(text){
      var exp = new RegExp("^"+text.replace(/\\/, "\\\\"),"i");
        user.find({firstName: exp}).exec(function(err,res){
            socket.emit('res',res);
        })    
    })
    /*---------------------------- Searching the multiple words using the username ---------------------------*/
    socket.on('srch', function(text){
      var e1 = new RegExp("^"+text[0].replace(/\\/, "\\\\"),"i");
      var e2 = new RegExp("^"+text[1].replace(/\\/, "\\\\"),"i");
      user.find({$and: [{firstName: e1},{lastName: e2}]}).exec(function(err,res){
        socket.emit('res',res);
      })    
    })
    /*---------------------------------- Searching the  designation -------------------------*/
    socket.on('srchDesg', function(text){
       var exp1 = new RegExp("^"+text.name.replace(/\\/, "\\\\"),"i");
       var exp2 = new RegExp("^"+text.des.replace(/\\/, "\\\\"),"i");
            user.find({$and: [{firstName: exp1},{designation: exp2}]}).exec(function(err,res){
               socket.emit('resDesg',res);
            })
    })
    /*-------------------------------------- Searching using user -------------------------------*/
    socket.on('txtDesg', function(text){
       var exp1 = new RegExp("^"+text.name[0].replace(/\\/, "\\\\"),"i");
       var exp2 = new RegExp("^"+text.name[1].replace(/\\/, "\\\\"),"i");
       var exp3 = new RegExp("^"+text.des.replace(/\\/, "\\\\"),"i");
            user.find({$and: [{firstName: exp1},{lastName: exp2},{designation: exp3}]}).exec(function(err,res){
                socket.emit('resDesg',res);
            })
    })
    /*-------------------------------------- Searching the Company name ------------------------------------*/
    socket.on('srchCmpny', function(text){
        var expr1 = new RegExp("^"+text.name.replace(/\\/, "\\\\"),"i");
        var expr2 = new RegExp("^"+text.des.replace(/\\/, "\\\\"),"i");
        var expr3 = new RegExp("^"+text.cmpny.replace(/\\/, "\\\\"),"i");
            user.find({$and: [{firstName: expr1},{designation: expr2},{companyName: expr3}]}).exec(function(err,res){
                socket.emit('resCmpny',res);
            })
    })
    /*--------------------------------------   Searching the company -------------------------------------- */
    socket.on('txtCmpny', function(text){
        var expr1 = new RegExp("^"+text.name[0].replace(/\\/, "\\\\"),"i");
        var expr2 = new RegExp("^"+text.name[1].replace(/\\/, "\\\\"),"i");
        var expr3 = new RegExp("^"+text.des.replace(/\\/, "\\\\"),"i");
        var expr4 = new RegExp("^"+text.cmpny.replace(/\\/, "\\\\"),"i");
            user.find({$and: [{firstName: expr1},{lastName: expr2},{designation: expr3},{companyName: expr4}]}).exec(function(err,res){
                socket.emit('resCmpny',res);
            })
    }) 
}