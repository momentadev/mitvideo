var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request'),
    friend = mongoose.model('friends'),
    message = mongoose.model('messages'),
    Connect = mongoose.model('connect'),
    file = mongoose.model('file');
var fs   = require('fs-extra'),
    path = require('path'),
    ffmpeg = require('fluent-ffmpeg');    
    module.exports = function(io,socket){

/*----------------------------------save messages to database---------------------------------*/
      socket.on('sendMsg',function(data){
        var fileID=data.fileid;
        var newMessage = new message({
              sender : data.sender,
              senderName : data.senderName,
              receiver : data.receiver,
              message : data.message,
              date : data.date
        })
        newMessage.save(function (err, data){
            if(!err){
                  if(fileID != undefined ){
                     file.update({_id:fileID}, {$set: {messageID:data._id}},function(err){
                            if(err){
                                console.log(err);
                            }        
                    })   
                }
            }
        })
        Connect.findOne({clientId : data.receiver}).exec(function(err,res){
            if(res!=null)
              io.to(res.socketId).emit('recMsg', newMessage);
        })
      })

/*---------------------------------change message read/unread--------------------------------*/
    socket.on('allRead', function(data){
      message.update({$and:[{sender:data.id},{receiver:data.uid},{read:[]}]},
        {$push: {"read": {user: data.uid}}},
        {multi:true,safe:true},
        function(err){
          if(err)
            console.log(err);
      })
    })

/* --------------------------------get messages from db--------------------------------------*/

    socket.on('get_messages',function(data){
      message.find({$or:[{$and:[{sender:data.id},{receiver:data.uid}]},{$and:[{sender:data.uid},{receiver:data.id}]}]}).sort('-date').limit(25).exec(function(err,res){
        if(res!=null){
          socket.emit('textMessages',res);
        }
      })
    })
    socket.on('getMore',function(data){
        message.find({$or:[{$and:[{sender:data.id},{receiver:data.uid}]},{$and:[{sender:data.uid},{receiver:data.id}]}]}).sort('-date').skip(data.skip).limit(25).exec(function(err,res){
          if(res!=null){
            if(res.length>=1){
              socket.emit('nextMessages',res);
            }
          }
        })
    })
    socket.on('recevierDetails',function(data){
      user.findOne({_id : data}).exec(function(err,res){
         socket.emit('recevierDetailsResult',res);
      }); 
    })
/*---------------------------------Video Message and combining the audio/video message  -----------------------*/  

socket.on('videoMessages',function(data){
    var upuser=data.sharedUser;
    var fileDetails = new file({
             fileName: data.fileName,
             location: "",
             date: data.date,
             uploadedUser: data.uploadedUser,
             extention:"webm",
             sharedUser:data.sharedUser,
             comments: "",
             uploadType:"videoMessage"
         });
      fileDetails.save(function(err, data) {
          if (!err) {
              socket.emit('vdoMsgResponse',data);
              io.to(upuser).emit('vdoMsgAlert', data);
          }
      });
}) 
 // combining the seperated audio and video files into one file 
socket.on('videoMessage', function(data) {
    var fileName = data.fileName;
    socket.emit('ffmpeg-output', 0);
    writeToDisk(data.audio.dataURL, fileName + '.wav');
    if (data.video) {
        writeToDisk(data.video.dataURL, fileName + '.webm');
        merge(fileName);
    } else socket.emit('merged', fileName + '.wav');
});
 // write the file into the local disk.
  function writeToDisk(dataURL, fileName) {
      var fileExtension = fileName.split('.').pop(),
          fileRootNameWithBase = './public/uploadedfiles/' + fileName,
          filePath = fileRootNameWithBase,
          fileID = 2,
          fileBuffer;
      while (fs.existsSync(filePath)) {
          filePath = fileRootNameWithBase + '(' + fileID + ').' + fileExtension;
          fileID += 1;
      }
      dataURL = dataURL.split(',').pop();
      fileBuffer = new Buffer(dataURL, 'base64');
      fs.writeFileSync(filePath, fileBuffer);
  }
  // merge the multiple file into single file.ffmpeg is used to merge files
  function merge(fileName) {
      var audioFile  = path.join('public/uploadedfiles', fileName + '.wav'),
          videoFile  = path.join('public/uploadedfiles', fileName + '.webm'),
          mergedFile = path.join('public/uploadedfiles', fileName + '-merged.webm');
      new ffmpeg({
              source: videoFile
          })
          .addInput(audioFile)
          .on('error', function(err) {
              console.log("err in file merge",err);
              socket.emit('ffmpeg-error', 'ffmpeg : An error occurred: ' + err.message);
          })
          .on('end', function() {
              socket.emit('merged', fileName + '-merged.webm');
              console.log('Merging finished !');
              fs.unlink(audioFile);
              fs.unlink(videoFile);
          })
          .saveToFile(mergedFile);
  }
}