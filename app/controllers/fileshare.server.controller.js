var mongoose = require('mongoose');
var file = mongoose.model('file'),
    message = mongoose.model('messages'),
    connect = mongoose.model('connect'),
    circle = mongoose.model('circles');
var fs   = require('fs-extra');
var fileNames = [];
module.exports = function(io,socket){
  /*-------------------Get Uploaded File Details From Database-------------------*/  
    socket.on('getFiles',function(data){
        file.find({$and:[{uploadedUser:data},{uploadType:"fileUpload"}]}).exec(function(err,resp){
            if(resp!=null){
                socket.emit('uploadedFile',resp)
            }    
        });
    }); 
/*---------------------------------------------- Remove Uploaded File -----------------------------------------------------*/    
    socket.on('removeFiles',function(data){
        file.findOne({_id : data}).exec(function(err,res){
            if(!err){
                var msgID = res.messageID;
                var shareID = res.sharedUser; 
                fs.unlink(res.location + res.fileName , function(err) {
                    if(!err){
                        file.remove({_id: data}, function(err) {
                            if(!err){
                                message.remove({_id: msgID}, function(err) {
                                    if(!err){
                                        var del="File Deleted"
                                        socket.emit('deleteResponse',del);
                                        connect.findOne({clientId:shareID}).exec(function(err,res){
                                            if(res!=null) {
                                               io.to(res.socketId).emit('shareFileDeleted');
                                            }
                                        })
                                    }
                                }) 
                            }   
                        });
                    }
                });
            }
        });   
    });
    socket.on('removeCirclefile',function(id){
        file.findOne({_id : id}).exec(function(err,res){
            if(!err){
                var msgID = res.messageID;
                fs.unlink(res.location + res.fileName , function(err) {
                    if(!err){
                        file.remove({_id: id}, function(err) {
                            if(!err){
                                message.remove({_id: msgID}, function(err) {
                                    if(!err){
                                        socket.emit('circleFiledeleted',"File Deleted");
                                        circle.findOne({_id : res.uploadedUser}).exec(function(err,result){
                                            result.members.forEach(function(mem){
                                                connect.findOne({clientId:mem.id}).exec(function(err,res){
                                                    if(res!=null) {
                                                       io.to(res.socketId).emit('returncircleFileDeleted');
                                                    }
                                                })
                                            })
                                            connect.findOne({clientId:result.creator}).exec(function(err,res){
                                                if(res!=null) {
                                                   io.to(res.socketId).emit('returncircleFileDeleted');
                                                }
                                            })
                                        })
                                    }
                                }) 
                            }   
                        });
                    }
                });
            }
        })        
    })
    /*---------------------------------  Get Shared  File Details From Database ---------------------------------*/  
    socket.on('getSharedFiles',function(data){
      file.find({$and:[{sharedUser:data},{uploadType:"fileUpload"}]}).exec(function(err,resp){  
            if(resp!=null){
                socket.emit('sharedFiles',resp)
            }    
        });
    });
    /*-------------------Revoke  Shared File Permisssion -------------------*/    
    socket.on('removeShareFiles',function(data){
        file.update({_id:data}, {$set: {sharedUser:""}},function(err){
            if(!err){
                var res="File Acess Permission Removed";
                socket.emit('shareRemove',res);
            }        
        })   
    }); 
    /*-----------------------Video Messages----------------------------------*/
    socket.on('getVdoMsg',function(data){
        file.find({$and:[{uploadedUser:data},{uploadType:"videoMessage"}]}).exec(function(err,resp){
            if(resp!=null){
                socket.emit('videoMsgDetails',resp)
            }    
        });
    });
     /*------------------- Details of Specific File to View -------------------*/ 
    socket.on('viewFiles',function(data){
        file.findOne({_id : data}).exec(function(err,res){
           socket.emit('fileDetails',res);
        });  
    }); 
    // get file name
    socket.on('getFileNames',function(id){
        file.find({uploadedUser:id}).exec(function(err,res){
            if(res){
                res.forEach(function(file){
                    fileNames.push(file.fileName);   
                })
                socket.emit("fileNameList",fileNames);
                fileNames = [];
            }
        }); 
    })
}