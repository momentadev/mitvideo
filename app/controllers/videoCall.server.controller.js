var kurento = require('kurento-client'),
    mongoose = require('mongoose'),
    user = mongoose.model('User'),
    minimist = require('minimist');
    connect = mongoose.model('connect')
var kurentoClient = null;
var userRegistry = new UserRegistry();
var pipelines = {};
var candidatesQueue = {};

// kurento configuration
var argv = minimist(process.argv.slice(2), {
    default: {
        //ws_uri: 'ws://192.168.1.149:8888/kurento'
        ws_uri: 'ws://212.116.73.202:8888/kurento'
    }
});

// Setting user Session configuration
function UserSession(id, socketid) {
    this.id = id;
    this.peer = null;
    this.sdpOffer = null;
}
// Send Message  prototype function.All  message send through this function.
UserSession.prototype.sendMessage = function(message,io) {
    connect.findOne({clientId:this.id}).exec(function(err,res){
        if(res!=null){
            io.to(res.socketId).emit('testcallResponse',message);
        }
    })
}
function UserRegistry() {
    this.usersById = {};
}
UserRegistry.prototype.register = function(user) {
    this.usersById[user.id] = user;
}
UserRegistry.prototype.unregister = function(id) {
    var user = this.getById(id);
    if (user) delete this.usersById[id]
}
UserRegistry.prototype.getById = function(id) {
    return this.usersById[id];
}
UserRegistry.prototype.removeById = function(id) {
    var userSession = this.usersById[id];
    if (!userSession) return;
    delete this.usersById[id];
}
module.exports = function(io,socket){
    // Represents registrar of users
    function CallMediaPipeline() {
        this.pipeline = null;
        this.webRtcEndpoint = {};
    }
    CallMediaPipeline.prototype.createPipeline = function(callerId, calleeId, socketid, callback) {
        var self = this;
        getKurentoClient(function(error, kurentoClient) {
            if (error) {
                return callback(error);
            }

            kurentoClient.create('MediaPipeline', function(error, pipeline) {
                if (error) {
                    return callback(error);
                }

                pipeline.create('WebRtcEndpoint', function(error, callerWebRtcEndpoint) {
                    if (error) {
                        pipeline.release();
                        return callback(error);
                    }

                    if (candidatesQueue[callerId]) {
                        while(candidatesQueue[callerId].length) {
                            var candidate = candidatesQueue[callerId].shift();
                            callerWebRtcEndpoint.addIceCandidate(candidate);
                        }
                    }

                    callerWebRtcEndpoint.on('OnIceCandidate', function(event) {
                        var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
                        var message = {
                            id : 'iceCandidate',
                            candidate : candidate
                        }
                        connect.findOne({clientId:userRegistry.getById(callerId).id}).exec(function(err,res){
                            io.to(res.socketId).emit('testcallResponse',message)
                        })
                    });

                    pipeline.create('WebRtcEndpoint', function(error, calleeWebRtcEndpoint) {
                        if (error) {
                            pipeline.release();
                            return callback(error);
                        }

                        if (candidatesQueue[calleeId]) {
                            while(candidatesQueue[calleeId].length) {
                                var candidate = candidatesQueue[calleeId].shift();
                                calleeWebRtcEndpoint.addIceCandidate(candidate);
                            }
                        }

                        calleeWebRtcEndpoint.on('OnIceCandidate', function(event) {
                            var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
                            var message = {
                                id : 'iceCandidate',
                                candidate : candidate
                            }
                            connect.findOne({clientId:userRegistry.getById(calleeId).id}).exec(function(err,res){
                                io.to(res.socketId).emit('testcallResponse',message)
                            })
                        });

                        callerWebRtcEndpoint.connect(calleeWebRtcEndpoint, function(error) {
                            if (error) {
                                pipeline.release();
                                return callback(error);
                            }
                            calleeWebRtcEndpoint.connect(callerWebRtcEndpoint, function(error) {
                                if (error) {
                                    pipeline.release();
                                    return callback(error);
                                }
                            });

                            self.pipeline = pipeline;
                            self.webRtcEndpoint[callerId] = callerWebRtcEndpoint;
                            self.webRtcEndpoint[calleeId] = calleeWebRtcEndpoint;
                            callback(null);
                        });
                    });
                });
            });
        })
    }
    CallMediaPipeline.prototype.generateSdpAnswer = function(id, sdpOffer, callback) {
        this.webRtcEndpoint[id].processOffer(sdpOffer, callback);
        this.webRtcEndpoint[id].gatherCandidates(function(error) {
            if (error) {
                return callback(error);
            }
        });
    }
    CallMediaPipeline.prototype.release = function() {
        if (this.pipeline) this.pipeline.release();
        this.pipeline = null;
    }
    socket.on('callStopbyinitiator',function(data){
        connect.findOne({clientId:data.calleid}).exec(function(err,res){
            io.to(res.socketId).emit('callendbycaller',data.callerid);
        })
    })
    socket.on('registerCaller',function(callerid){
        registercaller(callerid,socket.id)
    })
    socket.on('registerCallee',function(calleeid){
        connect.findOne({clientId:calleeid}).exec(function(err,res){
            if(res!=null){
                registercallee(calleeid,res.socketId)
            }else{
                socket.emit('registerCalleeResponse',false);
            }
        })
    })

    socket.on('TestCallMessage', function(_message) {
        var message = JSON.parse(_message);
        switch (message.id) {
            case 'call':
                call(message.to, message.from, message.sdpOffer);
                break;
            case 'incomingCallResponse':
                incomingCallResponse(message.to, message.from, message.callResponse, message.sdpOffer, socket.id);
                break;
            case 'stop':
                stop(message.from);
                break;
            case 'onIceCandidate':
                onIceCandidate(message.user, message.candidate);
                break;
            default:
                socket.emit('testcallResponse',({
                    id : 'error',
                    message : 'Invalid message ' + message
                }));
                break;
        }
    });
    // register caller  details
    function registercaller(id, socketid, callback) {
        function onError(error) {
            console.log("emitError")
        }
        if(!userRegistry.getById(id))
            userRegistry.register(new UserSession(id, socketid));
        socket.emit('registerCallerResponse',true)
    }
    // register calle details 
    function registercallee(id, socketid, callback) {
        if(!userRegistry.getById(id))
            userRegistry.register(new UserSession(id, socketid));
        socket.emit('registerCalleeResponse',true)
    }
    // get kurento configuration details.
    function getKurentoClient(callback) {
        if (kurentoClient !== null) {
            return callback(null, kurentoClient);
        }

        kurento(argv.ws_uri, function(error, _kurentoClient) {
            if (error) {
                var message = 'Coult not find media server at address ' + argv.ws_uri;
                 console.log(message + ". Exiting with error " + error)
                return callback(message + ". Exiting with  " + error);
            }
            kurentoClient = _kurentoClient;
            callback(null, kurentoClient);
        });
    }
    // generating call from the server side
    function call(to, from, sdpOffer) {
        
        clearCandidatesQueue(from._id);
        var caller = userRegistry.getById(from._id);
        var rejectCause = 'User ' + to + ' is not registered';
        if (userRegistry.getById(to)) {
            var callee = userRegistry.getById(to);
            caller.sdpOffer = sdpOffer
            callee.peer = from._id;
            caller.peer = to;
            var message = {
                id: 'incomingCall',
                from: from
            };
            connect.findOne({clientId:callee.id}).exec(function(err,res){
                if(res!=null){
                    io.to(res.socketId).emit('newIncomingVideoCall',message);
                }
            })
        }else{
            var message  = {
                id: 'callResponse',
                response: 'rejected: ',
                message: rejectCause
            };
            caller.sendMessage(message,io);
        }
    }
    // respond to incoming call
    function incomingCallResponse(calleeId, from, callResponse, calleeSdp, socketid) {
        clearCandidatesQueue(calleeId);
        function onError(callerReason, calleeReason) {
            if (pipeline) pipeline.release();
            if (caller) {
                var callerMessage = {
                    id: 'callResponse',
                    response: 'rejected'
                }
                if (callerReason) callerMessage.message = callerReason;
                caller.sendMessage(callerMessage,io);
            }

            var calleeMessage = {
                id: 'stopCommunication'
            };
            if (calleeReason) calleeMessage.message = calleeReason;
            callee.sendMessage(calleeMessage,io);
        }
        var callee = userRegistry.getById(calleeId);
        if (!from || !userRegistry.getById(from)) {
            return onError(null, 'unknown from = ' + from);
        }
        var caller = userRegistry.getById(from);

        if (callResponse === 'accept') {
            var pipeline = new CallMediaPipeline();
            pipelines[caller.id] = pipeline;
            pipelines[callee.id] = pipeline;

            pipeline.createPipeline(caller.id, callee.id, socketid, function(error) {
                if (error) {
                    return onError(error, error);
                }

                pipeline.generateSdpAnswer(caller.id, caller.sdpOffer, function(error, callerSdpAnswer) {
                    if (error) {
                        return onError(error, error);
                    }

                    pipeline.generateSdpAnswer(callee.id, calleeSdp, function(error, calleeSdpAnswer) {
                        if (error) {
                            return onError(error, error);
                        }

                        var message = {
                            id: 'startCommunication',
                            sdpAnswer: calleeSdpAnswer
                        };
                        callee.sendMessage(message,io);
                        message = {
                            id: 'callResponse',
                            response : 'accepted',
                            sdpAnswer: callerSdpAnswer
                        };
                        caller.sendMessage(message,io);
                    });
                });
            });
        } else {
            var decline = {
                id: 'callResponse',
                response: 'rejected',
                message: 'user declined'
            };
            caller.sendMessage(decline,io);
        }
    }
     // stop video call    
    function stop(sessionId) {
        if (!pipelines[sessionId]) {
            return;
        }
        var pipeline = pipelines[sessionId];
        delete pipelines[sessionId];
        pipeline.release();
        var stopperUser = userRegistry.getById(sessionId);
        var stoppedUser = userRegistry.getById(stopperUser.peer);
        stopperUser.peer = null;

        if (stoppedUser) {
            stoppedUser.peer = null;
            delete pipelines[stoppedUser.id];
            var message = {
                id: 'stopCommunication',
                message: 'remote user hanged out'
            }
            stoppedUser.sendMessage(message,io)
        }

        clearCandidatesQueue(sessionId);
    }
    function onIceCandidate(sessionId, _candidate) {
        var candidate = kurento.register.complexTypes.IceCandidate(_candidate);
        var user = userRegistry.getById(sessionId);
        if (pipelines[user.id] && pipelines[user.id].webRtcEndpoint && pipelines[user.id].webRtcEndpoint[user.id]) {
            var webRtcEndpoint = pipelines[user.id].webRtcEndpoint[user.id];
            webRtcEndpoint.addIceCandidate(candidate);
        }
        else {
            if (!candidatesQueue[user.id]) {
                candidatesQueue[user.id] = [];
            }
            candidatesQueue[sessionId].push(candidate);
        }
    }
    function clearCandidatesQueue(sessionId) {
        if (candidatesQueue[sessionId]) {
            delete candidatesQueue[sessionId];
        }
    }
};