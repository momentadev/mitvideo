var passport = require('passport');

exports.renderLanding = function(req, res, next) {
    if(!req.isAuthenticated()){
    	res.render('landing', {
	      title: 'Video Chat'
	    });
    } else {
    	return res.redirect('/');
    }    
};