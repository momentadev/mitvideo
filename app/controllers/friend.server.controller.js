var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request'),
    friend = mongoose.model('friends'),
    connect = mongoose.model('connect');
    module.exports = function(io,socket){

/*---------------------------------get friends list----------------------------------------*/
        socket.on('getFriends',function(id){
            var data = [],i=0;
            friend.findOne({user:id}).exec(function(err,res){
                if(res!=null){
                    if(res.friends.length>=1){
                      res.friends.forEach(function(f){
                        user.findOne({_id:f.id}).exec(function(err,res1){
                            data[i]=res1;
                            i++;
                            if(i>=res.friends.length){
                                socket.emit('friendsList',data)
                            }
                        })
                      })
                    }
                    else
                        socket.emit('friendsList',data)
                }
                else
                    socket.emit('friendsList',data)
            })
        })

/*-----------------------------------remove friend-------------------------------------*/

        socket.on('removeFriend',function(data){
            friend.update(
                {user: data.u},
                {$pull: {friends:{id:data.c}}},
                {safe:true},
                function(err,res){
                    if(err)
                        console.log(err)
                    else{
                        connect.findOne({clientId:data.u}).exec(function(err,res){
                            if(res!=null){
                                io.to(res.socketId).emit('removedFriend',data)
                            }
                        })
                    }
                }
            );
            friend.update(
                {user: data.c},
                {$pull: {friends:{id:data.u}}},
                {safe:true},
                function(err,res){
                    if(err)
                        console.log(err)
                    else{
                        connect.findOne({clientId:data.c}).exec(function(err,res){
                            if(res!=null){
                                io.to(res.socketId).emit('removedFriend',data)
                            }
                        })
                    }
                }
            );            
        })
    }