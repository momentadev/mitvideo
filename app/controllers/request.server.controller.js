var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request'),
    friend = mongoose.model('friends'),
    connect = mongoose.model('connect')
    module.exports = function(io,socket){

/*-------------------------------get sent requests list--------------------------------------*/

    socket.on('getSntReqData',function(id){
        request.find({sender:id}).exec(function(err,res){
            var datas = [];
            var a=[],i=0;
            if(res.length>=1){
                res.forEach(function(x){
                    user.findOne({_id:x.receiver}).exec(function(err,res1){
                        a[i++]=res1;
                        if(i>=res.length){
                            datas = a;
                            socket.emit('sntReqData', datas)
                        }
                    })  
                })
            }else{
                socket.emit('sntReqData', datas)
            }
        })
    })

/*--------------------------------get received requests list---------------------------------*/

    socket.on('getRecReqData',function(id){
        var datar=[],b=[],j=0;
        request.find({receiver:id}).exec(function(err,resp){
            if(resp.length>=1){
                resp.forEach(function(x){
                    user.findOne({_id:x.sender}).exec(function(err,resp1){
                        b[j++]=resp1;
                        if(j>=resp.length){
                            datar = b;
                            socket.emit('recReqData', datar)
                        }
                    })
                })
            }else{
                socket.emit('recReqData', datar)
            }
        })
    })

/*-------------------------remove request from db on reject request-------------------------*/

    socket.on('removeReq',function(data){
        request.find({$and : [{sender:data.c},{receiver:data.u}]}).remove().exec(function(err,res){
            if(!err){
                socket.emit('removedReq',data);
                request.find({receiver:data.u}).exec(function(err,result){
                    connect.findOne({clientId:data.u}).exec(function(err,res){
                        if(res!=null) {
                           io.to(res.socketId).emit('requestCountLive',result.length);
                        }
                    })
                })
            }
        });
    })
    socket.on('removeRequest',function(data){
        request.find({$and : [{sender:data.c},{receiver:data.u}]}).remove().exec(function(err,res){
            if(!err){
                socket.emit('removedReq',data);
                request.find({receiver:data.u}).exec(function(err,result){
                    connect.findOne({clientId:data.u}).exec(function(err,res){
                        if(res!=null) {
                           io.to(res.socketId).emit('requestCountLive',result.length);
                        }
                    })
                })
                user.findOne({_id:data.u}).exec(function(err,results){
                    connect.findOne({clientId:data.c}).exec(function(err,res){
                        if(res!=null){
                            io.to(res.socketId).emit('rejectedFriendRequest',results);
                            io.to(res.socketId).emit('changeRequestStatus');
                        }
                    })
                })
            }
        });
    })

    socket.on('pendingRequests',function(id){
        request.find({receiver:id}).exec(function(err,res){
            socket.emit("requestCount",res.length);
        })
    })

/*------------------------add to both user db on request accept-----------------------------*/

    socket.on('acceptReq',function(data){
        friend.findOneAndUpdate(
            {user:data.u},
            {$push: {"friends": {id: data.c}}},
            {safe: true, upsert: true, new: true},
            function(err, model) {
                if(err){
                    console.log(err);
                }else{
                    socket.emit('acceptedReq',data)
                }
            }
        );
        friend.findOneAndUpdate(
            {user:data.c},
            {$push: {"friends": {id: data.u}}},
            {safe: true, upsert: true, new: true},
            function(err, model) {
                if(err){
                    console.log(err);
                }else{
                    user.findOne({_id:data.u}).exec(function(err,res1){
                        connect.findOne({clientId:data.c}).exec(function(err,res){
                            if(res!=null){
                                var reqAcceptData = {
                                    user : res1
                                }
                                io.to(res.socketId).emit('accepted',reqAcceptData);
                            }
                        })
                    })
                }
            }
        );
    })
}