var mongoose = require('mongoose'),
    user = mongoose.model('User');
var passport = require('passport');

exports.renderIndex = function(req, res, next) {
    if (req.isAuthenticated()) {
    	res.render('index', {
	      title: 'Video Chat',
	      user: JSON.stringify(req.user)
	    });
    } else {
    	return res.redirect('/landing');
    }   



};

