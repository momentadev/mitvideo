var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request'),
    friend = mongoose.model('friends'),
    message = mongoose.model('messages'),
    connect = mongoose.model('connect')
    file = mongoose.model('file');
    module.exports = function(io, socket) {
    /*------------------------------------ Find user from user table (User Searching) ---------------------------------------*/    
        socket.on('find',function(query){
            var data = [];
            var que = new RegExp("^"+query.q,"i");
            friend.findOne({user : query.id}).exec(function(err,res){
                if(res!=null){
                    var a = [], i=0;
                    if(res.friends.length>=1){
                        res.friends.forEach(function(x){
                            user.findOne({_id : x.id}).exec(function(err,res1){
                                a[i] = res1;
                                i++;
                                if(i>=res.friends.length){
                                    var j=0,k=0;
                                    a.forEach(function(s){
                                        if(que.test(s.username))
                                            data[k++] = s;
                                        j++;
                                        if(j>=a.length){
                                            socket.emit('sel',data)
                                        }
                                    })
                                }
                            })
                        })
                    }
                    else{
                        socket.emit('sel',data)
                    }
                }
                else{
                    socket.emit('sel', data)
                }
            })
        })
/*---------------------------------add new circle to database-------------------------------*/

        socket.on('add_circle',function(c){
            var c = new circle({
                creator: c.cr._id,
                circleName: c.name,
                members: c.mem
            })
            c.save(function (err, data) {
                if (err) 
                    console.log(err);
                else {
                    socket.emit('added_circle');
                    user.findOne({_id:c.creator}).exec(function(err,res){
                        var cRoomData = {
                            creator: res,
                            circleName: c.circleName,
                            members: c.members
                        }
                        c.members.forEach(function(mem){
                            connect.findOne({clientId : mem._id}).exec(function(err,res){
                                if(res!=null)
                                    io.to(res.socketId).emit('addedToRoom', cRoomData);
                            })
                        })
                    })
                }
            });
        })
        // socket.on('oncallLeave',function(data){
        //     var onLeave = new interupt({
        //                 roomid:data.roomname,
        //                 circleId:data.circle,
        //                 insertby:data.id
        //                 })
        //     onLeave.save(function (err, res) {
        //         if(!err){
        //             console.log("res",res._id);
        //         }
        //     })
        // })
        // socket.on('getLeave',function(data){
        //     interupt.findOne({$and:[{circleId:data.circle},{insertby:data.userid}]}).exec(function(err,res){
        //         console.log("result",res);
        //         socket.emit("retrunleave",res);
        //     })
        // })
        // socket.on('removeLeave',function(data){
        //     interupt.findOne({$and:[{circleId:data.circleId},{insertby:data.insertby}]}).remove().exec(function(err,res){
        //         console.log("remove",res);
        //     })
        // })



/*--------------------------------- get group list -------------------------------------------*/
        socket.on('getOwnCircles',function(id){
            circle.find({creator : id}).exec(function(err,res){
                if(res!=null){
                    socket.emit('OwnCircles',res)
                }
            })
        })
        socket.on('getOtrCircles',function(id1){
            var res = [],i=0,k=0;
            circle.find().exec(function(err,ans){
                if(ans!=null)
                ans.forEach(function(x){
                    j = 0;
                    x.members.forEach(function(y){
                        j++;
                        if(y.id == id1){
                            res[k] = x;
                            k++;
                        }
                        if(j>=x.members.length){
                            i++;
                        }
                        if(i>=ans.length){
                            socket.emit('OtrCircles',res);
                        }
                    })
                })
                else
                    socket.emit('OtrCircles',res);
            })
        })
/*------------------------------add more members to group----------------------------------*/
        socket.on('addToCircle',function(data){
            circle.findOne({_id:data.circle}).exec(function(err,res){
                var i = 0;
                if(res!=null){
                    user.findOne({_id:res.creator}).exec(function(err,creator){
                        var cRoomData = {
                            creator: creator,
                            circleName: res.circleName,
                            members: data.members
                        }
                        data.members.forEach(function(member){
                            circle.findOneAndUpdate(
                                {_id:res._id},
                                {$push: {"members": {id: member._id}}},
                                {safe: true, upsert: true, new: true},
                                function(err, model) {
                                    i++;
                                    if(err)
                                        console.log(err);
                                    else{
                                        connect.findOne({clientId : member._id}).exec(function(err,res1){
                                            if(res1!=null)
                                                io.to(res1.socketId).emit('addedToRoom', cRoomData);
                                        })
                                    }
                                    if(i>=data.members.length){
                                        socket.emit('addedToCircle');
                                    }
                                }
                            )
                        })
                    })
                }
            })
        })

        socket.on('addToCircleinConf',function(data){
            circle.findOne({_id:data.circle}).exec(function(err,res){
                var i = 0;
                if(res!=null){
                    user.findOne({_id:res.creator}).exec(function(err,creator){
                        var cRoomData = {
                            creator: creator,
                            circleName: res.circleName,
                            members: data.members
                        }
                        data.members.forEach(function(member){
                            circle.findOneAndUpdate(
                                {_id:res._id},
                                {$push: {"members": {id: member._id}}},
                                {safe: true, upsert: true, new: true},
                                function(err, model) {
                                    i++;
                                    if(err)
                                        console.log(err);
                                    else{
                                        connect.findOne({clientId : member._id}).exec(function(err,res1){
                                            if(res1!=null)
                                                io.to(res1.socketId).emit('addedToCircleConfRoom', cRoomData);
                                        })
                                    }
                                    if(i>=data.members.length){
                                        socket.emit('addedToCircleConf');
                                    }
                                }
                            )
                        })
                    })
                }
            })
        })
/*------------------------------get group members list--------------------------------------*/
        socket.on('getMem',function(id){
            circle.findOne({_id:id}).exec(function(err,res){
                var data = [],i=0;
                if(res!=null)
                res.members.forEach(function(x){
                    user.findOne({_id : x.id}).exec(function(err,res1){
                        data[i] = res1;
                        i++;
                        if(i>=res.members.length){
                            user.findOne({_id : res.creator}).exec(function(err,res2){
                                var send = {
                                circle : res,
                                creator : res2,
                                members : data
                            }
                            socket.emit('memList',send)
                            })
                        }
                    })
                })
            })
        })

/*-----------------------------------------delete a group----------------------------------*/
        socket.on('deleteGroup',function(id){
            circle.findOne({_id : id}).exec(function(err,room){
                var i = 0;
                room.members.forEach(function(member){
                    connect.findOne({clientId:member.id}).exec(function(err,res){
                        i++;
                        if(res!=null){
                            io.to(res.socketId).emit('roomDeleted',room);
                        }
                        if(i>=room.members.length){
                            circle.findOne({_id : id}).remove().exec(function(err,res){
                                if(!err){
                                    socket.emit('roomDeleted',room)
                                    message.find({receiver: id}).exec(function(err,data){
                                        data.forEach(function(msg){
                                            message.remove({_id : msg._id}, function(err) { 
                                            });    
                                        })  
                                    })
                                }
                            });
                        }
                    })
                })
            })
        })
/*------------------------------------remove member from group-----------------------------*/
        socket.on('removeMember',function(data){
            circle.update(
                {_id: data.circle},
                {$pull: {members:{id:data.member}}},
                {safe:true},
                function(err,res){
                    if(err)
                        console.log(err)
                    else{
                        connect.findOne({clientId:data.member}).exec(function(err,user){
                            if(user!=null)
                            circle.findOne({_id:data.circle}).exec(function(err,room){
                                if(room!=null)
                                    io.to(user.socketId).emit('removedFromRoom',room)
                            })
                        })
                    }
                }
            );
        })
/*---------------------------------get group chat messages---------------------------------*/

        socket.on('grpMsgSent',function(data){
            var fileID=data.fileid;
            var newMessage = new message({
                sender : data.sender,
                senderName : data.senderName,
                senderPic : data.senderPic,
                receiver : data.receiver,
                message : data.message,
                date : data.date,
                read : [{user:data.sender}]
            })
            newMessage.save(function (err, res){
                if(!err){
                    if(fileID != undefined ){
                        file.update({_id:fileID}, {$set: {messageID:res._id}},function(err){
                            if(err){
                                console.log(err);
                            }        
                        })   
                    }
                }
            })
            circle.findOne({_id:data.receiver}).exec(function(err,room){
                if(room!=null){
                    if(room.members.length>=1)
                    room.members.forEach(function(member){
                        connect.findOne({clientId:member.id}).exec(function(err,res){
                            if(res!=null){
                                io.to(res.socketId).emit('recGrpMsg',newMessage);
                            }
                        })
                    })
                    connect.findOne({clientId:room.creator}).exec(function(err,ctr){
                        if(ctr!=null){
                            io.to(ctr.socketId).emit('recGrpMsg',newMessage);
                        }
                    })
                }
            })
        })
        socket.on('get_grp_messages',function(id){
            message.find({receiver:id}).sort('-date').limit(30).exec(function(err,res){
                if(res!=null){
                    socket.emit('grp_messages',res);
                }
            })
        })
        socket.on('getMoreGrp',function(data){
            message.find({receiver:data.id}).sort('-date').skip(data.skip).limit(30).exec(function(err,res){
                if(res!=null){
                    if(res.length>=1){
                        socket.emit('nextGrpMessages',res);
                    }
                }
            })
        })
/*-----------------------------------------------------------*/
    socket.on('circleRecevierDetails',function(data){
        circle.findOne({_id : data}).exec(function(err,res){
             socket.emit('circleRecevierDetailsResult',res);
        }); 
    });
    socket.on('checkinCircle',function(data){
        //circle.findOne({$and:[{_id:data.circleid},{creator: data.userid},{ members: { $elemMatch: { id: data.userid } } }]}).exec(function(err,res){     
        circle.findOne({$and : [{_id:data.circleid},{$or: [{creator: data.userid},{members:{ $elemMatch: { id: data.userid } } }]}]}).exec(function(err,res){  
            if(res==null){
                socket.emit('checkinCircleResult',0);
            }else{
                if(res.creator == data.userid ){
                    socket.emit('checkinCircleResult',0);
                }else{
                    socket.emit('checkinCircleResult',1);
                }
            }
        })
    }) 
    socket.on('getFullname',function(id){
        user.findOne({_id:id}).exec(function(err,res){
            socket.emit("returnFullname",res);
        })
    })
    socket.on('getuserNames',function(id){
        user.findOne({_id:id}).exec(function(err,res){
            socket.emit("returnUsername",res);
        })
    })
    socket.on('getcircleUserList',function(data){
     var test = [];   
        circle.findOne({_id:data}).exec(function(err,res){
            if(res!=null){  
                res.members.forEach(function(member){
                    user.findOne({_id:member.id}).exec(function(err,data){
                        if(data != null){
                            test.push(data.fullName);
                            socket.emit("circleUserslist",test)
                        }
                    })
                })
            }
        })
    })
}    