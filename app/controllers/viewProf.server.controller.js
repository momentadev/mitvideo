var mongoose = require('mongoose'),
    user = mongoose.model('User'),
    circle = mongoose.model('circles'),
    request = mongoose.model('request');
    friend = mongoose.model('friends');
 	connect = mongoose.model('connect');
//jijopaulose.branded.me
    module.exports = function(io,socket){

/*-----------------------------------add new request to database------------------------------*/

	    socket.on('addF',function(fReq){
		    request.find({$and:[{receiver:fReq.snd},{sender:fReq.rec}]}).exec(function(err,res){	
		    	if(res.length == 0){
			    		var af = new request({
						sender : fReq.snd,
						receiver : fReq.rec
					})
					af.save(function (err, data){
			 			if(!err){
				 			request.find({receiver:fReq.rec}).exec(function(err,result){
			           			connect.findOne({clientId:fReq.rec}).exec(function(err,res){
					                if(res!=null) {
					                   io.to(res.socketId).emit('requestCountLive',result.length);
					                }
					            })
			        		})
							var datar=[],b=[],j=0;
						    request.find({receiver:fReq.rec}).exec(function(err,resp){
						        if(resp.length>=1){
						            resp.forEach(function(x){
						                user.findOne({_id:fReq.snd}).exec(function(err,resp1){
						                    b[j++]=resp1;
						                    if(j>=resp.length){
						                        datar = b;
						                        connect.findOne({clientId:fReq.rec}).exec(function(err,res){
										    	    if(res!=null) {
							                         	io.to(res.socketId).emit('recReqData',datar);
							                        }
									            })
						                    }
						                })
						            })
						        }
						    })
			 			}
			 		})
		    	}else{
		    		user.findOne({_id:fReq.rec}).exec(function(err,result){
		    			if(!err){
		    				socket.emit("requestsendPending",result);
		    			}
		    		})
		    	}
		    })	
		})
	    socket.on('advancedAddF',function(fReq){
		    request.find({$and:[{receiver:fReq.rec},{sender:fReq.snd}]}).exec(function(err,res){	
		    	if(res.length == 0){
			    		var af = new request({
						sender : fReq.snd,
						receiver : fReq.rec
					})
					af.save(function (err, data){
			 			if(!err){
				 			request.find({receiver:fReq.rec}).exec(function(err,result){
			           			connect.findOne({clientId:fReq.rec}).exec(function(err,res){
					                if(res!=null) {
					                   io.to(res.socketId).emit('requestCountLive',result.length);
					                }
					            })
			        		})
							var datar=[],b=[],j=0;
						    request.find({receiver:fReq.rec}).exec(function(err,resp){
						        if(resp.length>=1){
						            resp.forEach(function(x){
						                user.findOne({_id:fReq.snd}).exec(function(err,resp1){
						                    b[j++]=resp1;
						                    if(j>=resp.length){
						                        datar = b;
						                        connect.findOne({clientId:fReq.rec}).exec(function(err,res){
										    	    if(res!=null) {
							                         	io.to(res.socketId).emit('recReqData',datar);
							                        }
									            })
						                    }
						                })
						            })
						        }
						    })
			 			}
			 		})
		    	}else{
		    		user.findOne({_id:fReq.rec}).exec(function(err,result){
		    			if(!err){
		    				socket.emit("PendingfromAdvacSearch",result);
		    			}
		    		})
		    	}
		    })	
		})



/*-----------------------------------check if already friends---------------------------------*/
		socket.on('get_friend',function(n){
			var flag = false;
			friend.findOne({$and:[{user:n.uid},{'friends.id':n.id}]}).exec(function(err,res){
				if(res!=null){
					flag = true;
					socket.emit('friend',flag);
				}
				else
					socket.emit('friend',flag);
			})
		})
/*------------------------- get profile details to be displayed -----------------------------*/

		socket.on('get_profInfo',function(n){
			user.findOne({_id:n.id}).exec(function(err,res){
				socket.emit('profInfo',res);
			})
		})

/*------------------------- check if already request sent or already friends ---------------*/

		socket.on('get_check',function(n){
			request.findOne({$and:[{sender: n.uid},{receiver: n.id}]}).exec(function(err,res){
				if(res==null){
					friend.findOne({$and:[{user:n.uid},{'friends.id':n.id}]}).exec(function(err,res){
						if(res==null)
							socket.emit('check',true)
						else
							socket.emit('check',false)
					})
				}
				else
					socket.emit('check',false)
			})
		})
    }