/*-----------------------------------------  Kurento user registy and other prototype functions ----------------------*/
function UserRegistry() {
    this.usersById = {};
    this.usersByName = {};
}
UserRegistry.prototype.register = function (user) {
    this.usersById[user.id] = user;
    this.usersByName[user.name] = user;
};
UserRegistry.prototype.unregister = function (id) {
    var user = this.getById(id);
    if (user)
        delete this.usersById[id];
    if (user && this.getByName(user.name))
        delete this.usersByName[user.name]
};
UserRegistry.prototype.getById = function (id) {
    return this.usersById[id];
};
UserRegistry.prototype.getByName = function (name){
    return this.usersByName[name];
};
UserRegistry.prototype.removeById = function (id) {
    var userSession = this.usersById[id];
    if (!userSession)
        return;
    delete this.usersById[id];
    delete this.usersByName[userSession.name];
};
UserRegistry.prototype.getUsersByRoom = function (room) {
    var userList = this.usersByName;
    var usersInRoomList = [];
    for (var i in userList) {
        if (userList[i].room === room) {
            usersInRoomList.push(userList[i]);
        }
    }
    return usersInRoomList;
};
function UserSession(id, roomName) {
    this.id = id;
    this.outgoingMedia = null;
    this.incomingMedia = {};
    this.roomName = roomName;
    this.iceCandidateQueue = {};
}
UserSession.prototype.addIceCandidate = function (data, candidate) {
    if (data.sender === this.id) {
        if (this.outgoingMedia) {
            this.outgoingMedia.addIceCandidate(candidate);
        } else {
            this.iceCandidateQueue[data.sender].push({
                data: data,
                candidate: candidate
            });
        }
    } else {
        var webRtc = this.incomingMedia[data.sender];
        if (webRtc) {
            webRtc.addIceCandidate(candidate);
        } else {
            if (!this.iceCandidateQueue[data.sender]) {
                this.iceCandidateQueue[data.sender] = [];
            }
            this.iceCandidateQueue[data.sender].push({
                data: data,
                candidate: candidate
            });
        }
    }
};
/*-------------------------------------------------------------- Registy and prototype functions end here -----------------*/
var userRegistry = new UserRegistry();
var rooms = {};
var path = require('path');
var minimist = require('minimist');
var url = require('url');
var async   = require("async");
var kurento = require('kurento-client');
var connect = require('mongoose').model('connect');
var schedule = require('mongoose').model('confrenceschedule');
var user    = require('mongoose').model('User');
var argv = minimist(process.argv.slice(2), {
    default: {
        ws_uri: 'ws://212.116.73.202:8888/kurento'
        //ws_uri: 'ws://192.168.1.149:8888/kurento'
    }
});
var kurentoClient = null;
module.exports = function(io,socket){
/*------------------------------------------  Schedule Confrence Listing ---------------------------------*/
    //socket.emit("editConfrenceResult",id);
    socket.on('editConfrence',function(id){
        var arr = []
        schedule.find({_id:id}).exec(function(err,resp){    
            if(resp!=null){
                resp.forEach(function(mem){
                    mem.members.forEach(function(memberArray){
                        user.findOne({_id : memberArray.id}).exec(function(err,res){
                            var userData={participantId:res._id,name:res.fullName,mailid:res.email,confId:resp[0]._id,confName:resp[0].subject,
                                          date:resp[0].date,time:resp[0].times}
                            arr.push(userData)
                            socket.emit("editConfrenceResult",arr);
                        })
                    })
                }); 
            }    
        });
    })
    // find user details
    socket.on('userDetails',function(data){
        var arra = [];
        data.forEach(function(participant){
                user.findOne({_id : participant}).exec(function(err,res){
                    arra.push(res);
                    socket.emit('userDetailsResponse',arra);
                })
        })
    })     
    /*------------------------ adding new  persons to the live call -----------------------------------*/
    // add new user to an active call 
    socket.on('newuserAdd',function(data){
        var invite = {id:data.addedBy._id,name:data.addedBy.fullName,roomName:data.roomName,circleID:data.circleID}
        data.participants.forEach(function(participant){
            connect.findOne({clientId:participant}).exec(function(err,res){
                if(res!=null) {
                   io.to(res.socketId).emit('conferenceInvite',invite);
                }
            })
        })
    })
    /*------------------------------------ accept the call and end the calling button ----------------------------*/
    socket.on('conferenceInviteAcepted',function(data){
        connect.findOne({clientId:data.id}).exec(function(err,res){
            if(res!=null) {
               io.to(res.socketId).emit('confAccepted',data);
            }
        })
    })

    /*------------------------------------ end a specific call from the calle side ----------------------------*/
   socket.on('endspecificCall',function(id){
        connect.findOne({clientId:id}).exec(function(err,res){
            if(res!=null) {
               io.to(res.socketId).emit('endsingleAddedcall');
            }
        })
   })
    // send confrence details 
    socket.on('confrenceDetails', function (data){ 
        var newSchedule = new schedule({
            creator: data.creator._id,
            createddate:Date.now(),
            date:data.confdate,
            circleid:data.circleID,
            subject: data.name,
            members: data.members
        })
        newSchedule.save(function (err, data) {
            if(!err){
                socket.emit("schedulesaved",data);
            }else{
                console.log("err",err)
            }
        })
    })
    // get active confrence details
    socket.on('getConfdetails', function(ids){
        schedule.find({$and:[{creator:ids},{eventSatus:""}]}).exec(function(err,resp){    
            if(resp!=null){
                socket.emit('receiveConfDetails',resp)
            }    
        });
    })
    // get expired confrence details
    socket.on('getexpConfdetails', function(ids){
        schedule.find({$and:[{creator:ids},{$or: [ { eventSatus: "Confrence Finished"}, { eventSatus: "Admin Rejected" },{ eventSatus: "Conference Expired" } ] }]}).exec(function(err,resp){    
            if(resp!=null){
                socket.emit('receiveexpConfDetails',resp);
            }    
        });
    })
    // remove both active & expired confrence
    socket.on('removeConfrence',function(data){
        schedule.remove({_id: data}, function(err) {
            if(!err)
            socket.emit('confrenceDeleteResponse');
        });
    });
    // get participant details for schedule confrence list
    socket.on('getParticipants',function(data){
        var arr = [];
        schedule.find({_id:data}).exec(function(err,resp){    
            if(resp!=null){
                resp.forEach(function(mem){
                    mem.members.forEach(function(memberArray){
                        user.findOne({_id : memberArray.id}).exec(function(err,res){
                            var userData={name:res.fullName,mailid:res.email}
                            arr.push(userData)
                            socket.emit('participants',arr)
                        })
                    })
                }); 
            }    
        });
    })
/*------------------------------------------  Schedule Confrence Listing  End Here ---------------------------------*/
    // update schedule confrence status 
    socket.on('scheduleConfReject', function(data){
        schedule.update({_id:data.id}, {$set: {eventSatus:data.status}},function(err){
        })   
    })
    // checking the schedule confrences ,if schedule date and time comes send a confirm alert to the confrence admin
    setInterval(function(){
            var querys = schedule.find({eventSatus : ""});
            querys.exec(function(err,result){
                async.each(result, function(res, callback){
                    var dbDate = res.date.getDate() + '/' + (res.date.getMonth() + 1)  + '/' +  res.date.getFullYear();
                    var cuDate = new Date();
                    var cutDate = new Date(cuDate.getFullYear(),cuDate.getMonth(),cuDate.getDate(),cuDate.getHours(),cuDate.getMinutes());
                    if(new Date(res.date).getTime() == cutDate.getTime()){
                        console.log("if");
                        connect.findOne({clientId:res.creator}).exec(function(err,result){
                            if(result != null) {
                                io.to(result.socketId).emit('scheduleconferenceInvite',res);
                            }
                        })
                    }else if(new Date(res.date).getTime() < cutDate.getTime()) {
                        console.log("else");
                        schedule.update({_id:res._id}, {$set: {eventSatus:"Conference Expired"}},function(err){
                        })   
                    }
                })
            });     
    }, 60000);
    var userList = '';
    for (var userId in userRegistry.usersById) {
        userList += ' ' + userId + ',';
    }
    // disconnect confrence call
    socket.on('disconnectCall', function (uid) {
        console.log("------------------------------------- disconnectCall ---------------------------",uid)
        leaveRoom(io,uid,function (){
        });
    });
    // if user declined the call
    socket.on('userDeclined', function (data){
        connect.findOne({clientId:data.to}).exec(function(err,res){
            if(res!=null) {
                io.to(res.socketId).emit('userDeclinedMsg',data.name);
            }
        })
    }); 
    // checking in call status 
    socket.on('inCall', function (data){
        connect.findOne({clientId:data.to}).exec(function(err,res){
            if(res!=null) {
                io.to(res.socketId).emit('inCAllReject',data.name);
            }
        })
    });
    // socket.on('onConfcall', function (data){
    //     console.log("onConfcall",data.from._id);
    //     // connect.findOne({clientId:data.to}).exec(function(err,res){
    //     //     if(res!=null) {
    //     //         io.to(res.socketId).emit('inCAllReject',data.name);
    //     //     }
    //     // })
    // });

    // message handling
    socket.on('message', function (message){
        switch (message.id) {
            case 'joinRoom':
                joinRoom(io, message.sessionId, message.roomName, function () {
                });
                break;
            case 'createRoom':
                createRoom(io, message.roomName,message.admin._id ,function () {
                });
                invite(io,message.admin, message.members,message.roomName,message.circleID,function (){})
                break;
            case 'receiveVideoFrom':
                receiveVideoFrom(io, message.user, message.sender, message.sdpOffer, function () {
                });
                break;
            case 'onIceCandidate':
                addIceCandidate(message);
                break;
            default:
                socket.emit({id: 'error', message: 'Invalid message ' + message});
        }
    });
};

/*--------------------------------------------------------- Kurento Functions Starts Here ---------------------------*/
// create a new room and join to that room 
function createRoom(io, roomName, user,callback) {
    newRoom(roomName, function (error, room) {
        if (error) {
            callback(error)
        }
        join(io, user, room, function (error, user) {
        });
    });
}
// Generating a new room with the help of kurento media server(pipeline generation through kurento) 
function newRoom(roomName, callback) {
    getKurentoClient(function (error, kurentoClient) {
        if (error) {
            return callback(error);
        }
        kurentoClient.create('MediaPipeline', function (error, pipeline) {
            if (error) {
                return callback(error);
            }
            room = {
                name: roomName,
                pipeline: pipeline,
                participants: {}
            };
            rooms[roomName] = room;
            callback(null, room);
        });
    });
}
// if the room is existing add to that room
function joinRoom(io, user, roomName, callback){
    getRoom(roomName, function (error, room) {
        if (error) {
            callback(error)
        }
        join(io, user, room, function (error, user) {
        });
    });
}
function getRoom(roomName, callback) {
    var room = rooms[roomName];
    if (room == null) {
    } else {
        callback(null, room);
    }
}
// join to the confrence call
function join(io, user, room, callback) {
     // create user session
    var userSession = new UserSession(user, room.name);
    room.pipeline.create('WebRtcEndpoint', function (error, outgoingMedia) {
        if (error) {
             // no participants in room yet release pipeline
            if (Object.keys(room.participants).length == 0) {
                room.pipeline.release();
            }
            return callback(error);
        }
        userSession.outgoingMedia = outgoingMedia;
        // add ice candidate the get sent before endpoint is established
        var iceCandidateQueue = userSession.iceCandidateQueue[user];
        if (iceCandidateQueue) {
            while (iceCandidateQueue.length) {
                var message = iceCandidateQueue.shift();
                userSession.outgoingMedia.addIceCandidate(message.candidate);
            }
        }
        userSession.outgoingMedia.on('OnIceCandidate', function (event) {
            var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
            connect.findOne({clientId:userSession.id}).exec(function(err,res){
                if(res!=null){
                    io.to(res.socketId).emit('message',{
                        id: 'iceCandidate',
                        sessionId: userSession.id,
                        candidate: candidate
                    })
                }
            })
        });
        // notify other user that new user is joining
        var usersInRoom = room.participants;
        var data = {
            id: 'newParticipantArrived',
            new_user_id: userSession.id
        };
        // notify existing user
        for (var i in usersInRoom) {
            connect.findOne({clientId:usersInRoom[i].id}).exec(function(err,res){
                if(res!=null){
                    io.to(res.socketId).emit('message',data);
                }
            })
        }
        var existingUserIds = [];
        for (var i in room.participants) {
            existingUserIds.push(usersInRoom[i].id);
        }
        // send list of current user in the room to current participant
        connect.findOne({clientId:userSession.id}).exec(function(err,res){
            if(res!=null){
                io.to(res.socketId).emit('message',{
                    id: 'existingParticipants',
                    data: existingUserIds,
                    roomName: room.name
                })
            }
        })
        // register user to room
        room.participants[userSession.id] = userSession;
        // register user in system
        userRegistry.register(userSession);
        callback(null, userSession);
    });
}
// send invites to the members 
function invite(io, admin, members,roomName,circleID){
    var invite = {id:admin._id,name:admin.fullName,roomName:roomName,circleID:circleID}
    members.forEach(function(member) {
        connect.findOne({clientId:member}).exec(function(err,res){
            if(res!=null) {
               io.to(res.socketId).emit('conferenceInvite',invite);
            }
        })
    })
}
// receive video from a client
function receiveVideoFrom(io, user, senderId, sdpOffer, callback) {
    var userSession = userRegistry.getById(user);
    var sender = userRegistry.getById(senderId);
    getEndpointForUser(io, userSession, sender, function (error, endpoint) {
        if (error) {
            callback(error);
        }
        endpoint.processOffer(sdpOffer, function (error, sdpAnswer) {
            if (error) {
                return callback(error);
            }
            var data = {
                id: 'receiveVideoAnswer',
                sessionId: sender.id,
                sdpAnswer: sdpAnswer
            };
            connect.findOne({clientId:userSession.id}).exec(function(err,res){
                if(res!=null){
                    io.to(res.socketId).emit('message',data);
                }
            })
            endpoint.gatherCandidates(function (error) {
                if (error) {
                    return callback(error);
                }
            });
            return callback(null, sdpAnswer);
        });
    });
}
// generate end points for users
function getEndpointForUser(io, userSession, sender, callback) {
    // request for self media
    if (userSession.id === sender.id) {
        callback(null, userSession.outgoingMedia);
        return;
    }
    var incoming = userSession.incomingMedia[sender.id];
    if (incoming == null) {
        //console.log('user : ' + userSession.id + ' create endpoint to receive video from : ' + sender.id);
        getRoom(userSession.roomName, function (error, room) {
            if (error) {
                return callback(error);
            }
            room.pipeline.create('WebRtcEndpoint', function (error, incomingMedia) {
                if (error) {
                     // no participants in room yet release pipeline
                    if (Object.keys(room.participants).length == 0) {
                        room.pipeline.release();
                    }
                    return callback(error);
                }
                userSession.incomingMedia[sender.id] = incomingMedia;

                // add ice candidate the get sent before endpoint is established
                var iceCandidateQueue = userSession.iceCandidateQueue[sender.id];
                if (iceCandidateQueue) {
                    while (iceCandidateQueue.length) {
                        var message = iceCandidateQueue.shift();
                        //console.log('user : ' + userSession.id + ' collect candidate for : ' + message.data.sender);
                        incomingMedia.addIceCandidate(message.candidate);
                    }
                }
                incomingMedia.on('OnIceCandidate', function (event) {
                   //console.log("generate incoming media candidate : " + userSession.id + " from " + sender.id);
                    var candidate = kurento.register.complexTypes.IceCandidate(event.candidate);
                    connect.findOne({clientId:userSession.id}).exec(function(err,res){
                        if(res!=null){
                            io.to(res.socketId).emit('message',{
                                id: 'iceCandidate',
                                sessionId: sender.id,
                                candidate: candidate
                            })
                        }
                    })
                });
                sender.outgoingMedia.connect(incomingMedia, function (error) {
                    if (error) {
                        callback(error);
                    }
                    callback(null, incomingMedia);
                });

            });
        });
    } else {
        //console.log('user : ' + userSession.id + ' get existing endpoint to receive video from : ' + sender.id);
        sender.outgoingMedia.connect(incoming, function (error) {
            if (error) {
                callback(error);
            }
            callback(null, incoming);
        });
    }
}
//Leave a participant from the room
function leaveRoom(io,uid, callback) {
    var userSession = userRegistry.getById(uid);
    if (!userSession) {
        return;
    }
    var room = rooms[userSession.roomName];
    //console.log('notify all user that ' + userSession.id + ' is leaving the room ' + room.name);
    var usersInRoom = room.participants;
    delete usersInRoom[userSession.id];
    userSession.outgoingMedia.release();
     // release incoming media for the leaving user
    for (var i in userSession.incomingMedia) {
        userSession.incomingMedia[i].release();
        delete userSession.incomingMedia[i];
    }
    var data = {
        id: 'participantLeft',
        sessionId: userSession.id
    };
    for (var i in usersInRoom) {
        var user = usersInRoom[i];
        // release viewer from this
        user.incomingMedia[userSession.id].release();
        delete user.incomingMedia[userSession.id];
        connect.findOne({clientId:user.id}).exec(function(err,res){
            if(res!=null){
                io.to(res.socketId).emit('message',data)
            }
        })
    }
    // notify all user in the room
    connect.findOne({clientId:uid}).exec(function(err,res){
        if(res!=null){
            io.to(res.socketId).emit('participantsRemoved');
        }
    })
    stop(userSession.id);
}
// stop the confrence call for a particular client
function stop(sessionId) {
    userRegistry.unregister(sessionId);
}
function addIceCandidate(message) {
    var user = userRegistry.getById(message.user);
    if (user != null) {
        var candidate = kurento.register.complexTypes.IceCandidate(message.candidate);
        user.addIceCandidate(message, candidate);
    } else {
       //console.error('ice candidate with no user receive : ' + socket.id);
    }
}
// Recover kurentoClient for the first time.
function getKurentoClient(callback) {
    if (kurentoClient !== null) {
        return callback(null, kurentoClient);
    }
    kurento(argv.ws_uri, function (error, _kurentoClient) {
        if (error) {
            var message = 'Coult not find media server at address ' + argv.ws_uri;
            console.log(message + ". Exiting with error " + error)
            return callback(message + ". Exiting with  " + error);
        }
        kurentoClient = _kurentoClient;
        callback(null, kurentoClient);
    });
}
/*--------------------------------------------------------- Kurento Functions Ends Here ---------------------------*/

