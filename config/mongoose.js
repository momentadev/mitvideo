var config = require('./config'),
    mongoose = require('mongoose');
module.exports = function() {
  	var db = mongoose.connect(config.db);
  	// Modules Required
  	require('../app/models/user.server.model');
  	require('../app/models/circle.server.model');
  	require('../app/models/friend.server.model');
  	require('../app/models/request.server.model');
  	require('../app/models/connected.server.model');
    require('../app/models/messages.server.model');
    require('../app/models/file.server.model');
    require('../app/models/scheduleconfrence.server.model');
  	return db;
};