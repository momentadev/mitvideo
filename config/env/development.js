module.exports = {
  	// Development configuration options
  	db: 'mongodb://localhost/MITVideoChats',
  	sessionSecret: 'developmentSessionSecret',
  	sessionCollection : 'session'
};