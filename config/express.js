//Required Configurations
var config = require('./config');
var express = require('express'),
  	http = require('http'),
    socketio = require('socket.io'),
  	morgan = require('morgan'),
  	compress = require('compression'),
  	bodyParser = require('body-parser'),
  	path = require('path'),
  	methodOverride = require('method-override'),
  	session = require('express-session'),
  	MongoStore = require('connect-mongo')(session),
  	flash = require('connect-flash'),
  	passport = require('passport');
var fs    = require('fs');
var https = require('https');
var os = require("os"); 
    // sudo apt-get install unoconv
    // sudo apt-get install libreoffice
    // sudo apt-get install ffmpeg
module.exports = function(db) {
  	var app = express();
    var options = {
                    key:  fs.readFileSync('./config/keys/server.key'),
                    cert: fs.readFileSync('./config/keys/server.crt')
                  };
    var server = https.createServer(options,app).listen(3000,function(){
      console.log('Free memory : ' + (os.totalmem()/1024/1024).toFixed()+"/"+(os.freemem()/1024/1024).toFixed()+ " MB.");
        //console.log(os.cpus())
      console.log('Server running at https://'+os.networkInterfaces().eth0[0].address+':' + server.address().port);
    });
   	var io = socketio.listen(server);
   	// Running Environment
  	// if (process.env.NODE_ENV === 'development') {
	  //   app.use(morgan('dev'));
	  // } else if (process.env.NODE_ENV === 'production') {
	  //   app.use(compress());
	  // }
	  // Middlewares

  	app.use(bodyParser.urlencoded({
    	extended: true
  	}));
  	app.use(bodyParser.json());
  	app.use(methodOverride());
  	//Sessions management
  	var mongoStore = new MongoStore({
	    url : config.db,
	    collection : config.sessionCollection,
	    autoRemove: 'native' // Default
	  });
   	app.use(session({
	    saveUninitialized: true,
	    resave: true,
	    secret: config.sessionSecret,
	    store: mongoStore
	  }));
  	// Passport
  	app.use(flash());
  	app.use(passport.initialize());
  	app.use(passport.session());
	  // view engine setup
  	app.set('views', path.join(__dirname, '../app/views'));
  	app.engine('html', require('ejs').renderFile);
  	app.set('view engine', 'html');
	   // Serving static files
	  app.use(express.static(path.join(__dirname, '../public')));
	   // Required routes
  	require('../app/routes/index.server.routes.js')(app);
  	require('../app/routes/landing.server.routes.js')(app);
  	require('../app/routes/user.server.routes.js')(app);
	  	// Required Socket Config
  	require('./socketio')(server, io, mongoStore);
  	return app;
};
