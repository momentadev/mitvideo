var config = require('./config'),
  cookieParser = require('cookie-parser'),
  passport = require('passport');
var User = require('mongoose').model('User'),
    Connect = require('mongoose').model('connect');
module.exports = function(server, io, mongoStore) {
    io.use(function(socket, next) {
        cookieParser(config.sessionSecret)(socket.request, {}, function(err) {
            var sessionId = socket.request.signedCookies['connect.sid'];
            mongoStore.get(sessionId, function(err, session) {
                socket.request.session = session;
                passport.initialize()(socket.request, {}, function() {
                    passport.session()(socket.request, {}, function() {
                        if (socket.request.user) {
                            next(null, true);
                        } else {
                            next(new Error('User is not authenticated'), false);
                        }
                    })
                });
            });
        });
    });
    Connect.remove({}, function(err) {});
    User.update({},{status:"offline"},{safe:true,multi:true},function(err){});

    io.on('connection', function(socket) {
        require('../app/controllers/chat.server.controller')(io, socket);
        require('../app/controllers/header.server.controller')(io, socket);
        require('../app/controllers/circle.server.controller')(io, socket);
        require('../app/controllers/viewProf.server.controller')(io, socket);
        require('../app/controllers/request.server.controller')(io, socket);
        require('../app/controllers/friend.server.controller')(io, socket);
        require('../app/controllers/fileshare.server.controller')(io, socket);
        require('../app/controllers/sidebar.server.controller')(io,socket);
        require('../app/controllers/videoCall.server.controller')(io,socket);
        require('../app/controllers/conferenceCall.server.controller')(io,socket);
        
      /*-------------------------------setting connected user status-------------------------------*/
        socket.on('setStatus',function(id){
             var sessionId = socket.request.signedCookies['connect.sid'];
            var c=new Connect({
                socketId : socket.id,
                clientId : id
            })
            c.save(function (err, data) {
                if (err) console.log(err);
            });
            User.findOneAndUpdate(
                {_id: id},
                {status: "online",lastLogin:new Date()},
                {safe: true, new: true},
                function(err, model) {
                console.log("------------------------------------- setStatus --------------------------------",model.fullName,model.status,new Date()) 
                if(err)
                  console.log(err);
                }
            );
            io.emit('statChange',id);
        })

    /*------------------------------setting disconnected user status------------------------------*/
        socket.on('disconnect',function(data){
            Connect.findOne({socketId : socket.id}).exec(function(err,res) {
                if(!err&&res!=null)
                User.findOneAndUpdate(
                    {_id: res.clientId},
                    {status: "offline"},
                    {safe: true, new: true},
                    function(err, model) {
                        console.log("------------------------------------- disconnect -------------------------------",model.fullName,model.status,new Date());
                        Connect.findOne({socketId : socket.id}).remove().exec()
                        io.emit('statChange',res.clientId);
                        if(err)
                        console.log(err);
                    }
                );
            })
        })
    });
};