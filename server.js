// Set Environment Variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Node Modules Required
var mongoose = require('./config/mongoose'),
	express  =  require('./config/express'),
	passport = require('./config/passport');
// Objects
var db = mongoose();
var app = express(db);
//console.log(app)
var passport = passport();

module.exports = app;

//console.log('Server running at https://localhost:3000/');