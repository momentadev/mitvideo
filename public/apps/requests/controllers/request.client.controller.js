angular.module('requests').controller('ReqController', ['$scope','$rootScope','$timeout','$cookies','$location','Socket', 'UserDetails',
  	function($scope,$rootScope,$timeout,$cookies,$location, Socket, UserDetails) {
  		$rootScope.$emit('noSel')
		Socket.emit('getRecReqData',UserDetails.user._id);
		Socket.emit('getSntReqData',UserDetails.user._id);
		
		Socket.on('recReqData',function(data){
			$scope.recItems = data;
			$scope.$apply();
		})
		Socket.on('sntReqData',function(data){
			$scope.sntItems = data;
			$scope.$apply();
		})

		// acccept friends request
		$scope.acceptReq=function(id){
			var data1 = {
				u : UserDetails.user._id,
				c : id
			} 
			Socket.emit('acceptReq',data1);
			Socket.emit('removeReq',data1);
			Socket.on('acceptedReq',function(data){
				Socket.emit('get_friends_sidebar',UserDetails.user._id);
			})
			// Socket.on('acceptedReq',function(data){
			// 	Socket.emit('get_friends_sidebar',UserDetails.user._id);
			// })
		}
		// reject friends request
		$scope.rejectReq=function(id){
			var data2 = {
				u : UserDetails.user._id,
				c : id
			}
			Socket.emit('removeRequest',data2);
		}
		Socket.on('removedReq',function(data){
			Socket.emit('getRecReqData',UserDetails.user._id);
		})
		
	}
])