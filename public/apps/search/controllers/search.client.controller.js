angular.module('search').controller('searchController', ['$scope','$mdDialog','$cookies','$location','Socket', 'UserDetails',
  	function($scope,$mdDialog,$cookies,$location, Socket, UserDetails) {
	$scope.users = [];
	$scope.uid = UserDetails.user._id;
	$scope.name = "";
  Socket.emit('getFriends',UserDetails.user._id)
  Socket.on('friendsList',function(data){
   $scope.friends=[];
      if(data.length>=1){
        data.forEach(function(dat){
          $scope.friends.push(dat);
          console.log("friends",$scope.friends);
          $scope.$apply();
        })
      }
  })
	// search with name user name
	$scope.fn_srch = function(name){
		if(name.length>=1){
			var str=name.split(" ");
				$scope.users=[];
				if(str.length==1)
					Socket.emit('txt',str[0]); 
				else
					Socket.emit('srch',str);
					Socket.once('res',function(res){
						if(res.length>=1){
              res.forEach(function(dat){
                $scope.users.push(dat);
                $scope.friends.forEach(function(frnd){
                  console.log("sep-----",frnd);
                })
                console.log("users",$scope.users);
                $scope.$apply();
              })
						}
					});
		}else{
			$scope.users = [];
			$scope.desg = "";
			$scope.cmp = "";
		}
  }
  // search with  designation name
	$scope.fn_srchDesig =function(desg){
		if(desg.length>=1){
				$scope.users=[];
				var strDesg=$scope.name.split(" ");
					if(strDesg.length==1){
						var srchStr={name:strDesg[0],des:$scope.desg}
						Socket.emit('srchDesg',srchStr);
					}else{
						var srchStr={name:strDesg,des:$scope.desg}
						Socket.emit('txtDesg',srchStr);
					}
					Socket.once('resDesg',function(res){
						if(res.length>=1){
							res.forEach(function(dat){
								$scope.users.push(dat);
								$scope.$apply();
							})
						}
					});
		}else{
			$scope.users=[];
			$scope.fn_srch($scope.name);
		}
	}
	// search with company name
	$scope.fn_srchCmpny =function(cmp){
			if(cmp.length>=1){
						var strCmpny=$scope.name.split(" ");
						$scope.users=[];
						if(strCmpny.length==1){
							var strCmp={name:strCmpny[0],des:$scope.desg,cmpny:cmp}
								Socket.emit('srchCmpny',strCmp);
						}else{
							var strCmp={name:strCmpny,des:$scope.desg,cmpny:cmp}
							Socket.emit('txtCmpny',strCmp);
						}
						Socket.once('resCmpny',function(res){
							if(res.length>=1){
								res.forEach(function(dat){
									$scope.users.push(dat);
									$scope.$apply();
								})
							}
						});
			}else{
				$scope.users=[];
				$scope.fn_srchDesig($scope.desg);
			}
	}
	// redirect to view profile
	$scope.fn_View = function(id){
		sessionStorage['id'] = id;
		$location.path('/viewProf');
  	}
  	$scope.fn_AddFriend = function(evt,id){
  		var newReq = {
    			snd: UserDetails.user._id,
    			rec: id
    		}
        Socket.emit('advancedAddF',newReq);
  	}
  	Socket.on('PendingfromAdvacSearch',function(data){
  		var alert = {
                    title : "Request Pending",
                    content : "You Already Send Friend Request to  "+ data.fullName+" ." 
                  }
      $scope.showAlert(alert);
  	})
  	$scope.showAlert = function(alert) {
        $mdDialog.show({
            locals: {alert:alert},
            controller: DialogController,
            templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true
        })
    };
	 // dialog controller
    function DialogController($scope, $mdDialog, alert) {
        $scope.alertData = alert
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(answer) {
                $mdDialog.hide(answer);
        };
    }
    $scope.$on('$destroy', function() {
        Socket.getSocket().removeAllListeners('PendingfromAdvacSearch');
    })
    $("#withName").keypress(function(event){
      var ew = event.which;
      if(ew == 32)
          return true;
      if(48 <= ew && ew <= 57)
          return true;
      if(65 <= ew && ew <= 90)
          return true;
      if(97 <= ew && ew <= 122)
          return true;
      return false;
    });

    $("#withDesg").keypress(function(event){
      var ew = event.which;
      if(ew == 32)
          return true;
      if(48 <= ew && ew <= 57)
          return true;
      if(65 <= ew && ew <= 90)
          return true;
      if(97 <= ew && ew <= 122)
          return true;
      return false;
    });

    $("#withCmp").keypress(function(event){
      var ew = event.which;
      if(ew == 32)
          return true;
      if(48 <= ew && ew <= 57)
          return true;
      if(65 <= ew && ew <= 90)
          return true;
      if(97 <= ew && ew <= 122)
          return true;
      return false;
    });
}]);	

