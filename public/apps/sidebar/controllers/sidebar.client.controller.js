angular.module('sidebar').controller('SidebarController', ['$scope','$mdDialog','$interval','$rootScope','$location','$timeout','$mdSidenav','UserDetails','Socket',
    function ($scope,$mdDialog,$interval,$rootScope,$location,$timeout,$mdSidenav,UserDetails,Socket) {
    $scope.contacts = [];
    $scope.ongoingVideoCall = false;
    $scope.currentStatus = "online";
    $scope.setAwayText = "Set Away";
    $scope.ringTone = $("#myAudio")[0];
    sessionStorage['conferenceInProgress'] = 'false';
    sessionStorage['videoInProgress'] = 'false';
    var path = $location.path();
    var x = path.split("/");
    Socket.emit("removeOldMsg");
    $scope.toggleLeft = function(){$mdSidenav('left').toggle() ;}
    $scope.selection = {
        selectedNode : null
    }
    if(sessionStorage['selected']!=null){
        var id = x[2];
        if(!($location.path() == '/chat/'+id+''||$location.path()=='/conferenceRooms/'+id+'')){
            sessionStorage['selected'] = null;
        }else{
            $scope.selection = {
                selectedNode : x[2]
            }
            sessionStorage['selected'] = x[2];
            sessionStorage['chatId'] = x[2];
        }
    }else{
        $scope.selection = {
            selectedNode : null
        }
    }
    $rootScope.$on('noSel',function(){
        $timeout(function(){$scope.selection.selectedNode = null});
    })
    $rootScope.$on('setSel',function(ev, id){
        $timeout(function(){
            $scope.selection.selectedNode = id;
        })
    })
    Socket.on('updatedPicture',function(res){
          var alert = {
                    title : "Updated",
                    content : res
                }
          $scope.showAlert(alert);
            Socket.emit('getPic',UserDetails.user._id);
            Socket.on('pic',function(res){
                $rootScope.$emit('updtprofPic',res);
                UserDetails.user = res;
                $scope.profPic = res.profilePicture;
                $scope.$apply();
            })
    })
    var frndList = [];
    this.user = UserDetails.user;
    $scope.profPic = UserDetails.user.profilePicture;
    // redirect to home
    $scope.fn_Home = function(){
        $location.path('/');
    }
    $scope.$on('$destroy', function() {
        Socket.getSocket().removeAllListeners('profDeatailsresp');
        Socket.getSocket().removeAllListeners('updatedPicture');
    })
    $scope.setAway = function(){
        Socket.emit("profileDetails",UserDetails.user._id);
        Socket.once('profDeatailsresp',function(data){
            if(data.status == "online"){
                var chStatus = {
                    uid:UserDetails.user._id,        
                    status:"offline"
                }
                $scope.currentStatus = "offline";
                $scope.setAwayText = "Set  Active";
                $scope.$apply();
                Socket.emit("changeStatus",chStatus);
            }else{
                var changStatus = {
                    uid:UserDetails.user._id,        
                    status:"online"
                }
                $scope.currentStatus = "online";
                $scope.setAwayText = "Set Away";
                $scope.$apply();
                Socket.emit("changeStatus",changStatus);
            }
        })
    }
    $scope.deleteAccount = function () {
         var confirm = $mdDialog.confirm()
            .title('Remove Acoount')
            .content('Delete this account?')
            .ok('Yes')
            .cancel('No')
            $mdDialog.show(confirm).then(function(){
                Socket.emit("deleteAccount",UserDetails.user._id);
            }, function() {
                $mdDialog.hide();
            });
    }
    $scope.sigout = function () {
        console.log("signout");
        if (sessionStorage['conferenceInProgress'] == 'false' && sessionStorage['videoInProgress'] == 'false') { 
             window.location="/signout";
        }else{
             var alert = {
                title : "Active Call",
                content : "You are on a active call.Please end the call and signout"
            }
            $scope.showAlert(alert);
        }     
        // ng-href="/signout"
    }

    Socket.on('accountDeleted', function(){
            var alert = {
                title : "Account Deleted",
                content : "Account Deleted Succesfully"
            }
            $scope.showAlert(alert);
            setInterval(function(){
                 window.location="/";
            }, 1000);
    });
    // side  bar  details
    Socket.emit("pendingRequests",UserDetails.user._id);
    Socket.on('requestCount',function(count){
        $scope.requestCount = count;
        $scope.$apply();
    })
    Socket.on('requestCountLive',function(count){
         $scope.requestCount = count;
         $scope.$apply();
    })
    Socket.emit('get_circles_sidebar',UserDetails.user._id);
    Socket.emit('get_friends_sidebar',UserDetails.user._id);
    Socket.on('friends_sidebar',function(data){
        frndList = data;
        $scope.contacts = [];
        if(data.length>=1){
            var i=0;
            $scope.got_online_contacts = false;
            data.forEach(function(dat){
                i++;
                if(dat.status == 'online' || dat.status == 'offline'){
                    var onlineContact = {
                        _id : dat._id,
                        username : dat.username,
                        fullName : dat.fullName,
                        status   : dat.status,
                        unreadMsgCount : 0,
                        new : false
                    }
                    if(dat.unreadMsg != 0){
                        onlineContact.new = true;
                        onlineContact.unreadMsgCount = dat.unreadMsg;
                    }
                    $scope.got_online_contacts = true;
                    $scope.contacts.push(onlineContact);
                }
                if(i>=data.length){
                    $scope.$apply();
                }
            })
        }
        else{
            $scope.$apply(function(){
                $scope.got_online_contacts = false;
                $scope.contacts = data;
            });
        }
    })
    Socket.on('circles_sidebar',function(data){
        $scope.conferenceRooms = [];
        if(data.length>=0){
            var i=0;
            $scope.got_circles = true;
            data.forEach(function(dat){
                var grp = {
                    _id : dat._id,
                    circleName : dat.circleName,
                    unreadMsgCount : 0,
                    new : false
                }
                if(dat.unreadMsg != 0){
                    grp.new = true;
                    grp.unreadMsgCount = dat.unreadMsg;
                }
                $scope.conferenceRooms.push(grp);
                i++;
                if(i>=data.length){
                    $scope.$apply();
                }
            })
        }
    })
    Socket.on('recMsg',function(data){
        var path = $location.path();
        var x = path.split("/")
        if(x[2] != data.sender){
            var i=0;
            $scope.contacts.forEach(function(contact){
                if(contact._id == data.sender){
                    $scope.$apply(function(){
                        $scope.contacts[i].new = true;
                        $scope.contacts[i].unreadMsgCount++;
                    })
                }
                i++;
            })
        }
    })
    // 
    Socket.on('recGrpMsg',function(data){
        var path = $location.path();
        var x = path.split("/")
        if(x[2] != data.receiver){
            var i=0;
            $scope.conferenceRooms.forEach(function(conferenceRoom){
                if(conferenceRoom._id == data.receiver){
                    $scope.$apply(function(){
                        $scope.conferenceRooms[i].new = true;
                        $scope.conferenceRooms[i].unreadMsgCount++;
                    })
                }
                i++;
            })
        }
    })
    // Pass the details about the new incoming call  details to chat controller
    Socket.on('newIncomingVideoCall',function(message) {
        if (sessionStorage['conferenceInProgress'] == 'false') {
            sessionStorage['selected'] = message.from._id;
            sessionStorage['chatId'] = message.from._id;
            $scope.ringTone.play();
            if(message.id == 'incomingCall'){
                var confirm = $mdDialog.confirm()
                    .title('Video Call')
                    .content('User ' + message.from.fullName + ' is calling you. Do you accept the call?')
                    .ok('Yes!')
                    .cancel('No!')
                $mdDialog.show(confirm).then(function() {
                        $scope.ringTone.pause();
                        sessionStorage['selected'] = message.from._id;
                        sessionStorage['chatId'] = message.from._id;
                        $location.path('/chat/'+message.from._id+'');
                        var promise = $interval(function(){
                            if($location.path() == '/chat/'+message.from._id+''){
                                $rootScope.$emit('incomingVideoCall',message);
                                $interval.cancel(promise);
                            }
                        },2000);
                } , function() {
                     $scope.ringTone.pause();
                        $mdDialog.hide();
                        var response = {
                            id : 'incomingCallResponse',
                            from : message.from._id,
                            to : UserDetails.user._id,
                            callResponse : 'reject',
                            message : 'user declined'
                        };
                        $scope.sendMessage(response);
                });        
            }
        }else{
            var response = {
                            id : 'incomingCallResponse',
                            from : message.from._id,
                            to : UserDetails.user._id,
                            callResponse : 'reject',
                            message : 'user declined'
                        };
            $scope.sendMessage(response);
        }  
    })
    Socket.on('callendbycaller',function(callerid){
        //console.log("callendbycaller",callerid);
        if (sessionStorage['videoInProgress'] == 'false') { 
            $scope.ringTone.pause();
            $mdDialog.cancel();
        }  
    })
    $scope.sendMessage  = function(message) {
        var jsonMessage = JSON.stringify(message);
        Socket.emit('TestCallMessage',jsonMessage);
    }
    Socket.on('conferenceInvite',function(message) {
        sessionStorage['circle'] = message.circleID;
        if (sessionStorage['conferenceInProgress'] == 'false' &&  sessionStorage['videoInProgress'] == 'false') {
           $scope.ringTone.play();
          var confirm = $mdDialog.confirm()
              .title('Confrence Invite?')
              .content('User ' + message.name + ' is invtiting you to a video conference. Do you accept?')
              .ok('Yes!')
              .cancel('No!')
          $mdDialog.show(confirm).then(function() {
             $scope.ringTone.pause();
             Socket.emit("conferenceInviteAcepted",message);
               $location.path('/conferenceRooms/'+message.circleID);
                var promise = $interval(function(){
                    if($location.path() == '/conferenceRooms/'+message.circleID){
                        $rootScope.$emit('invitetoCall',message);
                        $interval.cancel(promise);
                    }
                },2000);
          }, function() {
             Socket.emit("conferenceInviteAcepted",message);
              $scope.ringTone.pause();
              var usrDec = {
                  id: UserDetails.user._id,
                  name:UserDetails.user.fullName,
                  to: message.id
              }
              Socket.emit('userDeclined', usrDec);
              $mdDialog.hide();
          });
        }else{
            var inCall = {
                  id: UserDetails.user._id,
                  name: UserDetails.user.fullName,
                  to: message.id
                }
            Socket.emit('inCall', inCall);
        }
    });
    Socket.on('endsingleAddedcall',function() {

    })
    Socket.on('endsingleAddedcall',function() {
        if(sessionStorage['conferenceInProgress'] == 'false') {
            $scope.ringTone.pause();
            $mdDialog.cancel();
        }
    })
    Socket.once('scheduleconferenceInvite',function(data){
        if (sessionStorage['conferenceInProgress'] == 'false') {
          var confirm = $mdDialog.confirm()
              .title('Confrence Invite?')
              .content('You Initiate a Confrence  ' + data.subject +' . Do you want to start this conference ?')
              .ok('Yes!')
              .cancel('No!')
            $mdDialog.show(confirm).then(function() {
               $location.path('/conferenceRooms/'+data.circleid);
                var promise = $interval(function(){
                    if($location.path() == '/conferenceRooms/'+data.circleid){
                        $rootScope.$emit('invitetoConf',data);
                        $interval.cancel(promise);
                    }
                },2000);
                var schacc ={id:data._id,status:"Confrence Finished"}
                Socket.emit('scheduleConfReject',schacc);
            }, function() {
                var schrej ={id:data._id,status:"Admin Rejected"}
                Socket.emit('scheduleConfReject',schrej);  
                $mdDialog.hide();
            }); 
        }else{
            var inCall = {
                  id: UserDetails.user._id,
                  name: UserDetails.user.fullName,
                  to: message.id
                }
            Socket.emit('inCall', inCall);
        }
    })
    //show alerts using custom templates 
    $scope.showAlert = function(alert) {
        $mdDialog.show({
            locals: {alert:alert},
            controller: DialogController,
            templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true
        })
    };
    // dialog controller
    function DialogController($scope, $mdDialog, alert) {
        $scope.alertData = alert
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    /*--------------------- Different Message Handling in  side bar ---------------------------------*/
    
    Socket.on('addedToRoom',function (alert) {
        var content = ""+(alert.creator.fullName)+" added you to the conference room "+(alert.circleName)+"";
        var alert = {
            title : "New Conference Room",
            content : content
        }
        $scope.$apply(function(){
            $scope.showAlert(alert);
            $timeout(function(){Socket.emit('get_circles_sidebar',UserDetails.user._id);})
        })
    })
    Socket.once('addedToCircleConfRoom',function (alert) {
        var content = ""+(alert.creator.fullName)+" added you to the conference room "+(alert.circleName)+"";
        var alert = {
            title : "New Conference Room",
            content : content
        }
        //$scope.showAlert(alert);
        Socket.emit('get_circles_sidebar',UserDetails.user._id);
    })
    Socket.on('accepted',function(data){
        var content = "Your request was accepted by "+data.user.fullName+"";
        var alert = {
            title : "New Contact",
            content : content
        }
        $scope.showAlert(alert);
        Socket.emit('get_friends_sidebar',UserDetails.user._id);
        // $scope.$apply(function(){
        //     $timeout(function(){})
        // })
    })
    Socket.on('rejectedFriendRequest',function(data){
        var content = "Your request was rejected by "+data.fullName;
        var alert = {
            title : "Reject Friends Request",
            content : content
        }
        $scope.showAlert(alert);
    })
    Socket.on('removedFromRoom',function(data){
        var content = "You were removed from conference room "+data.circleName+"";
        var alert = {
            title: "Conference Rooms",
            content : content
        }
        $scope.$apply(function(){
            $scope.showAlert(alert);
            if($location.path() == '/conferenceRooms/'+data._id+'')
                $location.path('/');
            Socket.emit('get_circles_sidebar',UserDetails.user._id);
        })
    })
    // delete room from the list
    Socket.on('roomDeleted',function(data){
        if(data.creator == UserDetails.user._id)
            var content = "Room "+data.circleName+" was succesfully removed";
        else
            var content = "Conference room "+data.circleName+" was deleted by its admin";
        var alert = {
            title : "Conference Rooms",
            content: content
        }
        $scope.$apply(function(){
            $scope.showAlert(alert);
            if($location.path() == '/conferenceRooms/'+data._id+'')
                $location.path('/');
            Socket.emit('get_circles_sidebar',UserDetails.user._id);
        })
    })
    // remove friend response
    Socket.on('removedFriend',function(data){
        Socket.emit('get_friends_sidebar',UserDetails.user._id);
    })
    
    // if socket connect generate an enty in connect table with socket id and user id
    Socket.on('connect',function(data){
        Socket.emit('setStatus',UserDetails.user._id);
    })
    Socket.on('statChange',function(id){
       // console.log("statChange in side bar",id);
        frndList.forEach(function(contact){
            if(contact._id == id){
                $timeout(function(){Socket.emit('get_friends_sidebar',UserDetails.user._id);},3000,false);
            }
        })
    })
    /*--------------------- Message Hanling  ends here ---------------------------------*/
    $scope.fn_GoChat = function(item){
        $scope.selection.selectedNode = item._id;
        sessionStorage['selected'] = $scope.selection.selectedNode;
        sessionStorage['chatId'] = item._id;
        var n = {
            id:sessionStorage['chatId'],
            uid:UserDetails.user._id
        }
        Socket.emit('allRead', n);
        for(var contactCount=0;contactCount<$scope.contacts.length;contactCount++){
            if($scope.contacts[contactCount]._id == item._id){
                $timeout(function(){
                    $scope.contacts[contactCount].unreadMsgCount = 0;
                    $scope.contacts[contactCount].new = false;
                });
                break;
            }
        }
        $location.path('/chat/'+item._id+'');
    }
    // redirect  to  grouop chat 
    $scope.fn_GrpChat = function(item){
        $scope.selection.selectedNode = item._id;
        sessionStorage['selected'] = $scope.selection.selectedNode;
        sessionStorage['circle'] = item._id;
        var n = {
            id:sessionStorage['circle'],
            uid:UserDetails.user._id
        }
        Socket.emit('iRead', n);
        for(var crCount=0;crCount<$scope.conferenceRooms.length;crCount++){
            if($scope.conferenceRooms[crCount]._id == item._id){
                $timeout(function(){
                    $scope.conferenceRooms[crCount].unreadMsgCount = 0;
                    $scope.conferenceRooms[crCount].new = false;
                });
                break;
            }
        }
        $location.path('/conferenceRooms/'+item._id+'');
    }
    function clr(){
        $scope.password = "";
        $scope.confirm = "";
    }
    // Change the Password After Login 
    $scope.changePassword = function(){
        $('#changepassword').modal('hide');
        if($scope.password != $scope.confirm){
            var alert = {
                title : "Mismatch",
                content: "Password Not Matching"
            }
            $scope.showAlert(alert);
            clr();
        }else if($scope.password.length < 6){
            var alert = {
                title : "Too Small",
                content: "Password should be Longer"
            }
            $scope.showAlert(alert);
            clr();
        }else{
            Socket.emit("changePassword",{usersid:UserDetails.user._id,password:$scope.password})
            Socket.on('returnChagePassword',function(data){
                        var alert = {
                                title : "Password Changed",
                                content: data
                            }
                        $scope.showAlert(alert);
                        clr();
            });
        }
    }
        //Color Picker
    $scope.clickColor = function(color){
        if(color == 1 ){
            colorCode = "greenstyle.css";
        }else if(color == 2){
            colorCode = "style.css";
        }else if(color == 3){
            colorCode = "greenstyle.css";
        }else{
            colorCode = "style.css";
        }
        var theme = {id:UserDetails.user._id,cCode:colorCode}
           Socket.emit('chageTheme',theme);
           Socket.once('updatedTheme',function(data){
             alert(data);
                location.reload(); 
           });
    }
    
}])
