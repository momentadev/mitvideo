angular.module('friends').controller('FriendsController', ['$scope','$rootScope','$mdDialog','$cookies','$location','Socket', 'UserDetails',
  	function($scope, $rootScope, $mdDialog, $cookies, $location, Socket, UserDetails) {
      $rootScope.$emit('noSel')
  		Socket.emit('getFriends',UserDetails.user._id)
  		Socket.on('friendsList',function(data){
        $scope.friends=[];
        if(data.length>=1){
          data.forEach(function(dat){
            $scope.friends.push(dat);
            $scope.$apply();
          })
        }
  		})
      // go to chat
      $scope.fn_GoChat = function(id){
            sessionStorage['chatId'] = id;
            $location.path('/chat/'+id+'')
      }
      // remove friend from the friend list
  		$scope.fn_RemoveFriend = function(ev,id){
            var confirm = $mdDialog.confirm()
                .title('Remove Friend?')
                .content('You are about to remove this user from your contact list. Sure you want to do it???')
                .ok('Yes!')
                .cancel('No!')
                .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                var x = {
                    u : UserDetails.user._id,
                    c : id
                }
                Socket.emit('removeFriend',x)
                Socket.on('removedFriend',function(data){
                    Socket.emit('getFriends',UserDetails.user._id)
                })
            }, function() {
                  $mdDialog.hide();
            });
  		}
      // view profile
  		$scope.fn_View = function(id){
  			sessionStorage['id'] = id;
  			$location.path('/viewProf');
  		}
  	}
])