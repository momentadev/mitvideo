angular.module('conference').controller('conferenceListController', ['$scope', '$compile', '$filter','$rootScope', '$mdDialog', '$timeout', 'Socket', 'UserDetails',
    function($scope, $compile, $filter,$rootScope, $mdDialog, $timeout, Socket, UserDetails) {
        $scope.userID = UserDetails.user._id;
        init();
        initExpiry();
        $scope.memb = [];
        //Load initial active confrence details 
        function init(){
            Socket.emit('getConfdetails',$scope.userID);
            Socket.on('receiveConfDetails',function(data){
                $scope.activeConf = data;
               $scope.$apply();
            }); 
        }
        // Load initial expired confrence details 
        function initExpiry(){
            Socket.emit('getexpConfdetails',$scope.userID);
            Socket.on('receiveexpConfDetails',function(data){
                $scope.expDetails = data;
                $scope.$apply();
            }); 
        }
        $scope.editConfrence = function(id){
            $scope.memb = [];
            $scope.datetime = '';
            Socket.emit("editConfrence",id);
            Socket.on('editConfrenceResult',function(data){
                data.forEach(function(dataItems){
                    $scope.confName = dataItems.confName;
                    $scope.dt = dataItems.date;
                   // $scope.datetime = $filter('date')(new Date(dataItems.date), "MM/dd/yyyy");
                    //console.log($filter('date')(new Date(dataItems.date), 'HH:mm'))
                    //console.log($filter('date')(new Date(dataItems.date), "MM/dd/yyyy"))
                   // console.log( $filter('date')($scope.dt, "MM/dd/yyyy"))
                   // $scope.datetime = $filter('date')(new Date($scope.dt), "MM/dd/yyyy")
                    $scope.tm = dataItems.time;
                    if ($scope.memb.indexOf(dataItems.name) == -1) {
                        $scope.memb.push(dataItems.name);
                    }
                })
                $('#edtCof').modal('show');
            })
        }
        $scope.updateConfrence = function(){
            //console.log("$scope.confName",$scope.confName);
            //console.log("$scope.datetime",$scope.datetime);
            if($scope.datetime == ''){
                alert("empty date");
            }
            $('#edtCof').modal('hide');
        }
        // remove a particular confrence
        $scope.removeConfrence = function(ev,id,confName){
            var confirm = $mdDialog.confirm()
            .title('Remove Confrence')
            .content('Sure Delete confrence'+confName+'?')
            .ok('Yes')
            .cancel('No')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function(){
                Socket.emit('removeConfrence',id);
                Socket.once('confrenceDeleteResponse',function(){
             var alert = {
                         title: "Confrence Deleted",
                         content: "Confrence "+confName+ " Deleted"
                       }
            $scope.showAlert(alert);
                $scope.expDetails = [];
                $scope.activeConf = [];
                init();
                initExpiry();
            });     
            }, function() {
                $mdDialog.hide();
            });
        }
        // show acustom alerts
        $scope.showAlert = function(alert) {
            $mdDialog.show({
                locals: {
                    alert: alert
                },
                controller: DialogController,
                templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false
            })
        }
        // custom dialog controller
        function DialogController($scope, $mdDialog, alert) {
            $scope.alertData = alert;
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        // view participants for every confrence 
        $scope.viewParticipants = function(id){
            Socket.emit("getParticipants",id);
            Socket.on('participants',function(data){
                $scope.participant = data;
                $scope.$apply();
                $('#participantList').modal('show');
            })
        }
    }
]);
