angular.module('conference').controller('conferenceController', ['$scope', '$compile', '$filter','$location','$rootScope', '$mdDialog', '$timeout', 'Socket', 'UserDetails',
    function($scope, $compile, $filter,$location,$rootScope, $mdDialog, $timeout, Socket, UserDetails) {
  //       $scope.commonSidebar = $("#commonSideBar")[0];
  //       $scope.commonSidebar.style.display = "block";   
  //       $scope.userID = UserDetails.user._id;
  //       $scope.results = [];
  //       $scope.selected = [];
  //       $scope.selectedList = [];
  //       $scope.partic = [];
  //       $scope.partic2 = [];  
  //       $scope.showResults = false;
  //       $scope.sessionId = UserDetails.user._id;
  //       $scope.maindiv  = $("#main_id")[0];
  //       $scope.ringTone = $("#myAudio")[0];
  //       sessionStorage['conferenceInProgress'] = 'false';
  //       $scope.newparticipants  = [];
  //       $scope.newparticipants2 = [];
  //       $scope.expand = false;
  //       $scope.expandStatus = false;

  //       // getting the friend details.
  //       Socket.emit('get_friends_sidebar',UserDetails.user._id);
  //       Socket.on('friends_sidebar', function(data) {
  //            $scope.contacts = [];
  //            if (data.length >= 1) {
  //                var i = 0;
  //                $scope.got_contacts = false;
  //                data.forEach(function(dat) {
  //                    i++;
  //                    if (dat.status == 'online' || dat.status == 'offline') {
  //                        var allContact = {
  //                            _id: dat._id,
  //                            username: dat.username,
  //                            fullName: dat.fullName,
  //                            unreadMsgCount: 0,
  //                            status:dat.status
  //                        }
  //                       $scope.got_contacts = true;
  //                       $scope.contacts.push(allContact);
  //                    }
  //                    if (i >= data.length) {
  //                       $scope.$apply();
  //                    }
  //                })
  //            } else {
  //                 $scope.$apply(function() {
  //                    $scope.got_contacts = false;
  //                    $scope.contacts = data;
  //                 });
  //            }
  //       })
  //     // add friend to selected list
  //     $scope.addtoSelected = function(id,name){
  //         if($scope.selected.indexOf(id) == -1){
  //             $scope.selected.push(id);
  //             var dt = {_id:id,name:name,id:id};
  //             $scope.selectedList.push(dt);
  //         }
  //     }
  //     // remove friends from selected list
  //     $scope.removefromSelected = function(id){
  //         for (var i = 0; i < $scope.selected.length; i++) {
  //             if ($scope.selected[i] === id) {
  //                 $scope.selected.splice(i, 1);
  //                 i--;
  //             }
  //         }
  //         for (var i = 0; i < $scope.selectedList.length; i++) {
  //             if ($scope.selectedList[i]._id === id) {
  //                 $scope.selectedList.splice(i, 1);
  //                 i--;
  //             }
  //         }
  //     }
  //     // rootScope  to receive confrence call details from side bar.Confrence details received in sidebar controller 
  //   //   var incomingListenerCleanUp = $rootScope.$on('invitetoCall',function(ev, data){
  //   //     //console.log("data on incomingListenerCleanUp",data)
  //   //       if (sessionStorage['conferenceInProgress'] == 'false') {
  //   //            $scope.ringTone.play();
  //   //           var confirm = $mdDialog.confirm()
  //   //               .title('Confrence Invite?')
  //   //               .content('User ' + data.name + ' is invtiting you to a video conference. Do you accept?')
  //   //               .ok('Yes!')
  //   //               .cancel('No!')
  //   //           $mdDialog.show(confirm).then(function() {
  //   //           	$scope.maindiv.style.display="none";
  //   //               sessionStorage['conferenceId'] = data.roomName;
  //   //               sessionStorage['conferenceInProgress'] = 'true';
  //   //               $scope.join(sessionStorage['conferenceId'])
  //   //               $scope.ringTone.pause();
  //   //           }, function() {
  //   //               $scope.ringTone.pause();
  //   //               var usrDec = {
  //   //                   id: UserDetails.user._id,name: UserDetails.user.firstName + ' ' + UserDetails.user.lastName,
  //   //                   to: data.id
  //   //               }
  //   //               Socket.emit('userDeclined', usrDec);
  //   //               $mdDialog.hide();
  //   //           });
  //   //       } else {
  //   //           var inCall = {
  //   //               id: UserDetails.user._id,
  //   //               name: UserDetails.user.firstName + ' ' + UserDetails.user.lastName,
  //   //               to: data.id
  //   //           }
  //   //           Socket.emit('inCall', inCall);
  //   //       }
  //   // })
  //   // inform the admin to start the schedule confrence
		// Socket.once('scheduleconferenceInvite',function(data){
		//  	if (sessionStorage['conferenceInProgress'] == 'false') {
  //               var confirm = $mdDialog.confirm()
  //                   .title('Schedule Confrence')
  //                   .content('You Initiate a Confrence  ' + data.subject +' . Do you want to start this conference ?')
  //                   .ok('Yes!')
  //                   .cancel('No!')
  //               $mdDialog.show(confirm).then(function() {
  //                  schdule(data)
  //                  $scope.maindiv.style.display="none";
  //                  var schacc ={id:data._id,status:"Confrence Finished"}
  //                   Socket.emit('scheduleConfReject',schacc);
  //               }, function() {
  //               	  var schrej ={id:data._id,status:"Admin Rejected"}
  //                 	Socket.emit('scheduleConfReject',schrej);
  //                   $mdDialog.hide();
  //               });
  //           } 
  //   })
  //   // start the schedule confrence
		// function schdule(data){
		//   	$scope.newarr=[];
		//   	$scope.roomName = data.creator + '' + new Date().getTime();
  //           sessionStorage['conferenceId'] = $scope.roomName;
  //           for (i = 0; i < data.members.length; i++) {
  //               $scope.newarr.push(data.members[i].id)
  //           }
  //           $scope.register($scope.newarr, $scope.roomName);
  //           sessionStorage['conferenceInProgress'] = 'true';
  //       }
  //       Socket.on('userDeclinedMsg', function(from) {
  //           sessionStorage['conferenceInProgress'] = 'false';
  //           var alert = {
  //               title: "Reject Call",
  //               content: from + ' reject your call'
  //           }
  //           $scope.showAlert(alert);
  //       });
  //       Socket.on('inCAllReject', function(from) {
  //           // if(count < 2){
  //           //    sessionStorage['conferenceInProgress'] = 'false';
  //           // }        
  //           var alert = {
  //               title: "Call Rejected",
  //               content: from + ' is on another call.'
  //           }
  //           $scope.showAlert(alert);
  //       });
  //       $scope.showAlert = function(alert) {
  //           $mdDialog.show({
  //               locals: {
  //                   alert: alert
  //               },
  //               controller: DialogController,
  //               templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
  //               parent: angular.element(document.body),
  //               clickOutsideToClose: true
  //           })
  //       }
  //       function DialogController($scope, $mdDialog, alert) {
  //           $scope.alertData = alert
  //           $scope.hide = function() {
  //               $mdDialog.hide();
  //           };
  //           $scope.cancel = function() {
  //               $mdDialog.cancel();
  //           };
  //           $scope.answer = function(answer) {
  //               $mdDialog.hide(answer);
  //           };
  //       }
        // $scope.findUsers = function(query) {
        //     if (query.length >= 1) {
        //     	$scope.showResults = true;
        //         var str = query.split(" ");
        //         if (str.length == 1)
        //             Socket.emit('txt', str[0]);
        //         else
        //             Socket.emit('srch', str);
        //     } else {
        //         $scope.gotResults = false;
        //         $scope.results = [];
        //         $scope.showResults = false;
        //     }
        // }
        // Socket.on('res', function(results) {
        //     if (results.length >= 1) {
        //         $scope.gotResults = true;
        //           results.forEach(function(res){
        //           	if(res._id != UserDetails.user._id){
        //           		  $scope.results.push(res)
        //           	}		  
        //         });
        //     } else {
        //         $scope.results = [];
        //     }
        // });
        // $scope.fn_toggle = function(item, sel, srch) {
        //     var idx = $scope.selected.indexOf(item);
        //     var idx1 = $scope.results.indexOf(item);
        //     if (idx > -1) {
        //         $timeout(function() {
        //             $scope.selected.splice(idx, 1);
        //             $scope.results.push(item);
        //             $('#addMemberText').focus();
        //         })
        //     } else {
        //         $timeout(function() {
        //             $scope.selected.push(item);
        //             $scope.results.splice(idx1, 1);
        //             $('#addConferenceMember').focus();
        //         })
        //     }
        // };
        // $scope.fn_closeSearch = function(){
        //     $timeout(function(){
        //         $('#addConferenceMember').val("");
        //         $scope.showResults = false;
        //         $('#addConferenceMember').focus();
        //     });
        // }

        // start live call
    //   $scope.liveCall = function(ev){
    //       if($scope.confrenceName==null){
    //           var alert = {
    //           title: "Confrance Name",
    //           content: 'Enter a name for your confrence'
    //           }
    //           $scope.showAlert(alert);
    //       }else if($scope.selected.length<=0){
    //           var alert = {
    //           title: "Add Participant",
    //           content: 'No Participant selected'
    //           }
    //           $scope.showAlert(alert);
    //       }else{
    //           $scope.mainVideo.style.display = "none";
    //           $scope.conferenceAddUserText = "";
    //           $scope.gotResults = false;
    //           $scope.results = [];
    //           $scope.roomName = $scope.userID + '' + new Date().getTime();
    //           sessionStorage['conferenceId'] = $scope.roomName;
    //           $scope.register($scope.selected, $scope.roomName);
    //           sessionStorage['conferenceInProgress'] = 'true';
    //           $scope.maindiv.style.display="none";
    //           $scope.selected = [];
    //           $scope.selectedList = [];
    //           $scope.confrenceName = "";
    //           $scope.confdate = "";
    //       }
    // 	}
    //   // schedule confrence.getting date,time,confrence name and participant details 
  		// $scope.scheduleCall = function(){
  		// 	if($scope.confrenceName==null){
    //        		var alert = {
  	 //                title: "Room Name",
  	 //                content: 'Enter a name for your room'
  	 //            }
    //          		$scope.showAlert(alert);
    //     }else if($scope.selected.length<=0){
    //         	var alert = {
	   //              title: "Add Participant",
	   //              content: 'No Participant selected'
    //        		}
    //         	$scope.showAlert(alert);
    //     }else if($scope.confdate==null){
    //         	var alert = {
	   //              title: "Confrence Date",
	   //              content: 'Please Select the confrence date and time'
	   //          }
    //        		$scope.showAlert(alert);
    //     }else{
    //         	$scope.datetime = $scope.confdate.split(' ');
    //         	var timeString =  $scope.datetime[1];
    //           var splittime = timeString.split(':')
    //         	var H = +timeString.substr(0, 2);
    //   				var h = (H % 12) || 12;
    //   				var ampm = H < 12 ? "AM" : "PM";
    //   				timeString = h + timeString.substr(2, 3) + ampm;
    //             var dateString =  $scope.datetime[0].split('/');
    //             // var newDate = dateString[1]+"/"+dateString[0]+"/"+dateString[2]
    //             var date = new Date(dateString[2],dateString[1]-1,dateString[0],splittime[0],splittime[1]);
    //             var current_date = new Date();
    //             if(date <= current_date) {
    //                    var alert = {
    //                             title: "Invalid Date",
    //                             content: 'Selected date is Invalid.Please Select a valid date'
    //                         }
    //                         $scope.showAlert(alert);
    //             }else{
    //                  $scope.confdetails = {
    //                                       name    :$scope.confrenceName,
    //                                       creator :UserDetails.user,
    //                                       members :$scope.selectedList,
    //                                       confdate:date,
    //                                       conftime:timeString
    //                                     }
                                        
    //                   Socket.emit("confrenceDetails",$scope.confdetails)    
    //                   Socket.once('schedulesaved', function (data){
    //                      	var alert = {
    //       		                title: "Schedule Confrence",
    //       		                content: 'Schedule Confrence  '+data.subject+' Saved'
    //     		              }
    // 		              $scope.showAlert(alert);
    //                   $scope.selected = [];
    //                   $scope.selectedList = [];
    //                   $scope.confrenceName = "";
    //                   $scope.confdate = "";
    //                   $scope.confdetails = "";
    //                   $scope.$apply();
    //                 })
    //             }        
	   //      }       
  		// } 
    //   // end call handle here
    // 	  $scope.disconnectCall = function() {
    //         Socket.emit("disconnectCall", $scope.userID);
    //         var participant = $scope.participants[$scope.userID];
    //         participant.dispose();
    //         delete $scope.participants[$scope.userID];
    //         $scope.mainVideo.pause();
    //         $scope.mainVideo.src = "";
    //      			$scope.mainVideo.load();
    //      			$scope.mainVideo.style.display = "none";
    //      			for (i=0;i<$scope.partic.length;i++) {
    //      				 $("#" + $scope.partic[i]).remove();
    //                      var tt = $scope.partic[i].split('-');
    //                      $("#div-"+ tt[1]).remove();
    //      			};
    //         sessionStorage['conferenceInProgress'] = 'false';
    //         sessionStorage['conferenceId'] = null;
    //         $scope.maindiv.style.display="block";
    //     }
    //     // add new participants to a live call.show the new participant modal
    //     $scope.addnewParticipant = function (){
    //         Socket.emit('get_friends_sidebar',UserDetails.user._id);
    //         Socket.on('friends_sidebar', function(data) {
    //              frndList = data;
    //              $scope.contactsOnline = [];
    //              if (data.length >= 1) {
    //                  var i = 0;
    //                  $scope.got_online_contact_onadd = false;
    //                  data.forEach(function(dat) {
    //                      i++;
    //                      if (dat.status == 'online') {
    //                          var onlineContact = {
    //                             _id: dat._id,
    //                             username: dat.username,
    //                             fullName: dat.fullName
    //                          }
    //                          $scope.got_online_contact_onadd = true;
    //                          $scope.contactsOnline.push(onlineContact);
    //                      }
    //                      if (i >= data.length) {
    //                         $scope.$apply();
    //                      }
    //                  })
    //              } else {
    //                  $scope.$apply(function() {
    //                      $scope.got_online_contact_onadd = false;
    //                      $scope.contactsOnline = data;
    //                  });
    //              }
    //         })
    //             $('#newparticipantList').modal('show');
    //     }
    //     // add to live call list
    //     $scope.addtoList = function(id,name){
    //         if($scope.newparticipants.indexOf(id) == -1){
    //             $scope.newparticipants.push(id);
    //             var dt = {id:id,name:name};
    //             $scope.newparticipants2.push(dt);
    //         }  
    //     }
    //     // remove from the live call list
    //     $scope.removefromList = function(id){
    //         for (var i = 0; i < $scope.newparticipants.length; i++) {
    //           if ($scope.newparticipants[i] === id) {
    //             $scope.newparticipants.splice(i, 1);
    //             i--;
    //           }
    //         }
    //         for (var i = 0; i < $scope.newparticipants2.length; i++) {
    //           if ($scope.newparticipants2[i].id === id) {
    //             $scope.newparticipants2.splice(i, 1);
    //             i--;
    //           }
    //         }
    //     }
    //     // add participant to the confrence 
    //     $scope.addtoConf = function(){
    //         var index;
    //         for (var i=0; i< $scope.partic2.length; i++) {
    //             index = $scope.newparticipants.indexOf($scope.partic2[i]);
    //             if (index > -1) {
    //                 $scope.newparticipants.splice(index, 1);
    //             }
    //         }
    //         $('#newparticipantList').modal('hide');
    //         var newUser={roomName:sessionStorage['conferenceId'],addedBy:UserDetails.user,participants: $scope.newparticipants};
    //         Socket.emit("newuserAdd",newUser);
    //         $scope.newparticipants = [];
    //         $scope.newparticipants2 = [];
    //     }
    //     /* ---------------------------------------------------- Audio/Video Controls for Main Video-------------------------*/
    //     $scope.muteCall = function() {
    //         $scope.mute = $("#mute_call")[0]
    //         $scope.mute.style.visibility = "hidden";
    //         $scope.unmute = $("#unmute_call")[0]
    //         $scope.unmute.style.visibility = "visible";
    //         $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = false;
    //     }
    //     $scope.unmuteCall = function() {
    //         $scope.unmute = $("#unmute_call")[0]
    //         $scope.unmute.style.visibility = "hidden";
    //         $scope.mute = $("#mute_call")[0]
    //         $scope.mute.style.visibility = "visible";
    //         $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = true;
    //     }
    //     $scope.hideVideo = function() {
    //         $scope.removeVideo = $("#remove_video")[0];
    //         $scope.removeVideo.style.visibility = "hidden";
    //         $scope.showVideos = $("#show_video")[0];
    //         $scope.showVideos.style.visibility = "visible";
    //         $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = false;
    //     }
    //     $scope.showVideo = function() {
    //         $scope.showVideos = $("#show_video")[0];
    //         $scope.showVideos.style.visibility = "hidden";
    //         $scope.removeVideo = $("#remove_video")[0];
    //         $scope.removeVideo.style.visibility = "visible";
    //         $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = true;
    //     }
    //     /*------------------------------------------------- Main Video Audio/Video Controls ends Here--------------------------*/
    //     /*------------------------------------------------------------ Kurento User Side Fuctions ----------------------------- */
    //     function Participant(id) {
    //         this.id = id;
    //         this.rtcPeer = null;
    //         this.iceCandidateQueue = [];
    //     }
    //     Participant.prototype.offerToReceiveVideo = function(error, offerSdp) {
    //         if (error) {
    //             return console.error("sdp offer error");
    //         }
    //         var msg = {
    //             id: "receiveVideoFrom",
    //             user: $scope.userID,
    //             sender: this.id,
    //             sdpOffer: offerSdp
    //         };
    //         $scope.sendMessage(msg);
    //     };
    //     Participant.prototype.onIceCandidate = function(candidate) {
    //         var message = {
    //             id: 'onIceCandidate',
    //             user: $scope.userID,
    //             candidate: candidate,
    //             sender: this.id
    //         };
    //         $scope.sendMessage(message);
    //     };
    //     Participant.prototype.dispose = function() {
    //         this.rtcPeer.dispose();
    //         this.rtcPeer = null;
    //     };
    //     $scope.mainVideoCurrentId = null;
    //     $scope.mainVideo = null;
    //     $scope.participants = {};
    //     $scope.mainVideo = $("#main_video")[0];

    //     window.onbeforeunload = function() {
    //         $scope.mainVideo.pause();
    //         $scope.mainVideo.src = "";
    //         $scope.mainVideo.style.display = "none";
    //         $scope.mainVideo.load();
    //         Socket.emit("disconnectCall", $scope.userID);
    //     };
    //     // message handler
    //     Socket.on("message", function(message) {
    //         switch (message.id) {
    //             case "existingParticipants":
    //                 // console.log("existingParticipans : " + message.data);
    //                 $scope.onExistingParticipants(message);
    //                 break;
    //             case "newParticipantArrived":
    //                 //console.log("newParticipantArrived : " + message.new_user_id);
    //                 $scope.onNewParticipant(message);
    //                 break;
    //             case "participantLeft":
    //                 //console.log("participantLeft : " + message.sessionId);
    //                 $scope.onParticipantLeft(message);
    //                 break;
    //             case "receiveVideoAnswer":
    //                 //console.log("receiveVideoAnswer from : " + message.sessionId);
    //                 $scope.onReceiveVideoAnswer(message);
    //                 break;
    //             case "iceCandidate":
    //                 //console.log("iceCandidate from : " + message.sessionId);
    //                 var participant = $scope.participants[message.sessionId];
    //                 if (participant != null) {
    //                     // console.log(message.candidate);
    //                     participant.rtcPeer.addIceCandidate(message.candidate, function(error) {
    //                         if (error) {
    //                             if (message.sessionId === $scope.sessionId) {
    //                                 console.error("Error adding candidate to self : " + error);
    //                             } else {
    //                                 console.error("Error adding candidate : " + error);
    //                             }
    //                         }
    //                     });
    //                 } else {
    //                     console.error('still does not establish rtc peer for : ' + message.sessionId);
    //                 }
    //                 break;
    //             default:
    //                 console.error("Unrecognized message: ", message);
    //         }
    //     });

    //     $scope.expandVideo = function(ids) {
    //         //console.log("other partic",$scope.partic);
    //         $scope.videoClicked = $("#video-"+ids)[0];
    //             // for (var i = 0; i < $scope.partic.length;i++) {
    //             //              $scope.otherVideos = document.getElementById($scope.partic[i]);
    //             //              var vv = $scope.partic[i].split('-');
    //             //              $scope.ot = $("#div-"+vv[1]).contents();
    //             //              $("#div-"+vv[1]).html("");
    //             //              console.log("div content",$scope.ot);
    //             // }
               
    //            if($scope.expand == false){
    //              $('#video-'+ids).animate({marginLeft: 250, marginTop: 150, height: 400, width: 400}, 'slow');
    //                   for (var i = 0; i < $scope.partic.length;i++) {
    //                     if($scope.partic [i]!= "video-"+ids){
    //                          $scope.otherVideos = document.getElementById($scope.partic[i]);
    //                          $scope.otherVideos.style.display = "none";
    //                          // var vv = $scope.partic[i].split('-');
    //                          // $scope.ot = document.getElementById("div-"+vv[1]);
    //                          //  console.log("Videos List",$("#"+$scope.partic[i]).contents());
    //                          //  $("#"+$scope.partic[i].contents()).html()
    //                         //var ref = $("#reference_pane").contents();
    //                        //console.log($('#'+$scope.partic[i]))
    //                        //$('#'+$scope.partic[i]).animate({marginLeft: 0, marginTop: 0, height: 150, width: 200}, 'fast');
    //                     }
    //                   }
    //               $scope.expand = true;
    //             }else{
    //               $('#video-'+ids).animate({marginLeft: 0, marginTop: 0, height: 150, width: 200}, 'slow');
    //                 for (var i = 0; i < $scope.partic.length;i++) {
    //                     if($scope.partic [i]!= "video-"+ids){
    //                          $scope.otherVideos = document.getElementById($scope.partic[i]);
    //                          $scope.otherVideos.style.display = "block";
    //                     }
    //                 }
    //                $scope.expand = false;
    //              // $scope.expandStatus =false;
    //             }
    //             // if($scope.videoClicked.style.height != '400px') {
    //             //     $scope.videoClicked.style.height = '400px';
    //             //     $scope.videoClicked.style.width  = '400px';
    //             //     $scope.videoClicked.style.marginLeft = '250px';
    //             //     $scope.videoClicked.style.marginTop  = '150px';
    //             //     for (var i = 0; i < $scope.partic.length;i++) {
    //             //         if($scope.partic [i]!= "video-"+ids){
    //             //             console.log("test in if expandVideo");
    //             //             $scope.otherVideos = document.getElementById($scope.partic[i]);
    //             //             if($scope.otherVideos.style.height != '100px'){
    //             //                 $scope.otherVideos.style.height = '100px';
    //             //                 $scope.otherVideos.style.width  = '100px';
    //             //             }
    //             //         }
    //             //     }    
    //             // }else{
    //             //     $scope.videoClicked.style.height =  '200px';
    //             //     $scope.videoClicked.style.width  =  '200px';
    //             //     $scope.videoClicked.style.marginLeft = '0px';
    //             //     $scope.videoClicked.style.marginTop  = '0px'
    //             //     for(var i=0;i< $scope.partic.length;i++){
    //             //         var othervideos = document.getElementById(partic[i]);
    //             //         if($scope.partic[i] != "video-"+ids){
    //             //             othervideos.style.height = '200px';
    //             //             othervideos.style.width  = '200px';
    //             //         } 
    //             //     }
    //             // }      
    //     }
    //     $scope.sendMessage = function(data) {
    //         Socket.emit("message", data);
    //     }
    //     $scope.register = function(members, roomName) {
    //         var data = {
    //             id: "createRoom",
    //             admin: UserDetails.user,
    //             members: members,
    //             roomName: roomName
    //         };
    //         $scope.sendMessage(data);
    //     }
    //     $scope.join = function(room) {
    //         var data = {
    //             id: "joinRoom",
    //             sessionId: UserDetails.user._id,
    //             roomName: room
    //         };
    //         $scope.sendMessage(data);
    //     }
    //     $scope.onExistingParticipants = function(message) {
    //     	var audioConstraints = {
    //             mandatory : {
    //                 googEchoCancellation: true,
    //                 googNoiseSuppression: true
    //             }
    //         };
    //      //    var constraints = {
    //      //        audio: audioConstraints,
    //      //        video: {
    //      //            mandatory: {
    //      //                minWidth: 320,
    //      //                maxWidth: 320,
    //      //                maxFrameRate: 50,
    //      //                minFrameRate: 30
    //      //            }
    //      //        }
    //      //    };
    //         var constraints = {
    //             audio: audioConstraints,
    //             video: {
    //                 width:  { min: 320 },
    //                 height: { min: 320 },
    //             }
    //         };
         
    //         // create video for current user to send to server
    //         var localParticipant = new Participant($scope.sessionId);
    //         $scope.participants[$scope.sessionId] = localParticipant;
    //         var video = $scope.createVideoForParticipant(localParticipant);
    //         // bind function so that calling 'this' in that function will receive the current instance
    //         var options = {
    //             localVideo: video,
    //             mediaConstraints: constraints,
    //             onicecandidate: localParticipant.onIceCandidate.bind(localParticipant)
    //         };
    //         localParticipant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
    //             if (error) {
    //                 return console.error(error);
    //             }
    //             // initial main video to local first
    //             $scope.mainVideoCurrentId = $scope.sessionId;
    //             $scope.mainVideo.src = localParticipant.rtcPeer.localVideo.src;
    //             $scope.mainVideo.muted = true;
    //             $scope.mainVideo.style.display = "block";
    //             this.generateOffer(localParticipant.offerToReceiveVideo.bind(localParticipant));
    //         });
    //         for (var i in message.data) {
    //             $scope.receiveVideoFrom(message.data[i]);
    //         }
    //     }
    //     $scope.receiveVideoFrom = function(sender) {
    //         var participant = new Participant(sender);
    //         $scope.participants[sender] = participant;
    //         var video = $scope.createVideoForParticipant(participant);
    //         // bind function so that calling 'this' in that function will receive the current instance
    //         var options = {
    //             remoteVideo: video,
    //             onicecandidate: participant.onIceCandidate.bind(participant)
    //         };
    //         participant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
    //             if (error) {
    //                 return console.error(error);
    //             }
    //             this.generateOffer(participant.offerToReceiveVideo.bind(participant));
    //         });
    //     }
    //     $scope.onNewParticipant = function(message) {
    //         $scope.receiveVideoFrom(message.new_user_id)
    //     }
    //     $scope.onParticipantLeft = function(message) {
    //         var participant = $scope.participants[message.sessionId];
    //         participant.dispose();
    //         delete $scope.participants[message.sessionId];
    //         $("#video-" + participant.id).remove();
    //         $("#div-"+ participant.id).remove();
    //         for (var i = $scope.partic.length - 1; i >= 0; i--) {
    //             if ($scope.partic[i] === "video-" + participant.id) {
    //                 $scope.partic.splice(i, 1);
    //             }
    //         }
    //         if($scope.partic.length == 0){
    //             sessionStorage['conferenceInProgress'] = 'false';
    //         }
    //         for (var i = $scope.partic2.length - 1; i >= 0; i--) {
    //             if ($scope.partic2[i] ===  participant.id) {
    //                 $scope.partic2.splice(i, 1);
    //             }
    //         }
    //     }
    //     $scope.onReceiveVideoAnswer = function(message) {
    //         var participant = $scope.participants[message.sessionId];
    //         participant.rtcPeer.processAnswer(message.sdpAnswer, function(error) {
    //             if (error) {
    //                 console.error(error);
    //             } else {
    //                 participant.isAnswer = true;
    //                 while (participant.iceCandidateQueue.length) {
    //                     var candidate = participant.iceCandidateQueue.shift();
    //                     participant.rtcPeer.addIceCandidate(candidate);
    //                 }
    //             }
    //         });
    //     }
    //     // Create video for every participants
    //     $scope.createVideoForParticipant = function(participant) {
    //         $scope.videoId = "video-" + participant.id;
    //         $scope.divId= "div-"+ participant.id;
    //         $scope.btnlist = $("#button_list")[0];
    //         $scope.btnlist.style.display = "block";
    //         if ($scope.userID != participant.id) {
    //             $scope.videoHtml = '<div id="' + $scope.divId + '" style="display:inline-block;cursor:pointer;position:relative" class ="box"><video id="' + $scope.videoId + '"  autoplay ng-click=expandVideo("' + participant.id + '") width="200px"  height="150px" style="cursor:pointer;border:3px solid #357EBD;border-radius:10px;" poster="assets/imgs/novideo.png"></video></div>   &nbsp;&nbsp;';
    //             $scope.partic.push($scope.videoId);
    //             $scope.partic2.push(participant.id);
    //         } else {
    //             $scope.videoHtml = '<video id="' + $scope.videoId + '" autoplay ng-click=expandVideo("' + participant.id + '") width="200px"  height="150px"   style="cursor:pointer;border:3px solid #357ebd;border-radius:10px;display:none"></video>';
    //         }
    //         $scope.$apply();
    //         angular.element($('#video_list')[0]).append($compile($scope.videoHtml)($scope));
    //         return $("#" + $scope.videoId)[0];
    //     }
    //     /*------------------------------------------------------------ Kurento User Side Fuctions  End Here ----------------------------- */
    //     $scope.fullScreen =function(id){
    //         var element = $("#video-"+id)[0]
    //         if (element.requestFullscreen) {
    //             element.requestFullscreen();
    //         } else if (element.mozRequestFullScreen) {
    //             element.mozRequestFullScreen();
    //         } else if (element.webkitRequestFullscreen) {
    //             element.webkitRequestFullscreen();
    //         } else if (element.msRequestFullscreen) {
    //             element.msRequestFullscreen();
    //         }
    //     }
        // $scope.$on('$destroy', function() {
        //     sessionStorage['conferenceInProgress'] = 'false';
        //     sessionStorage['conferenceId'] = null;
        //     Socket.getSocket().removeAllListeners('message');
        //     Socket.getSocket().removeAllListeners('scheduleconferenceInvite');
        //     Socket.getSocket().removeAllListeners('userDetailsResponse');
        //     incomingListenerCleanUp();
        // })
    
    }
]);
