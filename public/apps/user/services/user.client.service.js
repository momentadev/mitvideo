angular.module('user').factory('UserDetails', [
  function() {
    this.user = window.user;

    return {
      user: this.user
    };
  }
]);