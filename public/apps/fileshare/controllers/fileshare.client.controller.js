angular.module('fileshare').controller('fileshareController', ['$scope','$mdDialog','PDFViewerService','Socket', 'UserDetails',
  	function($scope,$mdDialog,pdf,Socket, UserDetails) {
		
  		/*------------------- Initializing the uploaded file,shared file and video meassages ---------------------*/
		init();	
		initShared();
		initVdoMsg();
		/*--------------  PDF viewer Operations -----------------------  */
		$scope.viewer = pdf.Instance("viewer");
	    $scope.nextPage = function(){
	        $scope.viewer.nextPage();
	    };
	    $scope.prevPage = function(){
	        $scope.viewer.prevPage();
	    };
	    $scope.pageLoaded = function(curPage, totalPages){
	        $scope.currentPage = curPage;
	        $scope.totalPages = totalPages;
	    };
	    $scope.gotoPage = function(page){
			$scope.viewer.gotoPage(page);
		};
		$scope.gotoSpecificPage = function(){
		    $scope.viewer.gotoPage(parseInt($scope.pageno));
		}
		$scope.changeZoom = function(zoomLevel){
       	    $scope.viewer.changeZoom(zoomLevel);
    	};
		/*----------Initialize Uploaded File Details------------------*/
	  	function init(){
		    Socket.emit('getFiles',UserDetails.user._id);
		    Socket.on('uploadedFile',function(data){
		       	$scope.files=data;
	  			$scope.$apply();
		    });	
		}
		/*----------Initialize Shared File Details------------------*/
		function initShared(){
		    Socket.emit('getSharedFiles',UserDetails.user._id);
		    Socket.on('sharedFiles',function(data){
		    	$scope.sharedFiles=data;
	  			$scope.$apply();
		    });	
		}
		/*------------ Initialize Recoreded  video meassages ------------------*/
		function initVdoMsg(){
		    Socket.emit('getVdoMsg',UserDetails.user._id);
		    Socket.on('videoMsgDetails',function(data){
		       	$scope.videoMsg = data;
	  			$scope.$apply();
		    });	
		}
		/*----------Fetching the details of a specific file details ------------------*/
		$scope.viewFile = function(id){
			Socket.emit('viewFiles',id);
			Socket.once('fileDetails',function(data){
				$scope.ext =  data.fileName.split('.').pop();
				$scope.filname = "/uploadedfiles/"+data.fileName;
				$scope.$apply();
				if($scope.ext=="mp4" || $scope.ext=="mp3"  || $scope.ext=="webm" || $scope.ext=="wav" || $scope.ext=="mkv" ){
				    $('#showVideo').modal('show');
				}else if($scope.ext=="jpg" || $scope.ext=="png" || $scope.ext=="gif"){
				    $('#showImages').modal('show');
				}else if($scope.ext=="pdf"){
					$('#showfile').modal('show');
				}
		    });	
		}
		$scope.fn_closeVideo =function(){
			$scope.filname = "";
		}	
		/*----------Remove a file ------------------*/
		$scope.removeFile = function(ev,id,fileName){
			var confirm = $mdDialog.confirm()
			.title('Remove File')
			.content('Sure Delete  '+fileName+'?')
			.ok('Yes')
			.cancel('No')
			.targetEvent(ev);
			$mdDialog.show(confirm).then(function(){
				Socket.emit('removeFiles',id);
				Socket.once('deleteResponse',function(data){
				$mdDialog.show(
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('File Delete')
                        .content(data)
                        .ariaLabel('Alert')
                        .ok('Got it!')
                    );
				init()
			}); 	
			}, function() {
				$mdDialog.hide();
			});
		}
		/*----------Revoke the acess of shared file ------------------*/
		$scope.removeSharedFile = function(ev,id,fileName){
			 var confirm = $mdDialog.confirm()
			.title('Remove File')
			.content('Sure Delete Shared File  '+fileName+'?')
			.ok('Yes')
			.cancel('No')
			.targetEvent(ev);
			$mdDialog.show(confirm).then(function(){
				Socket.emit('removeShareFiles',id);
				Socket.once('shareRemove',function(data){
					$mdDialog.show(
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Share File Delete')
                        .content(data)
                        .ariaLabel('Alert')
                        .ok('Got it!')
                    );
				initShared()
			}); 	
			}, function() {
				$mdDialog.hide();
			});
		}  	
	}
])