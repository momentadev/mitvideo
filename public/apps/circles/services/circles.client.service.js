
// angular.module('circles').service('videoCall', ['UserDetails','Socket','$compile',
//   function(UserDetails,Socket,$compile) {
//     	var vdo = this;
//         vdo.callingDiv= $("#calling")[0];
//         vdo.sessionId = UserDetails.user._id;
//         vdo.partic = [];
//         vdo.partic2 = [];
//         vdo.mainVideoCurrentId = null;
//         vdo.mainVideo = null;
//         vdo.participants = {};
//         vdo.mainVideo = $("#main_video")[0];
//         vdo.messageListStyle = {
//             'max-height':'100%'
//         }
//    		vdo.muteCall = function() {
//             vdo.mute = $("#mute_call")[0]
//             vdo.mute.style.visibility = "hidden";
//             vdo.unmute = $("#unmute_call")[0]
//             vdo.unmute.style.visibility = "visible";
//             vdo.participants[UserDetails.user._id].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = false;
//         }
//         vdo.unmuteCall = function() {
//             vdo.unmute = $("#unmute_call")[0]
//             vdo.unmute.style.visibility = "hidden";
//             vdo.mute = $("#mute_call")[0]
//             vdo.mute.style.visibility = "visible";
//             vdo.participants[UserDetails.user._id].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = true;
//         }
//         vdo.hideVideo = function() {
//             vdo.removeVideo = $("#remove_video")[0];
//             vdo.removeVideo.style.visibility = "hidden";
//             vdo.showVideos = $("#show_video")[0];
//             vdo.showVideos.style.visibility = "visible";
//             vdo.participants[UserDetails.user._id].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = false;
//         }
//         vdo.showVideo = function() {
//             vdo.showVideos = $("#show_video")[0];
//             vdo.showVideos.style.visibility = "hidden";
//             vdo.removeVideo = $("#remove_video")[0];
//             vdo.removeVideo.style.visibility = "visible";
//             vdo.participants[UserDetails.user._id].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = true;
//         }
//         function Participant(id) {
//             this.id = id;
//             this.rtcPeer = null;
//             this.iceCandidateQueue = [];
//         }
//         Participant.prototype.offerToReceiveVideo = function(error, offerSdp) {
//             if (error) {
//                 return console.error("sdp offer error");
//             }
//             var msg = {
//                 id: "receiveVideoFrom",
//                 user: UserDetails.user._id,
//                 sender: this.id,
//                 sdpOffer: offerSdp
//             };
//             vdo.sendMessage(msg);
//         };
//         Participant.prototype.onIceCandidate = function(candidate) {
//             var message = {
//                 id: 'onIceCandidate',
//                 user: UserDetails.user._id,
//                 candidate: candidate,
//                 sender: this.id
//             };
//             vdo.sendMessage(message);
//         };
//         Participant.prototype.dispose = function() {
//             this.rtcPeer.dispose();
//             this.rtcPeer = null;
//         };
       

//         window.onbeforeunload = function() {
//             vdo.mainVideo.pause();
//             vdo.mainVideo.src = "";
//             vdo.mainVideo.style.display = "none";
//             vdo.mainVideo.load();
//             Socket.emit("disconnectCall", UserDetails.user._id);
//         };
//         // message handler
//         Socket.on("message", function(message) {
//             switch (message.id) {
//                 case "existingParticipants":
//                     vdo.onExistingParticipants(message);
//                     break;
//                 case "newParticipantArrived":
//                     vdo.onNewParticipant(message);
//                     break;
//                 case "participantLeft":
//                     vdo.onParticipantLeft(message);
//                     break;
//                 case "receiveVideoAnswer":
//                     vdo.onReceiveVideoAnswer(message);
//                     break;
//                 case "iceCandidate":
//                     var participant = vdo.participants[message.sessionId];
//                     if (participant != null) {
//                         participant.rtcPeer.addIceCandidate(message.candidate, function(error) {
//                             if (error) {
//                                 if (message.sessionId === vdo.sessionId) {
//                                     console.error("Error adding candidate to self : " + error);
//                                 } else {
//                                     console.error("Error adding candidate : " + error);
//                                 }
//                             }
//                         });
//                     } else {
//                         console.error('still does not establish rtc peer for : ' + message.sessionId);
//                     }
//                     break;
//                 default:
//                     console.error("Unrecognized message: ", message);
//             }
//         });

//         vdo.disconnectCall = function() {
//             console.log("vdo.disconnectCall");
//             vdo.callingDiv.style.display ="none";
//             Socket.emit("disconnectCall",UserDetails.user._id);
//             var participant = vdo.participants[UserDetails.user._id];
//             participant.dispose();
//             delete vdo.participants[UserDetails.user._id];
//             vdo.mainVideo.pause();
//             vdo.mainVideo.src = "";
//                    vdo.mainVideo.load();
//                    vdo.mainVideo.style.display = "none";
//                     for (i=0;i<vdo.partic.length;i++) {
//                          $("#" +vdo.partic[i]).remove();
//                          var tt =vdo.partic[i].split('-');
//                          $("#div-"+ tt[1]).remove();
//                     };
//             sessionStorage['conferenceInProgress'] = 'false';
//             sessionStorage['conferenceId'] = null;
//                 vdo.messageListStyle = {
//                     'max-height':'100%'
//                 }
//         }
//         vdo.sendMessage = function(data) {
//             Socket.emit("message", data);
//         }
//         vdo.register = function(members, roomName) {
//             var data = {
//                 id: "createRoom",
//                 admin: UserDetails.user,
//                 members: members,
//                 roomName: roomName,
//                 circleID:sessionStorage['circle']  
//             };
//             vdo.sendMessage(data);
//         }
//         vdo.join = function(room) {
//             var data = {
//                 id: "joinRoom",
//                 sessionId: UserDetails.user._id,
//                 roomName: room
//             };
//             vdo.sendMessage(data);
//         }
//         vdo.onExistingParticipants = function(message) {
//             var audioConstraints = {
//                 mandatory : {
//                     googEchoCancellation: true,
//                     googNoiseSuppression: true
//                 }
//             };
//             var constraints = {
//                 audio: audioConstraints,
//                 video: {
//                     width:  { min: 320 },
//                     height: { min: 320 },
//                 }
//             };
//             // create video for current user to send to server
//             var localParticipant = new Participant(vdo.sessionId);
//             vdo.participants[vdo.sessionId] = localParticipant;
//             var video = vdo.createVideoForParticipant(localParticipant);
//             // bind function so that calling 'this' in that function will receive the current instance
//             var options = {
//                 localVideo: video,
//                 mediaConstraints: constraints,
//                 onicecandidate: localParticipant.onIceCandidate.bind(localParticipant)
//             };
//             localParticipant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
//                 if (error) {
//                     return console.error(error);
//                 }
//                 // initial main video to local first
//                 vdo.mainVideoCurrentId = vdo.sessionId;
//                 vdo.mainVideo.src = localParticipant.rtcPeer.localVideo.src;
//                 vdo.mainVideo.muted = true;
//                 vdo.mainVideo.style.display = "block";
//                 this.generateOffer(localParticipant.offerToReceiveVideo.bind(localParticipant));
//             });
//             for (var i in message.data) {
//                 vdo.receiveVideoFrom(message.data[i]);
//             }
//         }
//         vdo.receiveVideoFrom = function(sender) {
//             var participant = new Participant(sender);
//             vdo.participants[sender] = participant;
//             var video = vdo.createVideoForParticipant(participant);
//             // bind function so that calling 'this' in that function will receive the current instance
//             var options = {
//                 remoteVideo: video,
//                 onicecandidate: participant.onIceCandidate.bind(participant)
//             };
//             participant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
//                 if (error) {
//                     return console.error(error);
//                 }
//                 this.generateOffer(participant.offerToReceiveVideo.bind(participant));
//             });
//         }
//         vdo.onNewParticipant = function(message) {
//             vdo.receiveVideoFrom(message.new_user_id)
//         }
//         vdo.onParticipantLeft = function(message) {
//             var participant = vdo.participants[message.sessionId];
//             console.log("participant",participant.id)
//             participant.dispose();
//             delete vdo.participants[message.sessionId];
//             $("#video-" + participant.id).remove();
//             $("#div-"+ participant.id).remove();
//             $("#span-"+participant.id).remove();
//             for (var i = vdo.partic.length - 1; i >= 0; i--) {
//                 console.log("participant list id ",vdo.partic[i]);
//                 if (vdo.partic[i] === "video-" + participant.id) {
//                     vdo.partic.splice(i, 1);
//                 }
//             }
//             if(vdo.partic.length == 0){
//                 sessionStorage['conferenceInProgress'] = 'false';
//                 //$scope.disconnectCall();
//             }
//             for (var i = vdo.partic2.length - 1; i >= 0; i--) {
//                 if (vdo.partic2[i] ===  participant.id) {
//                     vdo.partic2.splice(i, 1);
//                 }
//             }
//         }
//         vdo.onReceiveVideoAnswer = function(message) {
//             var participant = vdo.participants[message.sessionId];
//             participant.rtcPeer.processAnswer(message.sdpAnswer, function(error) {
//                 if (error) {
//                     console.error(error);
//                 } else {
//                     participant.isAnswer = true;
//                     while (participant.iceCandidateQueue.length) {
//                         var candidate = participant.iceCandidateQueue.shift();
//                         participant.rtcPeer.addIceCandidate(candidate);
//                     }
//                 }
//             });
//         }
//         // Create video for every participants
//         vdo.createVideoForParticipant = function(participant) {
//             console.log("createVideoForParticipant",participant.id);
//             vdo.usersName = "";
//             vdo.videoId = "video-" + participant.id;
//             vdo.divId= "div-"+ participant.id;
//             vdo.spanId="span-"+  participant.id;
//             vdo.btnlist = $("#button_list")[0];
//             vdo.btnlist.style.display = "block";
//             if (UserDetails.user._id != participant.id) {
//                 vdo.videoHtml = '<div id="' + vdo.divId + '" style="display:inline-block;cursor:pointer;position:relative"   class ="box"><video id="' + vdo.videoId + '"  autoplay ng-click=videoClick("' + participant.id + '") width="200px"  height="150px" style="cursor:pointer;border:3px solid #357EBD;border-radius:10px;text-transform: capitalize;font-weight: bold;font-size: 15px;" poster="assets/imgs/novideo.png"></video><span id="' + vdo.spanId + '" style="position:relative;top:-129px;left:-199px;text-transform: capitalize;font-weight: bold;font-size: 15px;"></span></div>';
//                 vdo.partic.push(vdo.videoId);
//                 vdo.partic2.push(participant.id);
//             } else {
//                 vdo.videoHtml = '<video id="' + vdo.videoId + '" autoplay ng-click=expandVideo("' + participant.id + '") width="200px"  height="150px"   style="cursor:pointer;border:3px solid #357ebd;border-radius:10px;display:none"></video>';
//             }
//             angular.element($('#video_list')[0]).append($compile(vdo.videoHtml)(vdo));
//             return $("#" + vdo.videoId)[0];
//         }
// }]);