angular.module('circles').controller('CreateCircleController', ['$scope','$rootScope','$mdDialog','$timeout','$location','Socket', 'UserDetails',
	function($scope,$rootScope,$mdDialog,$timeout,$location,Socket, UserDetails) {
    	$scope.selected = [];
        $scope.searchResult = [];
        $scope.results = [];
        $scope.selectedList = [];
        $scope.newparticipants = [];
        $scope.newparticipants2 = [];
        sessionStorage['selected'] = null;
        $rootScope.$emit('noSel')
        $scope.fn_save=function(ev){
            if($scope.confrenceName==null){
                var alert = {
                    title: "Confrence Name",
                    content: 'Enter a name for your Confrence'
                }
                $scope.showAlert(alert);
                 var element = $("#confname")[0];
                  element.focus();
            }else if($scope.selected.length<=0){
                var alert = {
                    title: "Add Participant",
                    content: 'No Participant selected'
                }
                $scope.showAlert(alert);
            }else{
      			var roomDeatils={
                  name: $scope.confrenceName,
                  cr: UserDetails.user,
                  mem: $scope.selectedList
                }
     			Socket.emit('add_circle',roomDeatils)
                $location.path('/circles')
                Socket.once('added_circle',function(){
                    var alert = {
                                title: "Created",
                                content: 'New Room '+ $scope.confrenceName+' Created'
                            }
                    $scope.showAlert(alert);
                    Socket.emit('get_circles_sidebar',UserDetails.user._id);
                })
        	}
      	}
        //content: 'New Room'+ $scope.confrenceName+' Created'
        Socket.emit('get_friends_sidebar',UserDetails.user._id);
        Socket.on('friends_sidebar', function(data) {
             $scope.contacts = [];
             if (data.length >= 1) {
                 var i = 0;
                 $scope.got_contacts = false;
                 data.forEach(function(dat) {
                     i++;
                     if (dat.status == 'online' || dat.status == 'offline') {
                         var allContact = {
                             _id: dat._id,
                             username: dat.username,
                             fullName: dat.fullName,
                             unreadMsgCount: 0,
                             status:dat.status
                         }
                        $scope.got_contacts = true;
                        $scope.contacts.push(allContact);
                     }
                     if (i >= data.length) {
                        $scope.$apply();
                     }
                 })
             } else {
                  $scope.$apply(function() {
                     $scope.got_contacts = false;
                     $scope.contacts = data;
                  });
             }
        })
      // add friend to selected list
      $scope.addtoSelected = function(id,name){
          if($scope.selected.indexOf(id) == -1){
              $scope.selected.push(id);
              var dt = {_id:id,name:name,id:id};
              $scope.selectedList.push(dt);
          }
      }
      // remove friends from selected list
      $scope.removefromSelected = function(id){
          for (var i = 0; i < $scope.selected.length; i++) {
              if ($scope.selected[i] === id) {
                  $scope.selected.splice(i, 1);
                  i--;
              }
          }
          for (var i = 0; i < $scope.selectedList.length; i++) {
              if ($scope.selectedList[i]._id === id) {
                  $scope.selectedList.splice(i, 1);
                  i--;
              }
          }
      }
     
        $scope.showAlert = function(alert) {
            $mdDialog.show({
                locals: {  alert: alert},
                controller: DialogController,
                templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
        }
        function DialogController($scope, $mdDialog, alert) {
            $scope.alertData = alert;
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        // $scope.$on('$destroy', function() {
        //     Socket.getSocket().removeAllListeners('added_circle');
        // })
     

       
	}
]);