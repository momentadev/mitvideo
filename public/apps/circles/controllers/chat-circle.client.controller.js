angular.module('circles').controller('grpChatController', ['$scope','$compile','$mdDialog','$rootScope','$q','$cookies','$timeout','$location','Socket', 'UserDetails',
    function($scope,$compile,$mdDialog,$rootScope,$q,$cookies,$timeout,$location, Socket, UserDetails) {
    $rootScope.$emit('setSel',sessionStorage['circle'])
      $scope.messageListStyle = {
            'max-height':'100%'
        }
    $scope.messageText = "";
    $scope.template = "";
    $scope.messages=[],$scope.count=[];
    $scope.newarr = [];
    $scope.userID = UserDetails.user._id;
    $scope.partic = [];
    $scope.partic2 = [];  
    $scope.sessionId = UserDetails.user._id;
    $scope.videoInProgress = false;
    $scope.circleID = '';
    $scope.newparticipants = [];
    $scope.newparticipants2 = [];
    $scope.test = [];
    //$scope.commonSidebar = $("#commonSideBar")[0];
    $scope.loader = $("#loader")[0];
    $scope.messageBox = $("#messageBox")[0];
    $scope.messageBox.style.display = "none";
    $scope.loader.style.display = "block";
    $scope.currentUser = UserDetails.user.fullName;
    // sessionStorage['conferenceInProgress'] = 'false';
    $scope.expand = false;
    $scope.expandStatus = false;
    $scope.callingDiv= $("#calling")[0];
    var path = $location.path();
    var x = path.split("/");
    var temp;
    Socket.emit('getMem',x[2]);
    sessionStorage['circle'] = x[2];
    Socket.on('memList',function(data){
    Socket.emit("getFullname",data.creator._id)
    Socket.on("returnFullname", function(datas) {
        $scope.creatorName  = datas.fullName;
        $scope.$apply();
    })
        if(data.creator._id == $scope.userID){
            $scope.adminStatus = false;
            $scope.toolTip = "Call";
            $scope.$apply();
        }else{
            $scope.adminStatus = true;
            $scope.toolTip = "Not an Admin";
            $scope.$apply();
        }
        $scope.circleID = data.circle._id;
        $scope.name = data.circle.circleName;
        $scope.members = data.members;
        for (i = 0; i < $scope.members.length; i++) {
                $scope.newarr.push($scope.members[i].id)
        }
        var auth = false,i = 0;
        if(data.creator._id == UserDetails.user._id){
            Socket.emit('get_grp_messages',x[2]);
        }else{
            $scope.members.forEach(function(member){
                if(member.id == UserDetails.user._id){
                    auth = true;
                    i++;
                    if(i>=$scope.members.length){
                        if(auth == false){
                            $location.path('/')
                        }else{
                            Socket.emit('get_grp_messages',x[2]);
                        }
                    }
                }else{
                    i++;
                    if(i>=$scope.members.length){
                        if(auth == false){
                            $location.path('/')
                        }else{
                            Socket.emit('get_grp_messages',x[2]);
                        }
                    }
                }
            })
        }
    })
 
    Socket.emit("getcircleUserList",sessionStorage['circle']);
    Socket.on('circleUserslist',function(data){
        $scope.circleUserName = data;
        $scope.$apply();
    })
    var y = 0,p,t;
    $scope.img = UserDetails.user.profilePicture;
    // Profile picture
    function fn_GetImage(){
        $scope.img = UserDetails.user.profilePicture;
        $scope.$apply();
    }
    var otrImgs = [];
    $scope.user = function(data){
        if(data.senderName == UserDetails.user.username){
            return true;
        }
        else
            return false;
    }
    $scope.otrUser = function(data){
        if(data.senderName == UserDetails.user.username){
            return false;
        }
        else
            return true;
    }
    var data = [];
    Socket.on('grp_messages',function(temp){

        $scope.messages = [];
        data = temp.sort(comp);
        var dividerText = null;
        var tempMessages = [];
        for(i=0;i<data.length;i++){
            var d = new Date(data[i].date);
            var dNow = new Date();
            if(d.getMonth()==dNow.getMonth()&&d.getDate()==dNow.getDate()){
                dividerText = "Today";
            }else if((dNow.getDate()-d.getDate()==1)&&(dNow.getMonth()==d.getMonth())){
                dividerText = "YesterDay";
            }else{
                switch(d.getMonth()){
                    case 0:
                        var mnth = "January";
                        break;
                    case 1:
                        var mnth = "February";
                        break;
                    case 2:
                        var mnth = "March";
                        break;
                    case 3:
                        var mnth = "April";
                        break;
                    case 4:
                        var mnth = "May";
                        break;
                    case 5:
                        var mnth = "June";
                        break;
                    case 6:
                        var mnth = "July";
                        break;
                    case 7:
                        var mnth = "August";
                        break;
                    case 8:
                        var mnth = "September";
                        break;
                    case 9:
                        var mnth = "October";
                        break;
                    case 10:
                        var mnth = "November";
                        break;
                    case 11:
                        var mnth = "December";
                        break;
                }
                dividerText = ""+mnth+" "+d.getDate()+"";
            }
            if(i>0&&i<=temp.length-1){
                if(tempDividerText == dividerText){
                    var newMsg = {
                        id : data[i]._id,
                        sender : data[i].sender,
                        receiver : data[i].receiver,
                        senderName : data[i].senderName,
                        img : data[i].senderPic,
                        date : data[i].date,
                        message : data[i].message,
                        show : true
                    }
                    if((tempMessages[tempMessages.length-1].sender == newMsg.sender)&&((new Date(newMsg.date).getTime()-new Date(tempMessages[tempMessages.length-1].date).getTime())<=(60*1000)))
                        newMsg.show = false;
                    tempMessages.push(newMsg);
                    if (i==data.length-1) {
                        var msgPerDay = {
                            dayDividerText : dividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.$apply();
                        $('#messageBoxList').scrollTop(100000000);
                        p = $('#messageBoxList').scrollTop();
                        $scope.count.push([])
                    };
                }
                else{
                    var msgPerDay = {
                        dayDividerText : tempDividerText,
                        msgs : tempMessages
                    }
                    $scope.messages.push(msgPerDay);
                    tempMessages = [];
                    tempDividerText = dividerText;
                    var newMsg = {
                        id : data[i]._id,
                        sender : data[i].sender,
                        receiver : data[i].receiver,
                        senderName : data[i].senderName,
                        img : data[i].senderPic,
                        date : data[i].date,
                        message : data[i].message,
                        show : true
                    }
                    tempMessages.push(newMsg);
                    if (i==data.length-1) {
                        var msgPerDay = {
                            dayDividerText : dividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.$apply();
                        $('#messageBoxList').scrollTop(100000000);
                        p = $('#messageBoxList').scrollTop();
                    };
                }
            }else if(i==0){
                var newMsg = {
                        id : data[i]._id,
                        sender : data[i].sender,
                        receiver : data[i].receiver,
                        senderName : data[i].senderName,
                        img : data[i].senderPic,
                        message : data[i].message,
                        date : data[i].date,
                        show : true
                }
                tempMessages.push(newMsg);
                tempDividerText = dividerText;
                if (data.length==1) {
                    var msgPerDay = {
                        dayDividerText : dividerText,
                        msgs : tempMessages
                    }
                    $scope.messages.push(msgPerDay);
                    $scope.$apply();
                };    
            }
        };
       
        $timeout(function() {
            $scope.messageBox.style.display = "block";
            $scope.loader.style.display = "none";
        }, 2000);
    })
    function comp(a, b) {
        return new Date(a.date).getTime() - new Date(b.date).getTime();
    }
    $('#messageBoxList').bind('scroll', function(){
        if($(this).scrollTop()==0){
            y+=25;
            var dat = {
                id:sessionStorage['circle'],
                skip: y
            }
            Socket.emit('getMoreGrp',dat)
        }          
    })
    Socket.on('nextGrpMessages',function(temp){
            var t = data.concat(temp);
            data = t.sort(comp);
            $scope.messages = [];
            var dividerText = null;
            var tempDividerText = null;
            var tempMessages = [];
            for(i=0;i<data.length;i++){
                var d = new Date(data[i].date);
                var dNow = new Date();
                if(d.getMonth()==dNow.getMonth()&&d.getDate()==dNow.getDate()){
                    dividerText = "Today";
                }else if((dNow.getDate()-d.getDate()==1)&&(dNow.getMonth()==d.getMonth())){
                    dividerText = "YesterDay";
                }else{
                    switch(d.getMonth()){
                        case 0:
                            var mnth = "January";
                            break;
                        case 1:
                            var mnth = "February";
                            break;
                        case 2:
                            var mnth = "March";
                            break;
                        case 3:
                            var mnth = "April";
                            break;
                        case 4:
                            var mnth = "May";
                            break;
                        case 5:
                            var mnth = "June";
                            break;
                        case 6:
                            var mnth = "July";
                            break;
                        case 7:
                            var mnth = "August";
                            break;
                        case 8:
                            var mnth = "September";
                            break;
                        case 9:
                            var mnth = "October";
                            break;
                        case 10:
                            var mnth = "November";
                            break;
                        case 11:
                            var mnth = "December";
                            break;
                    }
                    dividerText = ""+mnth+" "+d.getDate()+"";
                }
                if(i>0&&i<=data.length-1){
                    if(tempDividerText == dividerText){
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            img : data[i].senderPic,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        if((tempMessages[tempMessages.length-1].sender == newMsg.sender)&&((new Date(newMsg.date).getTime()-new Date(tempMessages[tempMessages.length-1].date).getTime())<=(60*1000)))
                            newMsg.show = false;
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000)
                            var t = $('#messageBoxList').scrollTop();
                            $('#messageBoxList').scrollTop(t-p);
                            p = t;  
                        };
                    }
                    else{
                        var msgPerDay = {
                            dayDividerText : tempDividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        tempMessages = [];
                        tempDividerText = dividerText;
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            img : data.senderPic,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000)
                            var t = $('#messageBoxList').scrollTop();
                            $('#messageBoxList').scrollTop(t-p);
                            p = t;
                        };
                    }
                }else if(i==0){
                    var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            message : data[i].message,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            show : true
                    }
                    tempMessages.push(newMsg);
                    tempDividerText = dividerText;
                    if (data.length==1) {
                        var msgPerDay = {
                            dayDividerText : dividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.$apply();
                    };    
                }
            }
    })
    $scope.fn_Send = function(ev){
        var newMsg = {
            sender: UserDetails.user._id,
            senderName: UserDetails.user.username,
            senderPic: UserDetails.user.profilePicture,
            receiver: sessionStorage['circle'],
            message: $scope.messageText,
            date: new Date(),
            show : true
        }
            $scope.messageText = "";
            if($scope.messages.length >=1)
            if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == newMsg.sender)&&(new Date(newMsg.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                newMsg.show = false;
            if($scope.messages.length>=1){
            if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                $scope.messages[$scope.messages.length-1].msgs.push(newMsg);
                Socket.emit('grpMsgSent',newMsg);
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('grpMsgSent',newMsg);
            }
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('grpMsgSent',newMsg);
            }
            $scope.count.push([]);
    }
    Socket.on('recGrpMsg',function(data){
        if(data.receiver == sessionStorage['circle']&&data.senderName!=UserDetails.user.username){
            var x = {
                sender: data.sender,
                senderName: data.senderName,
                receiver: data.receiver,
                message: data.message,
                date: data.date,
                show : true,
                img : data.senderPic
            }
            var n = {
                id : sessionStorage['circle'],
                uid : UserDetails.user._id
            }
            Socket.emit('iRead', n);
            if($scope.messages.length>=1){
                if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                    if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == x.sender)&&(new Date(x.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                        x.show = false;
                    $scope.messages[$scope.messages.length-1].msgs.push(x);
                    $scope.count.push([]);
                }else{
                    var tempMessages = [];
                    if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == x.sender))
                        x.show = false;
                    tempMessages.push(x);
                    var msgPerDay = {
                        dayDividerText : "Today",
                        msgs : tempMessages
                    }
                    $scope.messages.push(msgPerDay);
                    $scope.count.push([]);
                    $scope.$apply();
                }
            }else{
                var tempMessages = [];
                tempMessages.push(x);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                $scope.$apply();
            }
        }
    })
    var emoticonsPeople = ["bowtie", "smile", "laughing", "blush", "smiley", "relaxed",
        "smirk", "heart_eyes", "kissing_heart", "kissing_closed_eyes", "flushed",
        "relieved", "satisfied", "grin", "wink", "stuck_out_tongue_winking_eye",
        "stuck_out_tongue_closed_eyes", "grinning", "kissing",
        "kissing_smiling_eyes", "stuck_out_tongue", "sleeping", "worried",
        "frowning", "anguished", "open_mouth", "grimacing", "confused", "hushed",
        "expressionless", "unamused", "sweat_smile", "sweat",
        "disappointed_relieved", "weary", "pensive", "disappointed", "confounded",
        "fearful", "cold_sweat", "persevere", "cry", "sob", "joy", "astonished",
        "scream", "neckbeard", "tired_face", "angry", "rage", "triumph", "sleepy",
        "yum", "mask", "sunglasses", "dizzy_face", "imp", "smiling_imp",
        "neutral_face", "no_mouth", "innocent","thumbsup", "-1", "thumbsdown",
        "ok_hand", "punch", "facepunch", "fist", "v", "wave", "hand", "raised_hand",
        "open_hands", "point_up", "point_down", "point_left", "point_right",
        "raised_hands", "pray", "point_up_2", "clap", "muscle", "metal", "fu",
        "walking", "runner", "running", "couple", "family", "two_men_holding_hands",
        "two_women_holding_hands", "dancer", "dancers", "ok_woman", "no_good",
        "information_desk_person", "raising_hand", "bride_with_veil",
        "person_with_pouting_face", "person_frowning", "bow", "couplekiss",
        "couple_with_heart", "massage", "haircut", "nail_care", "boy", "girl",
        "woman", "man", "baby", "older_woman", "older_man",
        "person_with_blond_hair", "man_with_gua_pi_mao", "man_with_turban",
        "construction_worker", "cop", "guardsman", "angel", "princess",
        "smile_cat", "heart_eyes_cat", "kissing_cat", "smirk_cat", "scream_cat",
        "crying_cat_face", "joy_cat", "pouting_cat", "japanese_ogre",
        "japanese_goblin", "see_no_evil", "hear_no_evil", "speak_no_evil",
        "skull","trollface", "poop", "fire", "sparkles", "star", "star2", "dizzy", "boom",
        "collision", "anger", , "zzz", "dash", "sweat_drops", "feet", "lips", "kiss", "droplet", "ear", "eyes",
        "nose", "tongue", "shoe", "sandal", "high_heel", "lipstick", "boot", "shirt",
        "tshirt", "necktie", "womans_clothes", "dress", "running_shirt_with_sash",
        "jeans", "kimono", "bikini", "ribbon", "tophat", "crown", "womans_hat",
        "mans_shoe", "closed_umbrella", "briefcase", "handbag", "pouch", "purse",
        "eyeglasses","yellow_heart",
        "blue_heart", "purple_heart", "heart", "green_heart", "broken_heart",
        "heartbeat", "heartpulse", "two_hearts", "revolving_hearts", "cupid",
        "sparkling_heart", "love_letter", "bust_in_silhouette",
        "busts_in_silhouette", "speech_balloon", "thought_balloon"];
    var emoticonsAnimals = ["cat", "dog",
        "mouse", "hamster", "rabbit", "wolf", "frog", "tiger", "koala", "bear",
        "pig", "pig_nose", "cow", "boar", "monkey_face", "monkey", "horse",
        "racehorse", "camel", "sheep", "elephant", "panda_face", "snake", "bird",
        "baby_chick", "hatched_chick", "hatching_chick", "chicken", "penguin",
        "turtle", "bug", "honeybee", "ant", "beetle", "snail", "octopus",
        "tropical_fish", "fish", "whale", "whale2", "dolphin", "cow2", "ram", "rat",
        "water_buffalo", "tiger2", "rabbit2", "dragon", "goat", "rooster", "dog2",
        "pig2", "mouse2", "ox", "dragon_face", "blowfish", "crocodile",
        "dromedary_camel", "leopard", "cat2", "poodle", "octocat", "squirrel",
        , "fishing_pole_and_fish", "paw_prints", "bouquet",
        "cherry_blossom", "tulip", "four_leaf_clover", "rose", "sunflower",
        "hibiscus", "maple_leaf", "leaves", "fallen_leaf", "herb", "mushroom",
        "cactus", "palm_tree", "evergreen_tree", "deciduous_tree", "chestnut",
        "seedling", "blossom", "ear_of_rice", "shell", "globe_with_meridians",
        "sun_with_face", "full_moon_with_face", "new_moon_with_face", "new_moon",
        "waxing_crescent_moon", "first_quarter_moon", "waxing_gibbous_moon",
        "full_moon", "waning_gibbous_moon", "last_quarter_moon",
        "waning_crescent_moon", "last_quarter_moon_with_face",
        "first_quarter_moon_with_face", "moon", "earth_africa", "earth_americas",
        "earth_asia", "volcano", "milky_way", "partly_sunny","sunny", "umbrella", "cloud",
        "snowflake", "snowman", "zap", "cyclone", "foggy", "ocean"];
    var emoticonsTools = ["bamboo", "gift_heart", "dolls", "school_satchel", "mortar_board", "flags",
        "fireworks", "sparkler", "wind_chime", "rice_scene", "jack_o_lantern",
        "ghost", "santa", "christmas_tree", "gift", "bell", "no_bell",
        "tanabata_tree", "tada", "confetti_ball", "balloon", "crystal_ball", "cd",
        "dvd", "floppy_disk", "camera", "video_camera", "movie_camera", "computer",
        "tv", "iphone", "phone", "telephone_receiver", "pager", "fax",
        "minidisc", "vhs", "sound", "speaker", "mute", "loudspeaker", "mega",
        "hourglass", "hourglass_flowing_sand", "alarm_clock", "watch", "radio",
        "satellite", "loop", "mag", "mag_right", "unlock", "lock",
        "lock_with_ink_pen", "closed_lock_with_key", "key", "bulb", "flashlight",
        "high_brightness", "low_brightness", "electric_plug", "battery", "calling",
        "email", "mailbox", "postbox", "bath", "bathtub", "shower", "toilet",
        "wrench", "nut_and_bolt", "hammer", "seat", "moneybag", "yen", "dollar",
        "pound", "euro", "credit_card", "money_with_wings", "e-mail", "inbox_tray",
        "outbox_tray", "envelope", "incoming_envelope", "postal_horn",
        "mailbox_closed", "mailbox_with_mail", "mailbox_with_no_mail", "door",
        "smoking", "bomb", "gun", "hocho", "pill", "syringe", "page_facing_up",
        "page_with_curl", "bookmark_tabs", "bar_chart", "chart_with_upwards_trend",
        "chart_with_downwards_trend", "scroll", "clipboard", "calendar", "date",
        "card_index", "file_folder", "open_file_folder", "scissors", "pushpin",
        "paperclip", "black_nib", "pencil2", "straight_ruler", "triangular_ruler",
        "closed_book", "green_book", "blue_book", "orange_book", "notebook",
        "notebook_with_decorative_cover", "ledger", "books", "bookmark",
        "name_badge", "microscope", "telescope", "newspaper", "football",
        "basketball", "soccer", "baseball", "tennis", "8ball", "rugby_football",
        "bowling", "golf", "mountain_bicyclist", "bicyclist", "horse_racing",
        "snowboarder", "swimmer", "surfer", "ski", "spades", "hearts", "clubs",
        "diamonds", "gem", "ring", "trophy", "notes", "musical_note", "musical_score", "musical_keyboard",
        "violin", "space_invader", "video_game", "black_joker",
        "flower_playing_cards", "game_die", "dart", "mahjong", "clapper", "memo",
        "pencil", "book", "art", "microphone", "headphones", "trumpet", "saxophone",
        "guitar","coffee", "tea", "sake",
        "baby_bottle", "beer", "beers", "cocktail", "tropical_drink", "wine_glass",
        "fork_and_knife", "pizza", "hamburger", "fries", "poultry_leg",
        "meat_on_bone", "spaghetti", "curry", "fried_shrimp", "bento", "sushi",
        "fish_cake", "rice_ball", "rice_cracker", "rice", "ramen", "stew", "oden",
        "dango", "egg", "bread", "doughnut", "custard", "icecream", "ice_cream",
        "shaved_ice", "birthday", "cake", "cookie", "chocolate_bar", "candy",
        "lollipop", "honey_pot", "apple", "green_apple", "tangerine", "lemon",
        "cherries", "grapes", "watermelon", "strawberry", "peach", "melon",
        "banana", "pear", "pineapple", "sweet_potato", "eggplant", "tomato", "corn"];
    var emoticonsVehicles = ["house", "house_with_garden", "school", "office", "post_office", "hospital",
        "bank", "convenience_store", "love_hotel", "hotel", "wedding", "church",
        "department_store", "european_post_office", "city_sunrise", "city_sunset",
        "japanese_castle", "european_castle", "tent", "factory", "tokyo_tower",
        "japan", "mount_fuji", "sunrise_over_mountains", "sunrise", "stars",
        "statue_of_liberty", "bridge_at_night", "carousel_horse", "rainbow",
        "ferris_wheel", "fountain", "roller_coaster", "ship", "speedboat", "boat",
        "sailboat", "rowboat", "anchor", "rocket", "airplane", "helicopter",
        "steam_locomotive", "tram", "mountain_railway", "bike", "aerial_tramway",
        "suspension_railway", "mountain_cableway", "tractor", "blue_car",
        "oncoming_automobile", "car", "red_car", "taxi", "oncoming_taxi",
        "articulated_lorry", "bus", "oncoming_bus", "rotating_light", "police_car",
        "oncoming_police_car", "fire_engine", "ambulance", "minibus", "truck",
        "train", "station", "train2", "bullettrain_front", "bullettrain_side",
        "light_rail", "monorail", "railway_car", "trolleybus", "ticket", "fuelpump",
        "vertical_traffic_light", "traffic_light", "warning", "construction",
        "beginner", "atm", "slot_machine", "busstop", "barber", "hotsprings",
        "checkered_flag", "crossed_flags", "izakaya_lantern", "moyai",
        "circus_tent", "performing_arts", "round_pushpin",
        "triangular_flag_on_post", "jp", "kr", "cn", "us", "fr", "es", "it", "ru",
        "gb", "uk", "de"];
    var emoticonsSymbols = ["one", "two", "three", "four", "five", "six", "seven",
        "eight", "nine", "keycap_ten", "1234", "zero", "hash", "symbols",
        "arrow_backward", "arrow_down", "arrow_forward", "arrow_left",
        "capital_abcd", "abcd", "abc", "arrow_lower_left", "arrow_lower_right",
        "arrow_right", "arrow_up", "arrow_upper_left", "arrow_upper_right",
        "arrow_double_down", "arrow_double_up", "arrow_down_small",
        "arrow_heading_down", "arrow_heading_up", "leftwards_arrow_with_hook",
        "arrow_right_hook", "left_right_arrow", "arrow_up_down", "arrow_up_small",
        "arrows_clockwise", "arrows_counterclockwise", "rewind", "fast_forward",
        "information_source", "ok", "twisted_rightwards_arrows", "repeat",
        "repeat_one", "new", "top", "up", "cool", "free", "ng", "cinema", "koko",
        "signal_strength", "u5272", "u5408", "u55b6", "u6307", "u6708", "u6709",
        "u6e80", "u7121", "u7533", "u7a7a", "u7981", "sa", "restroom", "mens",
        "womens", "baby_symbol", "no_smoking", "parking", "wheelchair", "metro",
        "baggage_claim", "accept", "wc", "potable_water", "put_litter_in_its_place",
        "secret", "congratulations", "m", "passport_control", "left_luggage",
        "customs", "ideograph_advantage", "cl", "sos", "id", "no_entry_sign",
        "underage", "no_mobile_phones", "do_not_litter", "non-potable_water",
        "no_bicycles", "no_pedestrians", "children_crossing", "no_entry",
        "eight_spoked_asterisk", "eight_pointed_black_star", "heart_decoration",
        "vs", "vibration_mode", "mobile_phone_off", "chart", "currency_exchange",
        "aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpius",
        "sagittarius", "capricorn", "aquarius", "pisces", "ophiuchus",
        "six_pointed_star", "negative_squared_cross_mark", "a", "b", "ab", "o2",
        "diamond_shape_with_a_dot_inside", "recycle", "end", "on", "soon","exclamation", "question", "grey_exclamation",
        "grey_question", "clock1",
        "clock130", "clock10", "clock1030", "clock11", "clock1130", "clock12",
        "clock1230", "clock2", "clock230", "clock3", "clock330", "clock4",
        "clock430", "clock5", "clock530", "clock6", "clock630", "clock7",
        "clock730", "clock8", "clock830", "clock9", "clock930", "heavy_dollar_sign",
        "copyright", "registered", "tm", "x", "heavy_exclamation_mark", "bangbang",
        "interrobang", "o", "heavy_multiplication_x", "heavy_plus_sign",
        "heavy_minus_sign", "heavy_division_sign", "white_flower", "100",
        "heavy_check_mark", "ballot_box_with_check", "radio_button", "link",
        "curly_loop", "wavy_dash", "part_alternation_mark", "trident",
        "black_square", "white_square", "white_check_mark", "black_square_button",
        "white_square_button", "black_circle", "white_circle", "red_circle",
        "large_blue_circle", "large_blue_diamond", "large_orange_diamond",
        "small_blue_diamond", "small_orange_diamond", "small_red_triangle",
        "small_red_triangle_down"];
    $scope.smiliesPeople = [];
    $scope.smiliesAnimals = [];
    $scope.smiliesTools = [];
    $scope.smiliesVehicles = [];
    $scope.smiliesSymbols = [];
    $scope.smileyDivStyle = {"display":"none"};
    emoticonsPeople.forEach(function(data){
        var emo = {
            class : "emoticon emoticon-"+data+"",
            title : ":"+data+":"
        }
        $scope.smiliesPeople.push(emo);
    })
    emoticonsAnimals.forEach(function(data){
        var emo = {
            class : "emoticon emoticon-"+data+"",
            title : ":"+data+":"
        }
        $scope.smiliesAnimals.push(emo);
    })
    emoticonsTools.forEach(function(data){
        var emo = {
            class : "emoticon emoticon-"+data+"",
            title : ":"+data+":"
        }
        $scope.smiliesTools.push(emo);
    })
    emoticonsVehicles.forEach(function(data){
        var emo = {
            class : "emoticon emoticon-"+data+"",
            title : ":"+data+":"
        }
        $scope.smiliesVehicles.push(emo);
    })
    emoticonsSymbols.forEach(function(data){
        var emo = {
            class : "emoticon emoticon-"+data+"",
            title : ":"+data+":"
        }
        $scope.smiliesSymbols.push(emo);
    })
    $scope.showEmoPeople = true;
    $scope.showEmoAnimals = false;
    $scope.showEmoTools = false;
    $scope.showEmoVehicles = false;
    $scope.showEmoSymbols = false;
    $scope.emoPeopleStyle = {"border-bottom":"3px solid #357ebd"};
    $scope.emoAnimalsStyle = null;
    $scope.emoToolsStyle = null;
    $scope.emoVehiclesStyle = null;
    $scope.emoSymbolsStyle = null;
    $scope.emoPeople = function(){
        $scope.showEmoPeople = true;
        $scope.showEmoAnimals = false;
        $scope.showEmoTools = false;
        $scope.showEmoVehicles = false;
        $scope.showEmoSymbols = false;
        $scope.emoPeopleStyle = {"border-bottom":"3px solid #357ebd"};
        $scope.emoAnimalsStyle = null;
        $scope.emoToolsStyle = null;
        $scope.emoVehiclesStyle = null;
        $scope.emoSymbolsStyle = null;
    }
    $scope.emoAnimals = function(){
        $scope.showEmoPeople = false;
        $scope.showEmoAnimals = true;
        $scope.showEmoTools = false;
        $scope.showEmoVehicles = false;
        $scope.showEmoSymbols = false;
        $scope.emoPeopleStyle = null;
        $scope.emoAnimalsStyle = {"border-bottom":"3px solid #357ebd"};
        $scope.emoToolsStyle = null;
        $scope.emoVehiclesStyle = null;
        $scope.emoSymbolsStyle = null;
    }
    $scope.emoTools = function(){
        $scope.showEmoPeople = false;
        $scope.showEmoAnimals = false;
        $scope.showEmoTools = true;
        $scope.showEmoVehicles = false;
        $scope.showEmoSymbols = false;
        $scope.emoPeopleStyle = null;
        $scope.emoAnimalsStyle = null;
        $scope.emoToolsStyle = {"border-bottom":"3px solid #357ebd"};
        $scope.emoVehiclesStyle = null;
        $scope.emoSymbolsStyle = null;
    }
    $scope.emoVehicles = function(){
        $scope.showEmoPeople = false;
        $scope.showEmoAnimals = false;
        $scope.showEmoTools = false;
        $scope.showEmoVehicles = true;
        $scope.showEmoSymbols = false;
        $scope.emoPeopleStyle = null;
        $scope.emoAnimalsStyle = null;
        $scope.emoToolsStyle = null;
        $scope.emoVehiclesStyle = {"border-bottom":"3px solid #357ebd"};
        $scope.emoSymbolsStyle = null;
    }
    $scope.emoSymbols = function(){
        $scope.showEmoPeople = false;
        $scope.showEmoAnimals = false;
        $scope.showEmoTools = false;
        $scope.showEmoVehicles = false;
        $scope.showEmoSymbols = true;
        $scope.emoPeopleStyle = null;
        $scope.emoAnimalsStyle = null;
        $scope.emoToolsStyle = null;
        $scope.emoVehiclesStyle = null;
        $scope.emoSymbolsStyle = {"border-bottom":"3px solid #357ebd"};
    }
    $scope.addToText=function(title){
        $scope.messageText += title;
        $scope.smileyDivStyle = {"display":"none"};
        $('#cInput').focus();
    }
    var config = {};
    $scope.scrollbar = function(direction, autoResize, show) {
        config.direction = direction;
        config.autoResize = autoResize;
        config.scrollbar = {
            show: !!show,
            color: '#fbc064'
        };
        return config;
    }
    $scope.openSmileyMenu = function(){
        $timeout(function(){
            $scope.smileyDivStyle = {"display":"block"}
        });
        click();
    }
    function click(){
        $(document).bind('click', function (e) {
            if (($(e.target).closest(".smileySelectDiv").length === 0)&&($(e.target).closest(".smileyPopupButton").length === 0)) {
                $scope.smileyDivStyle={"display":"none"};
                $scope.$apply();
                $(document).unbind('click');
            }
        });
    }
/*------------------------------File Upload------------------------------------*/
        $scope.showAlert = function(alert) {
            $mdDialog.show({
                locals: {
                    alert: alert
                },
                controller: DialogController,
                templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
        }
        function DialogController($scope, $mdDialog, alert) {
            $scope.alertData = alert
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
   $scope.duplicte = 0;     
   $scope.setFile = function(element){
            $scope.userData = [];
            $scope.file = element.files[0];
            $scope.size = element.files[0].size;
            if($scope.size > 100000000){
                var flg=0;
            }else{
                var flg=1;
            }
            $scope.flname = $scope.file.name;
            $scope.exten = $scope.flname.split('.').pop();
            $scope.withoutExte = $scope.flname.split('.');
            var filetypes=["ppt","pptx","doc","docx","xls","xlsx","pdf","mp4","mp3","webm","wav","jpg","jpeg","png","gif","mkv","zip","rar","js","txt","css","json"];
            var flag ;
            if (filetypes.indexOf($scope.exten) > -1){
                flag=0
            }else{
                flag=-1
            }
            if(flag == 0 && flg == 1){
                if(sessionStorage['circle'] != undefined ){ 
                    Socket.emit('circleRecevierDetails',sessionStorage['circle']);
                    Socket.emit("getFileNames",sessionStorage['circle']);
                }else{
                       var alert = {
                        title : "Error",
                        content: "Circle Not Defined"
                    }
                    $scope.showAlert(alert);
                }
                Socket.on('fileNameList',function(data){
                    for(var i = 0 ; i < data.length;i++){
                        if(data[i] == sessionStorage['circle']+'_'+$scope.flname || data[i] == sessionStorage['circle']+'_'+$scope.withoutExte[0]+'.pdf' ){
                            $scope.duplicte = 1;
                        }
                    }
                })
                Socket.once('circleRecevierDetailsResult',function(data){
                    $scope.circleName  = data.circleName;
                });
                $('#fileDetails').modal('show');
            }else if(flag == -1){
                var alert = {
                    title : "Invalid Extention",
                    content: "Not a valid Extention"
                }
                 $scope.showAlert(alert);
            }else if(flg == 0){
                var alert = {
                    title : "Size Limit",
                    content: "Size Limit Exceeded"
                }
                 $scope.showAlert(alert);
            }    
    }
    $scope.removeFile = function(){
        $('#removeFile').modal('hide')
        $scope.fileIds = $("#flid").text();
        Socket.emit("removeCirclefile",$scope.fileIds);
    }
    Socket.on('circleFiledeleted',function(data){
        Socket.emit('get_grp_messages',sessionStorage['circle']);
    })
    Socket.on("returncircleFileDeleted",function(){
        Socket.emit('get_grp_messages',sessionStorage['circle']);
    })
    $scope.uploadFile=function(){
             $('#upld').attr('disabled','disabled');
            var currentdate = new Date(); 
            var date_time = currentdate.getDate() + "/"
                      + (currentdate.getMonth()+1)  + "/" 
                      + currentdate.getFullYear() + " @ "  
                      + currentdate.getHours() + ":"  
                      + currentdate.getMinutes() + ":" 
                      + currentdate.getSeconds();
            $('div.progress').show();
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append('files', $scope.file);
            formData.append('userId',sessionStorage['circle']);
            formData.append('datetime',date_time);
            formData.append('shareduserID',sessionStorage['circle']);
            formData.append('comments',$scope.commentText);
            formData.append('uploadext',$scope.exten);
            formData.append('duplicteStatus',$scope.duplicte);
            xhr.open('post', '/uploadSharedFiles', true);
            xhr.upload.onprogress = function(e) {
                if (e.lengthComputable) {
                     var percentage = (e.loaded / e.total) * 100;
                             $('div.progress div').css('width', percentage.toFixed(0) + '%');
                             $('div.progress div').html(percentage.toFixed(0) + '%');
                }
            };
            xhr.onload = function(){
                    $('div.progress div').css('width','0%');
                    $('div.progress').hide();
                    $('#fileDetails').modal('hide');
                    $('#upld').removeAttr('disabled');
              var resp = xhr.responseText;
              var respArray = resp.split(',');
              // var template;
              if($scope.exten == "jpg" || $scope.exten == "jpeg" || $scope.exten == "gif" ||  $scope.exten == "png"){
                        $scope.template ='  &nbsp;<p><b>'+ respArray[2] +' &nbsp Uploaded &nbsp;<a  style="cursor:pointer" onclick=fn_removeFile("'+ respArray[4] +'")>Remove</a></b></p><br><img  src="uploadedfiles/'+respArray[2]+'"  width="300" height="200" />'
                }else if($scope.exten == "mp4" || $scope.exten == "webm" ||  $scope.exten == "mkv"  ||  $scope.exten == "wav"){
                        $scope.template ='  &nbsp;<p><b>'+ respArray[2] +' &nbsp Uploaded &nbsp;<a  style="cursor:pointer" onclick=fn_removeFile("'+ respArray[4] +'")>Remove</a></b></p><br><video  src="uploadedfiles/'+respArray[2]+'"  width="450" height="240"  controls ></video>'
                }else if($scope.exten == "mp3"){
                        $scope.template ='  &nbsp;<p><b>'+ respArray[2] +' &nbsp Uploaded &nbsp;<a  style="cursor:pointer" onclick=fn_removeFile("'+ respArray[4] +'")>Remove</a></b></p><br><audio  src="uploadedfiles/'+respArray[2]+'"  controls ></<audio>'
                }else if($scope.exten == "pptx" ||  $scope.exten == "ppt" || $scope.exten == "docx" || $scope.exten == "doc" || $scope.exten == "js" || $scope.exten == "json" || $scope.exten == "css" || $scope.exten == "txt" || $scope.exten == "pdf"){
                        $scope.template ='  &nbsp;<p><b>'+ respArray[2] +' &nbsp Uploaded &nbsp;<a  style="cursor:pointer" onclick=fn_removeFile("'+ respArray[4] +'")>Remove</a></b></p><br><embed  src="uploadedfiles/'+respArray[2]+'"  width="500" height="200" />'
                }else{
                        $scope.template ='  &nbsp;<p><b>'+ respArray[2] +'</a>&nbsp Uploaded &nbsp;<a  style="cursor:pointer" onclick=fn_removeFile("'+ respArray[4] +'")>Remove</a></b></p><br>' 
                }
              var alert = {
                    title :  respArray[3],
                    content: respArray[0]
              }
             $scope.showAlert(alert);
                var newMsg = {
                    sender: UserDetails.user._id,
                    senderName: UserDetails.user.username,
                    senderPic: UserDetails.user.profilePicture,
                    receiver: sessionStorage['circle'],
                    message: $scope.template,
                    date: new Date(),
                    show : true,
                    fileid:respArray[4]
                }
            $scope.messageText = "";
            if($scope.messages.length >=1)
            if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == newMsg.sender)&&(new Date(newMsg.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                newMsg.show = false;
            if($scope.messages.length>=1){
            if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                $scope.messages[$scope.messages.length-1].msgs.push(newMsg);
                Socket.emit('grpMsgSent',newMsg);
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('grpMsgSent',newMsg);
            }
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('grpMsgSent',newMsg);
            }
            $scope.count.push([]);
        };
         xhr.send(formData);
         $scope.commentText="";
         $("#upload-file").val('');
   }     
   /*---------------------------------------------------------------------------------------------------------*/  
        if(sessionStorage['oldroomId'] ){
            var res = sessionStorage['oldroomId'].split(",");
            //console.log("roomname",res[0]); 
            console.log("circleid",res[1]); 
            if(res[1] == sessionStorage['circle']){
                console.log("roomname",res[0])
                $scope.videoInProgress = true;
                $scope.messageListStyle = {
                            'max-height':'calc(100% - 300px)'
                        }
                sessionStorage['conferenceId'] = res[0];
                sessionStorage['conferenceInProgress'] = 'true';  
                var data = {
                    id: "joinRoom",
                    sessionId: UserDetails.user._id,
                    roomName: sessionStorage['conferenceId']
                };
                console.log("data",data);
                //$scope.sendMessage(data);  
                Socket.emit("message", data);
            }
        }
        var incomingListenerCleanUp = $rootScope.$on('invitetoCall',function(ev, data){
                $scope.videoInProgress = true;
                $scope.messageListStyle = {
                            'max-height':'calc(100% - 300px)'
                        }   
                sessionStorage['conferenceId'] = data.roomName;
                sessionStorage['conferenceInProgress'] = 'true';
                $scope.join(sessionStorage['conferenceId'])
        })
        Socket.on('userDeclinedMsg', function(from) {
            if($scope.partic2.length == 0){
                sessionStorage['conferenceInProgress'] = 'false';
            }
            var alert = {
                title: "Reject Call",
                content: from + ' reject your call'
            }
            $scope.showAlert(alert);
        });
        Socket.once('inCAllReject', function(from) {
            // if(count < 2){
            //    sessionStorage['conferenceInProgress'] = 'false';
            // }        
            var alert = {
                title: "Call Rejected",
                content: from + ' is on another call.'
            }
            $scope.showAlert(alert);
        });
        $scope.submitSchedule = function(){
            $('#scheduleCall').modal('hide');
            if($scope.confdate == null){
                var alert = {
                    title: "Confrence Date",
                    content: 'Please Select the Confrence Date '
                }
                $scope.showAlert(alert);
            }else if($scope.confTime==null) {
                var alert = {
                    title: "Confrence Time",
                    content: 'Please Select the Confrence Time '
                }
                $scope.showAlert(alert);
            }else{
                var selDate = new Date($scope.confdate.getFullYear(),$scope.confdate.getMonth(),$scope.confdate.getDate(),$scope.confTime.getHours(),$scope.confTime.getMinutes());
                var currentDate = new Date();
                if(selDate <= currentDate) {
                       var alert = {
                                title: "Invalid Date",
                                content: 'Selected date is Invalid.Please Select a valid date'
                            }
                            $scope.showAlert(alert);
                }else{
                        $scope.confdetails = {
                                          name    :$scope.name,
                                          creator :UserDetails.user,
                                          members :$scope.members,
                                          confdate:selDate,
                                          circleID:$scope.circleID 
                                        }
                      Socket.emit("confrenceDetails",$scope.confdetails)    
                      Socket.once('schedulesaved', function (data){
                            var alert = {
                                title: "Schedule Confrence",
                                content: 'Schedule Confrence  '+data.subject+' Saved'
                              }
                      $scope.showAlert(alert);
                      $scope.confdate = "";
                      $scope.confTime = "";
                      $scope.confdetails = "";
                      $scope.$apply();
                    })
                }    
            }   
        }
        var incomingListener = $rootScope.$on('invitetoConf',function(ev, data){
            schdule(data)
        })
        function schdule(data){
            $scope.newar=[];
                for (i = 0; i < data.members.length; i++) {
                  $scope.newar.push(data.members[i].id)
                } 
            $scope.videoInProgress = true;
            $scope.messageListStyle = {
                            'max-height':'calc(100% - 300px)'
                        }   
            $scope.roomName = data.creator + '' + new Date().getTime();
            sessionStorage['conferenceId'] = $scope.roomName;
            $scope.register($scope.newar, $scope.roomName);
            //$scope.commonSidebar.style.pointerEvents = 'none';
            sessionStorage['conferenceInProgress'] = 'true';
        }
        $scope.scheduleCall = function(){
            $('#scheduleCall').modal('show');
        }
        $scope.liveVideoCall =function(){  
            $scope.videoInProgress = true;
            $scope.messageListStyle = {
                                'max-height':'calc(100% - 300px)'
                            }   
            $scope.roomName = UserDetails.user._id + '' + new Date().getTime();
            sessionStorage['conferenceId'] = $scope.roomName;
            $scope.register($scope.newarr, $scope.roomName);
            //$scope.commonSidebar.style.pointerEvents = 'none';
            sessionStorage['conferenceInProgress'] = 'true';
        }
        $scope.disconnectCall = function() {
            $scope.callingDiv.style.display ="none";
            Socket.emit("disconnectCall", $scope.userID);
            var participant = $scope.participants[$scope.userID];
            participant.dispose();
            delete $scope.participants[$scope.userID];
            $scope.mainVideo.pause();
            $scope.mainVideo.src = "";
                    $scope.mainVideo.load();
                    $scope.mainVideo.style.display = "none";
                    for (i=0;i<$scope.partic.length;i++) {
                         $("#" + $scope.partic[i]).remove();
                         var tt = $scope.partic[i].split('-');
                         $("#div-"+ tt[1]).remove();
                    };
            sessionStorage['conferenceInProgress'] = 'false';
            //$scope.commonSidebar.style.pointerEvents = 'auto';
            sessionStorage['conferenceId'] = null;
            sessionStorage['oldroomId'] = null
            $scope.videoInProgress = false;
                $scope.messageListStyle = {
                    'max-height':'100%'
                }
        }
        $scope.addnewParticipant = function (){
            Socket.emit('get_friends_sidebar',UserDetails.user._id);
            Socket.on('friends_sidebar', function(data) {
                //console.log("new participant",data);
                 frndList = data;
                 $scope.contactsOnline = [];
                 if (data.length >= 1) {
                     var i = 0;
                     $scope.got_online_contact_onadd = false;
                     data.forEach(function(dat) {
                         i++;
                         if (dat.status == 'online') {
                             var onlineContact = {
                                _id: dat._id,
                                username: dat.username,
                                fullName: dat.fullName
                             }
                             $scope.got_online_contact_onadd = true;
                             $scope.contactsOnline.push(onlineContact);
                         }
                         if (i >= data.length) {
                            $scope.$apply();
                         }
                     })
                 } else {
                     $scope.$apply(function() {
                        $scope.got_online_contact_onadd = false;
                        $scope.contactsOnline = data;
                     });
                 }
            })
                $('#newparticipantList').modal('show');
        }
        // add to live call list
        $scope.addtoList = function(id,name){
            if($scope.newparticipants.indexOf(id) == -1){
               if($scope.newparticipants.length <1){ 
                    $scope.newparticipants.push(id);
                    var dt = {_id:id,name:name};
                    $scope.newparticipants2.push(dt);
                }   
            }  
        }
        // remove from the live call list
        $scope.removefromList = function(id){
            for (var i = 0; i < $scope.newparticipants.length; i++) {
              if ($scope.newparticipants[i] === id) {
                $scope.newparticipants.splice(i, 1);
                i--;
              }
            }
            for (var i = 0; i < $scope.newparticipants2.length; i++) {
              if ($scope.newparticipants2[i]._id === id) {
                $scope.newparticipants2.splice(i, 1);
                i--;
              }
            }
        }
        function getUserDetails(id){
            Socket.emit("getFullname",id)
            Socket.on("returnFullname", function(data) {
                $scope.callName  = data.fullName;
                $scope.callerPic = data.profilePicture; 
                $scope.callid = data._id;
                $scope.$apply();
            })
        }
        // add participant to the confrence 
        $scope.addtoConf = function(){
            var index;
            for (var i=0; i< $scope.partic2.length; i++) {
                index = $scope.newparticipants.indexOf($scope.partic2[i]);
                if (index > -1) {
                    $scope.newparticipants.splice(index, 1);
                }
            }
            $('#newparticipantList').modal('hide');
            var dataToAdd = {
                circle  : sessionStorage['circle'],
                members : $scope.newparticipants2
            }
            $scope.oldparticipant = $scope.newparticipants2[0]._id;
            Socket.emit("checkinCircle",{userid:$scope.newparticipants2[0]._id,circleid:sessionStorage['circle']});
            Socket.once('checkinCircleResult', function(count) { 
                if(count == 0){
                    getUserDetails($scope.newparticipants2[0]._id)
                    $scope.callingDiv.style.display ="block";
                    Socket.emit('addToCircleinConf',dataToAdd);
                    Socket.on("addedToCircleConf", function() {
                        var newUser = {roomName:sessionStorage['conferenceId'],addedBy:UserDetails.user,participants: $scope.newparticipants,circleID:sessionStorage['circle']};
                        Socket.emit("newuserAdd",newUser);
                        sessionStorage['conferenceInProgress'] = 'true';
                        $scope.newarr = [];
                        Socket.emit('getMem',sessionStorage['circle']);
                          $scope.newparticipants  = [];
                          $scope.newparticipants2 = [];
                    });
                } else {
                    $scope.newparticipants  = [];
                    $scope.newparticipants2 = [];
                    if ($.inArray($scope.oldparticipant , $scope.partic2) != -1){
                      var alert = {
                                    title: "Already Exist",
                                    content: 'This User Already in this Call'
                                }
                        $scope.showAlert(alert);
                    }else{
                        getUserDetails($scope.oldparticipant)
                        $scope.callingDiv.style.display ="block";
                        $scope.test.push($scope.oldparticipant)
                        var newUser = {roomName:sessionStorage['conferenceId'],addedBy:UserDetails.user,participants: $scope.test,circleID:sessionStorage['circle']};
                        Socket.emit("newuserAdd",newUser);
                        sessionStorage['conferenceInProgress'] = 'true';
                        $scope.test = [];
                    }
                }
            })
        }
        Socket.on("confAccepted", function(data) {
           $scope.callingDiv.style.display ="none";
        })
        $scope.endSingleCall = function(){
            $scope.callingDiv.style.display ="none";
            Socket.emit("endspecificCall",$scope.callid);
        } 
        /* ---------------------------------------------------- Audio/Video Controls for Main Video-------------------------*/
        $scope.muteCall = function() {
            $scope.mute = $("#mute_call")[0]
            $scope.mute.style.visibility = "hidden";
            $scope.unmute = $("#unmute_call")[0]
            $scope.unmute.style.visibility = "visible";
            $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = false;
        }
        $scope.unmuteCall = function() {
            $scope.unmute = $("#unmute_call")[0]
            $scope.unmute.style.visibility = "hidden";
            $scope.mute = $("#mute_call")[0]
            $scope.mute.style.visibility = "visible";
            $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getAudioTracks()[0].enabled = true;
        }
        $scope.hideVideo = function() {
            $scope.removeVideo = $("#remove_video")[0];
            $scope.removeVideo.style.visibility = "hidden";
            $scope.showVideos = $("#show_video")[0];
            $scope.showVideos.style.visibility = "visible";
            $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = false;
        }
        $scope.showVideo = function() {
            $scope.showVideos = $("#show_video")[0];
            $scope.showVideos.style.visibility = "hidden";
            $scope.removeVideo = $("#remove_video")[0];
            $scope.removeVideo.style.visibility = "visible";
            $scope.participants[$scope.userID].rtcPeer.peerConnection.getLocalStreams()[0].getVideoTracks()[0].enabled = true;
        }
        /*------------------------------------------------- Main Video Audio/Video Controls ends Here--------------------------*/
        /*------------------------------------------------------------ Kurento User Side Fuctions ----------------------------- */
        function Participant(id) {
            this.id = id;
            this.rtcPeer = null;
            this.iceCandidateQueue = [];
        }
        Participant.prototype.offerToReceiveVideo = function(error, offerSdp) {
            if (error) {
                return console.error("sdp offer error");
            }
            var msg = {
                id: "receiveVideoFrom",
                user: $scope.userID,
                sender: this.id,
                sdpOffer: offerSdp
            };
            $scope.sendMessage(msg);
        };
        Participant.prototype.onIceCandidate = function(candidate) {
            var message = {
                id: 'onIceCandidate',
                user: $scope.userID,
                candidate: candidate,
                sender: this.id
            };
            $scope.sendMessage(message);
        };
        Participant.prototype.dispose = function() {
            this.rtcPeer.dispose();
            this.rtcPeer = null;
        };
        $scope.mainVideoCurrentId = null;
        $scope.mainVideo = null;
        $scope.participants = {};
        $scope.mainVideo = $("#main_video")[0];

        window.onbeforeunload = function() {
            $scope.mainVideo.pause();
            $scope.mainVideo.src = "";
            $scope.mainVideo.style.display = "none";
            $scope.mainVideo.load();
            sessionStorage['oldroomId'] = null
            Socket.emit("disconnectCall", $scope.userID);
        };
        // message handler
        Socket.on("message", function(message) {
            switch (message.id) {
                case "existingParticipants":
                    $scope.onExistingParticipants(message);
                    break;
                case "newParticipantArrived":
                    $scope.onNewParticipant(message);
                    break;
                case "participantLeft":
                    $scope.onParticipantLeft(message);
                    break;
                case "receiveVideoAnswer":
                    $scope.onReceiveVideoAnswer(message);
                    break;
                case "iceCandidate":
                    var participant = $scope.participants[message.sessionId];
                    if (participant != null) {
                        participant.rtcPeer.addIceCandidate(message.candidate, function(error) {
                            if (error) {
                                if (message.sessionId === $scope.sessionId) {
                                    console.error("Error adding candidate to self : " + error);
                                } else {
                                    console.error("Error adding candidate : " + error);
                                }
                            }
                        });
                    } else {
                        console.error('still does not establish rtc peer for : ' + message.sessionId);
                    }
                    break;
                default:
                    console.error("Unrecognized message: ", message);
            }
        });

        $scope.expandVideo = function(ids) {
            $scope.videoClicked = $("#video-"+ids)[0];
               if($scope.expand == false){
                 $('#video-'+ids).animate({marginLeft: 250, marginTop: 0, height: 300, width: 300}, 'slow');
                      for (var i = 0; i < $scope.partic.length;i++) {
                        if($scope.partic [i]!= "video-"+ids){
                             $scope.otherVideos = document.getElementById($scope.partic[i]);
                             $scope.otherVideos.style.display = "none";
                        }
                      }
                  $scope.expand = true;
                }else{
                  $('#video-'+ids).animate({marginLeft: 0, marginTop: 0, height: 150, width: 200}, 'slow');
                    for (var i = 0; i < $scope.partic.length;i++) {
                        if($scope.partic [i]!= "video-"+ids){
                             $scope.otherVideos = document.getElementById($scope.partic[i]);
                             $scope.otherVideos.style.display = "block";
                        }
                    }
                   $scope.expand = false;
                 // $scope.expandStatus =false;
                }
        }
        $scope.sendMessage = function(data) {
            Socket.emit("message", data);
        }
        $scope.register = function(members, roomName) {
            var data = {
                id: "createRoom",
                admin: UserDetails.user,
                members: members,
                roomName: roomName,
                circleID:sessionStorage['circle']  
            };
            $scope.sendMessage(data);
        }
        $scope.join = function(room) {
            var data = {
                id: "joinRoom",
                sessionId: UserDetails.user._id,
                roomName: room
            };
            $scope.sendMessage(data);
        }
        $scope.onExistingParticipants = function(message) {
            var audioConstraints = {
                mandatory : {
                    googEchoCancellation: true,
                    googNoiseSuppression: true
                    // googEchoCancellation: false, 
                    // googAutoGainControl: true,
                    // googNoiseSuppression: true,
                    // googHighpassFilter: true,
                    // googTypingNoiseDetection: true,
                }
            };
            var constraints = {
                audio: audioConstraints,
                video: {
                    width:  { min: 320 },
                    height: { min: 320 },
                }
            };
            // create video for current user to send to server
            var localParticipant = new Participant($scope.sessionId);
            $scope.participants[$scope.sessionId] = localParticipant;
            var video = $scope.createVideoForParticipant(localParticipant);
            // bind function so that calling 'this' in that function will receive the current instance
            var options = {
                localVideo: video,
                mediaConstraints: constraints,
                onicecandidate: localParticipant.onIceCandidate.bind(localParticipant)
            };
            localParticipant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
                if (error) {
                    return console.error(error);
                }
                // initial main video to local first
                $scope.mainVideoCurrentId = $scope.sessionId;
                $scope.mainVideo.src = localParticipant.rtcPeer.localVideo.src;
                $scope.mainVideo.muted = true;
                $scope.mainVideo.style.display = "block";
                this.generateOffer(localParticipant.offerToReceiveVideo.bind(localParticipant));
            });
            for (var i in message.data) {
                $scope.receiveVideoFrom(message.data[i]);
            }
        }
        $scope.receiveVideoFrom = function(sender) {
            var participant = new Participant(sender);
            $scope.participants[sender] = participant;
            var video = $scope.createVideoForParticipant(participant);
            // bind function so that calling 'this' in that function will receive the current instance
            var options = {
                remoteVideo: video,
                onicecandidate: participant.onIceCandidate.bind(participant)
            };
            participant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
                if (error) {
                    return console.error(error);
                }
                this.generateOffer(participant.offerToReceiveVideo.bind(participant));
            });
        }
        $scope.onNewParticipant = function(message) {
            $scope.receiveVideoFrom(message.new_user_id)
        }
        $scope.onParticipantLeft = function(message) {
            var participant = $scope.participants[message.sessionId];
            console.log("participant",participant.id)
            participant.dispose();
            delete $scope.participants[message.sessionId];
            $("#video-" + participant.id).remove();
            $("#div-"+ participant.id).remove();
            $("#span-"+participant.id).remove();
            for (var i = $scope.partic.length - 1; i >= 0; i--) {
                console.log("participant list id ",$scope.partic[i]);
                if ($scope.partic[i] === "video-" + participant.id) {
                    $scope.partic.splice(i, 1);
                }
            }
            if($scope.partic.length == 0){
                sessionStorage['conferenceInProgress'] = 'false';
                //$scope.disconnectCall();
            }
            for (var i = $scope.partic2.length - 1; i >= 0; i--) {
                if ($scope.partic2[i] ===  participant.id) {
                    $scope.partic2.splice(i, 1);
                }
            }
        }
        $scope.onReceiveVideoAnswer = function(message) {
            var participant = $scope.participants[message.sessionId];
            participant.rtcPeer.processAnswer(message.sdpAnswer, function(error) {
                if (error) {
                    console.error(error);
                } else {
                    participant.isAnswer = true;
                    while (participant.iceCandidateQueue.length) {
                        var candidate = participant.iceCandidateQueue.shift();
                        participant.rtcPeer.addIceCandidate(candidate);
                    }
                }
            });
        }
        // Create video for every participants
        $scope.createVideoForParticipant = function(participant) {
           // console.log("createVideoForParticipant",participant.id);
            $scope.usersName = "";
            $scope.videoId = "video-" + participant.id;
            $scope.divId= "div-"+ participant.id;
            $scope.spanId="span-"+  participant.id;
            $scope.btnlist = $("#button_list")[0];
            $scope.btnlist.style.display = "block";
            if ($scope.userID != participant.id) {
                // Socket.emit("getuserNames",participant.id);
                // Socket.on('returnUsername',function(data){
                //    // console.log("returnUsername data",data);
                //     document.getElementById("span-"+ participant.id).innerHTML = data.fullName;
                // })
                $scope.videoHtml = '<div id="' + $scope.divId + '" style="display:inline-block;cursor:pointer;position:relative"   class ="box"><video id="' + $scope.videoId + '"  autoplay ng-click=expandVideo("' + participant.id + '") width="200px"  height="150px" style="cursor:pointer;border:3px solid #357EBD;border-radius:10px;text-transform: capitalize;font-weight: bold;font-size: 15px;" poster="assets/imgs/novideo.png"></video><span id="' + $scope.spanId + '" style="position:relative;top:-129px;left:-199px;text-transform: capitalize;font-weight: bold;font-size: 15px;"></span></div>';
                $scope.partic.push($scope.videoId);
                $scope.partic2.push(participant.id);
            } else {
                $scope.videoHtml = '<video id="' + $scope.videoId + '" autoplay ng-click=expandVideo("' + participant.id + '") width="200px"  height="150px"   style="cursor:pointer;border:3px solid #357ebd;border-radius:10px;display:none"></video>';
            }
            $scope.$apply();
            angular.element($('#video_list')[0]).append($compile($scope.videoHtml)($scope));
            return $("#" + $scope.videoId)[0];
        }
        /*------------------------------------------------------------ Kurento User Side Fuctions  End Here ----------------------------- */
        $scope.fullScreen = function(id){
            var element = $("#video-"+id)[0]
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }
        $scope.$on('$destroy', function(event) {
            console.log("event",event);
            if($scope.partic.length > 0){
                var arrs = [];
                console.log("roomname",sessionStorage['conferenceId'])
                console.log("circle",sessionStorage['circle'])
                arrs.push(sessionStorage['conferenceId']);
                arrs.push(sessionStorage['circle'])
                sessionStorage['oldroomId'] = arrs;
            }
            Socket.emit("disconnectCall", $scope.userID);
            var participant = $scope.participants[$scope.userID];
            if(participant){
                participant.dispose();
                delete $scope.participants[$scope.userID];
                $scope.mainVideo.pause();
                $scope.mainVideo.src = "";
                $scope.mainVideo.load();
                $scope.mainVideo.style.display = "none";
                for (i=0;i<$scope.partic.length;i++) {
                     $("#" + $scope.partic[i]).remove();
                     var tt = $scope.partic[i].split('-');
                     $("#div-"+ tt[1]).remove();
                };
            }
            sessionStorage['conferenceInProgress'] = 'false';
            sessionStorage['conferenceId'] = null;
            Socket.getSocket().removeAllListeners('message');
            Socket.getSocket().removeAllListeners('checkinCircleResult');
            Socket.getSocket().removeAllListeners('userDetailsResponse');
            Socket.getSocket().removeAllListeners('userDeclinedMsg');
            incomingListenerCleanUp();
            incomingListener();
        })         
}]);