angular.module('circles').controller('ViewCirclesController', ['$scope','$rootScope','$mdDialog','$timeout','$location','Socket', 'UserDetails',
    function($scope,$rootScope,$mdDialog,$timeout,$location,Socket, UserDetails) {
        $rootScope.$emit('noSel')
        $scope.memItems = [];
        $scope.selected = [];
        Socket.emit('getMem',sessionStorage['circle']);
        Socket.on('memList',function(data){
            $scope.creator = data.creator;
            $scope.circleName = data.circle.circleName;
            $scope.memItems = data.members;
            if(UserDetails.user._id == data.circle.creator)
                $scope.admin = true;
            else
                $scope.admin = false;
            $scope.$apply();
        })
        // redirect to group chat 
        $scope.fn_GrpChat = function(){
            var id = sessionStorage['circle'];
            $location.path('/conferenceRooms/'+id+'');
        }
        // delete group
        $scope.fn_Delete = function(ev){
            var confirm = $mdDialog.confirm()
            .title('Delete this group?')
            .content('You are about to delete this group. Are you sure you want to do it???')
            .ok('Yes!')
            .cancel('No!')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                Socket.emit('deleteGroup',sessionStorage['circle']);
                $location.path('/circles');
            }, function() {
                $mdDialog.hide();
            });
        }
        // remove member from a group
        $scope.fn_RemoveMember = function(ev,id){
            var confirm = $mdDialog.confirm()
            .title('Remove Member?')
            .content('You are about to remove this member from this group. Are you sure you want to do it???')
            .ok('Yes!')
            .cancel('No!')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                var x = {
                    circle : sessionStorage['circle'],
                    member : id
                }
                Socket.emit('removeMember',x);
                Socket.emit('getMem',sessionStorage['circle']);
            }, function() {
                $mdDialog.hide();
            });
        }
        $scope.fn_closeSearch = function(){
            $timeout(function(){
                $('#addMemberText').val("");
                $scope.showResults = false;
                $('#addMemberText').focus();
            });
        }
        $scope.fn_findUser = function(query){
            var x = {
                id : UserDetails.user._id,
                q : query
            }
            $scope.showResults = false;
            if(query.length>=1){
                Socket.emit('find',x);
                $timeout(function(){$scope.showResults = true});
            }else if(!$scope.selected.length>=1){
                $timeout(function(){$scope.showResults = false});
                $scope.searchResult = [];
            }else{
                $timeout(function(){$scope.searchResult = [];$scope.showResults = true});
            }
            Socket.on('sel',function(rsp){
                $scope.searchResult = [];
                if(rsp.length>=1){
                    var k = 0;
                    rsp.forEach(function(res){
                        if($scope.memItems.length>=1){
                            var f = true,i = 0;
                            $scope.memItems.forEach(function(member){
                                if(res.username == member.username){
                                    f = false;
                                }
                                i++;
                                if(i>=$scope.memItems.length){
                                    if(f == true){
                                        $scope.searchResult.push(res);
                                        k++;
                                        // console.log("here")
                                    }else{
                                        k++;
                                    }
                                    if(k>=rsp.length){
                                        $scope.newUserSearchResultsStyle = {"display":"block"};
                                        $scope.$apply();
                                    }
                                }
                            })
                        }
                    }) 
                }else{
                    $scope.$apply();
                }
            })
        }
        $scope.fn_toggle = function (item, sel, srch) {
            var idx  = $scope.selected.indexOf(item);
            var idx1 = $scope.searchResult.indexOf(item);
            if (idx > -1){
                $timeout(function(){
                    $scope.selected.splice(idx, 1);
                    $scope.searchResult.push(item);
                    $('#addMemberText').focus();
                })
            }
            else {
                $timeout(function(){
                    $scope.selected.push(item);
                    $scope.searchResult.splice(idx1, 1);
                    $('#addMemberText').focus();
                })
                
            }
        };
        // Leave from group
        $scope.fn_Leave = function(ev){
            var confirm = $mdDialog.confirm()
            .title('Leave this group?')
            .content('You are about to leave this group. Are you sure you want to do it???')
            .ok('Yes!')
            .cancel('No!')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                var data = {
                    member : UserDetails.user._id,
                    circle : sessionStorage['circle']
                }
                Socket.emit('removeMember',data);
                $location.path('/circles');
            }, function() {
                $mdDialog.hide();
            }); 
        }
        // Add users to group
        $scope.fn_AddToGroup = function(ev){
            var dataToAdd = {
                circle : sessionStorage['circle'],
                members : $scope.selected
            }
            Socket.emit('addToCircle',dataToAdd);
            Socket.on('addedToCircle',function(){
                Socket.emit('getMem',sessionStorage['circle'])
                $timeout(function(){
                    $('#addMemberText').val("");
                    $scope.showResults = false;
                    $('#addMemberText').focus();
                    $scope.selected = [];
                    $scope.showMembersAddedAlert(ev)
                });
            })
        }
        $scope.showMembersAddedAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Updated')
                .content('Conference room updated with new members.')
                .ariaLabel('Alert Dialog')
                .ok('Ok!')
                .targetEvent(ev)
            );
        };
    }
]);