angular.module('circles').controller('CirclesController', ['$scope','$rootScope','$timeout','$location','Socket', 'UserDetails',
	function($scope,$rootScope,$timeout,$location,Socket, UserDetails) {
    sessionStorage['selected'] = null;
    $rootScope.$emit('noSel')
    Socket.emit('getOwnCircles',UserDetails.user._id);
    Socket.emit('getOtrCircles',UserDetails.user._id);
    Socket.on('OwnCircles',function(data){
      $scope.urItems = data;
      $scope.$apply();
    })
    Socket.on('OtrCircles',function(data){
      $scope.otrItems = data;
      $scope.$apply();
    })
    $scope.fn_View = function(id){
      sessionStorage['circle'] = id;
      $location.path('/circles/view-circle')
    }
    $scope.fn_Create=function(){
      $location.path('/circles/create');
    }
  }
]);
