angular.module('index').controller('editProfController', ['$scope','$rootScope','$timeout','$mdDialog','$http','$mdSidenav','Socket', 'UserDetails',
  function($scope,$rootScope,$timeout,$mdDialog,$http,$mdSidenav,Socket, UserDetails) {
    $rootScope.$emit('noSel')
    Socket.on('connect',function(data){
          Socket.emit('setStatus',UserDetails.user._id);
    })
  $scope.commonSidebar = $("#commonSideBar")[0];
  $scope.commonSidebar.style.display = "none";
    var edited = false;
    // toggling the sidebar to left
    $scope.toggleLeft = function(){$mdSidenav('left').toggle() ;}
    $scope.usr = UserDetails.user;
    $scope.editProfNameInputText = "";
    $scope.editProfDesigInputText = "";
    $scope.editProfCompanyInputText = "";
    $scope.nameEdit = false;
    if(!($scope.usr.designation&&$scope.usr.companyName)){
      $scope.desigEdit = true;
    }else{
      $scope.desigEdit = false;
    }
    // edit name in profile
    $scope.fn_NameEdit = function(){
      $scope.editProfFirstNameInputText = UserDetails.user.firstName;
      $scope.editProfLastNameInputText = UserDetails.user.lastName;
      $scope.nameEdit = true;
    }
    // edit designation
    $scope.fn_DesigEdit = function(){
      $scope.editProfDesigInputText = UserDetails.user.designation;
      $scope.editProfCompanyInputText = UserDetails.user.companyName;
      $scope.desigEdit = true;
    }
    $scope.fn_clearNameEdit = function(){
      $scope.editProfFirstNameInputText = "";
      $scope.editProfLastNameInputText = "";
      $scope.nameEdit = false;
    }
    $scope.fn_AddName = function(){
      $scope.usr.firstName = $('#editProfFirstNameInput').val();
      $scope.usr.lastName = $('#editProfLastNameInput').val();
      $scope.usr.fullName = $('#editProfFirstNameInput').val()+" "+$('#editProfLastNameInput').val();;
      $scope.nameEdit = false;
        edited = true;
    }
    $scope.fn_clearDesigEdit = function(){
      $scope.editProfDesigInputText = "";
      $scope.editProfCompanyInputText = "";
      $scope.desigEdit = false;
    }
    $scope.fn_AddDesig = function(){
      $scope.usr.designation = $('#editProfDesigInput').val();
      $scope.usr.companyName = $('#editProfCompanyInput').val();
      $scope.desigEdit = false;
      edited = true;
    }
    $scope.editAboutMeStyle = {'display':"none"};
    $scope.editEducationStyle = {'display':"none"};
    $scope.editWrkExpStyle = {'display':"none"};
    $scope.editProjectsStyle = {'display':"none"};
    // edit about me
    $scope.fn_EditAboutMe = function(){
      $scope.aboutMeStyle = {'display':"none"};
      $scope.editAboutMeStyle = {'display':"block"};
      $scope.aboutMeText = $scope.usr.aboutMe;
      $('#aboutMeText').focus();
    }
    // edit education details
    $scope.fn_EditEducation = function(){
      $scope.editEducationStyle = {'display':"block"};
    }
    // edit work experiance
    $scope.fn_EditWrkExp = function(){
      $scope.editWrkExpStyle = {'display':"block"};
    }
    // edit project details
    $scope.fn_EditProjects = function(){
      $scope.editProjectsStyle = {'display':"block"};
    }
    // clear aboutme fields
    $scope.fn_clearAboutMe = function(){
      $scope.aboutMeText = "";
      $scope.aboutMeStyle = {'display':"block"};
      $scope.editAboutMeStyle = {'display':"none"};
    }
    // clear education details
    $scope.fn_clearEduEdit = function(){
      $scope.eduCourse = "";
      $scope.eduPeriod = "";
      $scope.eduUty = "";
      $scope.editEducationStyle = {'display':"none"};
    }

    $scope.fn_clearExpEdit = function(){
      $scope.expDesig = "";
      $scope.expPrd = "";
      $scope.expCmpny = "";
      $scope.editWrkExpStyle = {'display':"none"};
    }
    $scope.fn_clearProject = function(){
      $scope.projectName = "";
      $scope.editProjectsStyle = {'display':"none"};
    }
    $scope.fn_showButton = function(){$scope.changePhotoButtonStyle = {'display':'block'}};
    $scope.fn_hideButton = function(){$scope.changePhotoButtonStyle = {'display':'none'}};
    $scope.fn_AddAboutMe = function(){
      $scope.usr.aboutMe = $scope.aboutMeText;
      $scope.aboutMeText = "";
      $scope.aboutMeStyle = {'display':"block"};
      $scope.editAboutMeStyle = {'display':"none"};
        edited = true;
    }
    // add new education details
    $scope.fn_AddToEdu = function(){
      var newEdu = {
        course: $scope.eduCourse,
        period: $scope.eduPeriod,
        university: $scope.eduUty
      }
      $scope.usr.education.push(newEdu);
      $scope.eduCourse = "";
      $scope.eduPeriod = "";
      $scope.eduUty = "";
      $scope.editEducationStyle = {'display':"none "};
        edited = true;
    }
    // add new work experiance
    $scope.fn_AddToWrkExp = function(){
      var newExp = {
        designation: $scope.expDesig,
        period: $scope.expPrd,
        company: $scope.expCmpny
      }
      $scope.usr.experience.push(newExp);
      $scope.expDesig = "";
      $scope.expPeriod = "";
      $scope.expCmpny = "";
      $scope.editWrkExpStyle = {'display':"none"};
        edited = true;
    }
    $scope.fn_AddToProjects = function(){
      var newProject = {
        project: $scope.projectName,
      }
      $scope.usr.projects.push(newProject);
      $scope.projectName = "";
      $scope.editProjectsStyle = {'display':"none"};
        edited = true;
    }
    // update user proile
    $scope.fn_UpdateProfile = function(ev){
        if (edited == false) {
            //$scope.showAlert(ev,"NOTHING TO UPDATE","No changes were made");
            $scope.usr = UserDetails.user;
             var alert = {
                    title : "NOTHING TO UPDATE",
                    content : "No changes were made"
                  }
            $scope.showAlert(alert);
        }else{
          Socket.emit('updateProfile',$scope.usr);
          edited = false;
        }
      Socket.on('updateComplete',function(updatedUser){
          UserDetails.user = updatedUser;
          $scope.usr = UserDetails.user;
               var alert = {
                  title : "Done",
                  content : "Profile Updated"
              }
          $scope.showAlert(alert);
          $scope.$apply();
      })
    }
    //show alerts using custom templates 
    $scope.showAlert = function(alert) {
        $mdDialog.show({
            locals: {alert:alert},
            controller: DialogController,
            templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true
        })
    };
    // dialog controller
    function DialogController($scope, $mdDialog, alert) {
        $scope.alertData = alert
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    var incomingListen = $rootScope.$on('updtprofPic',function(ev, res){
        $scope.usr = res;
        UserDetails.user = res;
        $scope.$apply()
    });
    $scope.myImage='';
    $scope.myCroppedImage='';
    var handleFileSelect=function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        var ext = $('#fileInput').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['jpg']) == -1) {
            alert('Invalid Extension. Use .jpg images');
            $('#fileInput').val(''); 
            $scope.myImage = null;
            $scope.$apply();
        }else{
            $("#preview").show(); 
            reader.onload = function (evt) {
              $scope.$apply(function($scope){
                 $scope.myImage=evt.target.result;
              });
            };
        }
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    $scope.uploadProfilepicture = function(imgBase64) {
        var profilePicture={id:UserDetails.user._id,image:imgBase64};
        Socket.emit('profilePicturUpload',profilePicture);
        $('#changeProfilepicture').modal('hide');
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $('#fileInput').val('');
    }
    $scope.closeProfile = function(){
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $('#fileInput').val('');
    }
}
])
// .directive('fileModel', ['$parse', function ($parse) {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             var model = $parse(attrs.fileModel);
//             var modelSetter = model.assign;
            
//             element.bind('change', function(){
//                 scope.$apply(function(){
//                     modelSetter(scope, element[0].files[0]);
//                 });
//             });
//         }
//     };
// }])
// .service('fileUpload', ['$http', function ($http) {
//     this.uploadFileToUrl = function(file, uploadUrl){
//         var fd = new FormData();
//         fd.append('file', file);
//         $http.post(uploadUrl, fd, {
//             transformRequest: angular.identity,
//             headers: {'Content-Type': undefined}
//         })
//         .success(function(){
//           alert("done");
//         })
//         .error(function(){
//           alert("err")
//         });
//     }
// }]);
