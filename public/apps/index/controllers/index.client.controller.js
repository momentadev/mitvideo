angular.module('index').controller('IndexController', ['$scope','$rootScope','$timeout','$location','$interval','$mdDialog','$http','Socket', 'UserDetails',
    function($scope,$rootScope,$timeout,$location,$interval,$mdDialog,$http,Socket, UserDetails) {
        $rootScope.$emit('noSel')
        $scope.commonSidebar = $("#commonSideBar")[0];
        $scope.commonSidebar.style.display = "block";
        $scope.fullName = UserDetails.user.fullName;
        $scope.email = UserDetails.user.email;
        $scope.username = UserDetails.user.username;
        $scope.usrid = UserDetails.user._id;
        $scope.picid = UserDetails.user.profilePicture;
        $scope.im = UserDetails.user.profilePicture; 
        sessionStorage['selected'] = null;
        sessionStorage['conferenceInProgress'] = 'false';
        sessionStorage['videoInProgress'] = 'false';
    }])

