angular.module('index').config(['$stateProvider', '$urlRouterProvider',
  	function($stateProvider, $urlRouterProvider) {
  	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('empty', {
			url:'/',
			views: {
				'header': {
					templateUrl: 'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/index/views/index.client.view.html'
				}
			}
		})
		.state('circles',{
			url:'/circles',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/circles/views/view-circles.client.view.html'
				}
			}
		})
		.state('editProfile',{
			url:'/editProfile',
			views: {
				'content': {
					templateUrl:'apps/index/views/editProfile.client.view.html'
				}
			}
		})
		.state('circlesCreate',{
			url:'/circles/create',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/circles/views/create-circles.client.view.html'
				}
			}
		})
		.state('circlesView',{
			url:'/circles/view-circle',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/circles/views/view-circle.client.view.html'
				}
			}
		})
		.state('conference',{
			url:'/conference',
			views: {
				
				'content': {
					templateUrl: 'apps/conference/views/conference.client.view.html'
				}
			}
		})
		.state('conferenceList',{
			url:'/conference/list-conference',
			views: {
				
				'content': {
					templateUrl: 'apps/conference/views/conferenceList.client.view.html'
				}
			}
		})
		
		.state('circlesChat',{
			url:'/conferenceRooms/{id}',
			views: {
				// 'header': {
				// 	templateUrl:'apps/header/views/header.client.view.html'
				// },
				'content': {
					templateUrl: 'apps/circles/views/chat-circle.client.view.html'
				}
			}
		})
		.state('requests',{
			url:'/requests',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/requests/views/request.client.view.html'
				}
			}
		})
		.state('chat',{
			url:'/chat/{id}',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/chat/views/chat.client.view.html'
				}
			}
		})
		.state('viewProfile',{
			url:'/viewProf',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/viewProfile/views/viewProf.client.view.html'
				}
			}
		})
		.state('friends',{
			url:'/friends',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/friends/views/friend.client.view.html'
				}
			}
		})
		.state('fileshare',{
			url:'/fileshare',
			views: {
				'header': {
					templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/fileshare/views/fileshare.client.view.html'
				}
			}
		})
		.state('search',{
			url:'/search',
			views: {
				'header': {
					// templateUrl:'apps/header/views/header.client.view.html'
				},
				'content': {
					templateUrl: 'apps/search/views/search.client.view.html'
				}
			}
		})
    }
]);