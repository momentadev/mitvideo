var mainApplicationModuleName = 'mean';

var mainApplicationModule = angular.module(mainApplicationModuleName, 
	[
		'ngAnimate',
		'ngMaterial',
		'ngSanitize',
		'ui.bootstrap',
		'ngEmbed',
		'ngCookies',
		'ngResource',
		'ngImgCrop',
		'ui.router',
		'ngPDFViewer',
		'user',
		'header',
		'chat',
		'sidebar',
		'index',
		'circles',
		'viewProfile',
		'requests',
		'friends',
		'fileshare',
		'search',
		'conference',
		'mb-scrollbar'
	]);
// Configuring the URL scheme
mainApplicationModule.config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
     // $locationProvider.html5Mode({
     //             enabled: true,
     //             requireBase: false
     //      });

  }
]);

if (window.location.hash === '#_=_') window.location.hash = '#!';
angular.element(document).ready(function() {
  angular.bootstrap(document, [mainApplicationModuleName]);
});