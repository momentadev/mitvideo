angular.module('header').controller('HeaderController', ['$scope','$cookies','$mdDialog','$timeout','$location','Socket', 'UserDetails',
  function($scope,$cookies,$mdDialog,$timeout,$location, Socket, UserDetails) {
		  var self = this;
      var k=0;
      var temp=[];
      self.simulateQuery = false;
    	self.isDisabled    = false;
      $scope.firstName = UserDetails.user.firstName;
      $scope.lastName = UserDetails.user.lastName;
      $scope.email = UserDetails.user.email;
      $scope.username = UserDetails.user.username;
      $scope.fullName = UserDetails.user.fullName;
      $scope.reslts = false;
      // search users in every key up
      this.srch = function(query) {
      if(query.length>=1){
        var str=query.split(" ");
          if(str.length==1)
              Socket.emit('txt',str[0]); 
          else
              Socket.emit('srch',str);
        Socket.once('res',function(qw){
          if(qw.length>=1){
            $scope.reslts = true;
            temp = [];
            qw.forEach(function(result){
              var res = {
                id : result._id,
                fullName : result.fullName,
                username : result.username,
                img : result.profilePicture
              }
              temp.push(res);
              if(temp.length >= qw.length){
                  $scope.results = temp;
                  k=0;
                  $scope.select.selectedNode = $scope.results[k];
                  $scope.style={"display":"block"};
                  $scope.$apply();
              }
            })
          }
          else{
              $scope.reslts = false;
              $scope.results = null;
              $scope.style={"display":"block"};
              $scope.$apply();
          }
          $(document).bind('click', function (e) {
            if ($(e.target).closest(".searchForm").length === 0) {
              $scope.key = null;
              $scope.results = null;
              $scope.style={"display":"none"};
              $scope.$apply();
              $(document).unbind('click');
            }
          });
        });
      }
      else{
        $scope.results = null;
        $scope.style={"display":"none"};
      }
    };
      // Socket.on('connect',function(data){
      //     Socket.emit('setStatus',UserDetails.user._id);
      // }) 

      // toggling function
      $scope.fn_Toggle = function(ev){
        if(ev.keyCode==40){
          if(k==$scope.results.length-1)
            k=0;
          else
            k++;
          $scope.select.selectedNode = $scope.results[k];
        }else if(ev.keyCode==38){
          if(k==0)
            k=$scope.results.length-1;
          else
            k--;
          $scope.select.selectedNode = $scope.results[k];
        }else if(ev.keyCode == 13){
          if($scope.reslts)
          self.viewP(temp[k].id);
        }
      }
      // view profile
      self.viewP=function(id){
        $scope.key = null;
        $(document).unbind('click');
        $scope.results = null;
        $scope.style={"display":"none"};
        sessionStorage['id'] = id;
        var n={
            uid: UserDetails.user._id,
            id: sessionStorage['id']
        }
        Socket.emit('get_profInfo',n)
        Socket.emit('get_check',n);
        Socket.emit('get_friend',n);
        $location.path('/viewProf')
      }
    	self.find=function(ev){
        if(self.searchText!=null&&self.searchText!=""){
    		$mdDialog.show({
      			controller: DialogController,
      			templateUrl: 'apps/header/views/dialog1.tmpl.html',
      			parent: angular.element(document.body),
      			targetEvent: ev,
      			clickOutsideToClose:true
    		})
        self.st=self.searchText;
        self.searchText="";
        } else
          alert("Enter Something First")
    	}
    	function DialogController($scope, $mdDialog) {
        var s=self.st.split(" ");
        if(s.length==1)
          Socket.emit('txt',s[0]);
        else
          Socket.emit('srch',s);
        Socket.once('res',function(qw){
          $scope.items=qw;
        });
    		self.res=null;
        $scope.fn_View=function(id){
            $mdDialog.hide();
            sessionStorage['id']=id;
            var n={
              uid: UserDetails.user._id,
              id: sessionStorage['id']
            }
            Socket.emit('get',n);
            $location.path('/viewProf');
        }
		  }
/*--------------------Advaced Search--------------------------*/
    $scope.fn_advanceSearch = function(){
      $location.path('/search')
    }
/*-------------------------------------------------------------*/

    $("#search").keypress(function(event){
      var ew = event.which;
      if(ew == 32)
          return true;
      if(48 <= ew && ew <= 57)
          return true;
      if(65 <= ew && ew <= 90)
          return true;
      if(97 <= ew && ew <= 122)
          return true;
      return false;
    });

}]);	