angular.module('viewProfile').controller('viewProfController', ['$scope','$rootScope','$mdDialog','$cookies','UserDetails','Socket',
	function ($scope,$rootScope,$mdDialog,$cookies,UserDetails,Socket) { 
        $rootScope.$emit('noSel')
        var n={
            uid: UserDetails.user._id,
            id: sessionStorage['id']
        }
        $scope.$on('$destroy', function() {
            Socket.getSocket().removeAllListeners('requestsendPending');
        })
        // console.log("n",n)
        Socket.emit('get_profInfo',n)
        Socket.emit('get_check',n);
        Socket.emit('get_friend',n);
        // profile info 
        Socket.on('profInfo',function(res){
            $scope.usr = res;
            $scope.$apply();
        })
        // check the existence
		Socket.on('check',function(res){
            if(sessionStorage['id']!=UserDetails.user._id)
        	   $scope.check=res;
            else
               $scope.check = false;
        	$scope.$apply();
    	})
        Socket.on('friend',function(data){
            $scope.friend = data;
            $scope.$apply();
        })
        // add new friend to the list
    	$scope.fn_Add = function(){
    		var newReq = {
    			snd: UserDetails.user._id,
    			rec: $scope.usr._id
    		}
            Socket.emit('addF',newReq)
    		$scope.check = false;
    	}
        Socket.on('changeRequestStatus',function(){
            $scope.check = true;
            $scope.$apply();
        })
        Socket.on('requestsendPending',function (data) {
            $scope.check = true;
                var alert = {
                    title : "Request Pending",
                    content : "Friends Request From "+ data.fullName+" is Already Pending.Go to  <a href='/#!/requests'  aria-label='AlertCloseButton' ng-click='cancel()'>Request</a>" 
                }
            $scope.showAlert(alert);
        })
        $scope.showAlert = function(alert) {
            $mdDialog.show({
                locals: {alert:alert},
                controller: DialogController,
                templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true
            })
        };
        // dialog controller
        function DialogController($scope, $mdDialog, alert) {
            $scope.alertData = alert
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        // remove friend from our friends list
        $scope.fn_RemoveFriend = function(ev){
                var confirm = $mdDialog.confirm()
                .title('Unfriend?')
                .content('You are about to remove this user from your friend list. Sure you want to do it???')
                .ok('Yes!')
                .cancel('No!')
                .targetEvent(ev);
                $mdDialog.show(confirm).then(function() {
                  var x = {
                    u : UserDetails.user._id,
                    c : sessionStorage['id']
                  }
                  Socket.emit('removeFriend',x)
                  Socket.emit('get',n)
              }, function() {
                  $mdDialog.hide();
            });
        }
}]);