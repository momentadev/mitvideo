angular.module('chat').controller('chatController', ['$scope','$mdDialog','$rootScope','$interval','$location','$timeout','Socket', 'UserDetails',
    function($scope,$mdDialog,$rootScope,$interval,$location,$timeout, Socket, UserDetails) {
/*-----------------------------------------Video Call-------------------------------------*/

        $scope.commonSidebar = $("#commonSideBar")[0];
        $scope.commonSidebar.style.display = "block";   
        var path = $location.path();
        var x = path.split("/")
        sessionStorage['selected'] = x[2];
        sessionStorage['chatId'] = x[2];
        //$scope.commonSidebar = $("#commonSideBar")[0];
        //$scope.commonSidebar.style.pointerEvents = 'auto';
        $scope.videoInProgress = false;
        sessionStorage['videoInProgress'] = false;
        $scope.IsHiddenmute = true;
        $scope.IsHiddenunmute = false;
        $scope.IsHiddenshowVideo = true;
        $scope.IsHiddenhideVideo = false;
        $scope.messageListStyle = {
            'max-height':'100%'
        }
        $scope.videoInput  = $('#videoInput')[0];
        $scope.videoOutput = $('#videoOutput')[0];
        $scope.callerId = UserDetails.user;
        $scope.calleeId = sessionStorage['chatId'];
        var webRtcPeer;

        Socket.on('testcallResponse',function(message) {
            switch (message.id) {
                case 'callResponse':
                    $scope.callResponse(message);
                    break;
                case 'startCommunication':
                    $scope.startCommunication(message);
                    break;
                case 'stopCommunication':
                    $scope.stop(true);
                    break;
                case 'iceCandidate':
                    webRtcPeer.addIceCandidate(message.candidate)
                    break;
                default:
                    console.error('Unrecognized message', message);
            }
        }); 
        // receive incoming call details  from sidebar controller using rooteScope.
        var incomingListenerCleanUp = $rootScope.$on('incomingVideoCall',function(ev, message){
            $scope.incomingCall(message);
        })

        $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParam){
            // console.log("stateChangeStart",event,toState)
            // if(sessionStorage['videoInProgress'] == 'true'){         
            //     event.preventDefault();
            // } 
            Socket.getSocket().removeAllListeners('testcallResponse')
            Socket.getSocket().removeAllListeners('registerCallerResponse')
            Socket.getSocket().removeAllListeners('registerCalleeResponse')
            incomingListenerCleanUp();
        });
        // call response handle here
        $scope.callResponse = function(message) {
            if (message.response != 'accepted') {
                var alert = {
                    title: "Call Rejected",
                    content: 'Your Call is Rejected.'
                }
                $scope.showAlert(alert);
                var errorMessage = message.message ? message.message
                        : 'Unknown reason for call rejection.';
                $scope.stop(true);
            } else {
                if (webRtcPeer) {
                    webRtcPeer.processAnswer(message.sdpAnswer);
                }    
            }
        }
        // alert meassge handling
        $scope.showAlert = function(alert) {
            $mdDialog.show({
                locals: {
                    alert: alert
                },
                controller: DialogController,
                templateUrl: 'apps/sidebar/views/dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
        }
        // custom dialog controller
        function DialogController($scope, $mdDialog, alert) {
            $scope.alertData = alert
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        // strat communication. sdp send to the server
        $scope.startCommunication = function(message) {
            webRtcPeer.processAnswer(message.sdpAnswer);
        }
        // checking the incall status
        $scope.incomingCall = function(message) {
            if ($scope.videoInProgress == true) {
                var response = {
                    id : 'incomingCallResponse',
                    from : message.from._id,
                    to : UserDetails.user._id,
                    callResponse : 'reject',
                    message : 'bussy'
                };
                return $scope.sendMessage(response);
            }else{
                    $timeout(function(){
                       // $scope.commonSidebar.style.pointerEvents = 'none';
                        $scope.videoInProgress = true;
                        sessionStorage['videoInProgress'] = true;
                        $scope.messageListStyle = {
                            'max-height':'calc(100% - 300px)'
                        }
                    });
                    var options = {
                        localVideo :  $scope.videoInput,
                        remoteVideo : $scope.videoOutput,
                        onicecandidate : $scope.onIceCandidate
                    }

                    webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options,
                    function(error) {
                        if (error) {
                            console.error(error);
                        }

                        this.generateOffer(function(error, offerSdp) {
                            if (error) {
                                console.error(error);
                            }
                            var response = {
                                id : 'incomingCallResponse',
                                from : message.from._id,
                                to : UserDetails.user._id,
                                callResponse : 'accept',
                                sdpOffer : offerSdp
                            };
                            $scope.sendMessage(response);
                        });
                    });
            }
        }
        // register the peer and start the video call
        $scope.startVideoCall = function() {
            console.log("registerCaller")
            Socket.emit('registerCaller',$scope.callerId._id);
        }
        Socket.on('registerCallerResponse',function(callerRegRes){
            console.log("registerCallerResponse")
            if(callerRegRes == true){
                Socket.emit('registerCallee',$scope.calleeId);
            }else{
                var alert = {
                    title :  "Call Failed",
                    content: "Register Caller Failed"
                }
                $scope.showAlert(alert);
            }
        })
        //register call  response
        Socket.on('registerCalleeResponse',function(calleeRegRes){
            if(calleeRegRes == true){
                if($scope.videoInProgress == false){
                    $timeout(function(){
                        //$scope.commonSidebar.style.pointerEvents = 'none';
                        $scope.videoInProgress = true;
                        sessionStorage['videoInProgress'] = true;
                        $scope.messageListStyle = {
                            'max-height':'calc(100% - 300px)'
                        }
                    });
                    var options = {
                        localVideo : $scope.videoInput,
                        remoteVideo : $scope.videoOutput,
                        onicecandidate : $scope.onIceCandidate
                    }
                    webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options, function(error) {
                        if (error) {
                            console.error(error);
                        }
                        this.generateOffer(function(error, offerSdp) {
                            if (error) {
                                console.error(error);
                            }
                            var message = {
                                id : 'call',
                                from : $scope.callerId,
                                to : $scope.calleeId,
                                sdpOffer : offerSdp
                            };
                            $scope.sendMessage(message);
                        });
                    });
                }
            }else{
                var alert = {
                    title :  "Not Available",
                    content: "User Currently Not Available"
                }
                $scope.showAlert(alert);
            }
        })
        // stop video calling
        $scope.stop = function(message) {
            $timeout(function(){
               // $scope.commonSidebar.style.pointerEvents = 'auto';
                $scope.videoInProgress = false;
                sessionStorage['videoInProgress'] = false;
                Socket.emit("callStopbyinitiator",{calleid:$scope.calleeId,callerid:UserDetails.user._id})
                $scope.messageListStyle = {
                    'max-height':'100%'
                }
            })
            if (webRtcPeer) {
                webRtcPeer.dispose();
                webRtcPeer = null;
                if (!message) {
                    var message = {
                        id : 'stop',
                        from: UserDetails.user._id
                    }
                    $scope.sendMessage(message);
                }
            }
           
        }
        // send  message through this function
        $scope.sendMessage  = function(message) {
            var jsonMessage = JSON.stringify(message);
            Socket.emit('TestCallMessage',jsonMessage);
        }
        $scope.onIceCandidate = function(candidate) {
            var message = {
                id : 'onIceCandidate',
                user : UserDetails.user._id,
                candidate : candidate
            }
            $scope.sendMessage(message);
        }
        /*----------------------------------------  Audio/Video controls ------------------------------- */
        $scope.mute = function(){
            $scope.IsHiddenmute   = false;
            $scope.IsHiddenunmute = true;
            if(webRtcPeer.audioEnabled == true){
                webRtcPeer.audioEnabled = false;
            }
        }
        $scope.unmute = function(){
            $scope.IsHiddenmute   = true;
            $scope.IsHiddenunmute = false;
            if(webRtcPeer.audioEnabled == false){
                webRtcPeer.audioEnabled = true;
            }
        }
        $scope.hideVideo = function(){
            $scope.IsHiddenshowVideo = false;
            $scope.IsHiddenhideVideo = true;
            if(webRtcPeer.videoEnabled == true){
                webRtcPeer.videoEnabled = false;
            }
        }
        $scope.showVideo = function(){
            $scope.IsHiddenshowVideo = true;
            $scope.IsHiddenhideVideo=false;
            if(webRtcPeer.videoEnabled == false){
                webRtcPeer.videoEnabled = true;
            }
        }
         /*-------------------------------------------  Audio/Video controls end here ------------------------- */
        $scope.addFile = function(){
            $("#upload-file").click();
        }
        /*---------------------------------------textChat ---------------------------------------*/
        $scope.messageText = "";
        $rootScope.$emit('setSel',sessionStorage['chatId'])
        $scope.usrid =  UserDetails.user._id;
        $scope.img = UserDetails.user.profilePicture;
        function comp(a, b) {
            return new Date(a.date).getTime() - new Date(b.date).getTime();
        }
        $scope.user = function(data){
            if(data.senderName == UserDetails.user.username){
                return true;
            }
            else
                return false;
        }
        $scope.otrUser = function(data){
            if(data.senderName == UserDetails.user.username){
                return false;
            }
            else
                return true;
        }
        var y = 0,p;
        $scope.messages = [],$scope.count=[];
        var n = {
            id:sessionStorage['chatId'],
            uid:UserDetails.user._id
        }
        Socket.emit('allRead', n);
        Socket.emit('get_profInfo',n);
        Socket.emit('get_messages',n);
        Socket.on('statChange',function(id){
            //console.log("state in chat client controller",id);
            if(id == n.id){
                $timeout(function(){Socket.emit('get_profInfo',n)},5000,false);
            }
        })
        Socket.on("shareFileDeleted",function(){
            Socket.emit('get_messages',n);
        })
        Socket.on('profInfo',function(data){
            sessionStorage['selected'] = data._id;
            $scope.img1 = data.profilePicture;
            $scope.usr = data;
            if(data.status=='online')
                $scope.status = true;
            else
                $scope.status = false;
            $scope.$apply()
        })
        var data = [];
        var tempDividerText = null;
        Socket.on('textMessages',function(temp){
            $scope.messages = [];
            data = temp.sort(comp);
            var dividerText = null;
            var tempMessages = [];
            for(i=0;i<data.length;i++){
                var d = new Date(data[i].date);
                var dNow = new Date();
                if(d.getMonth()==dNow.getMonth()&&d.getDate()==dNow.getDate()){
                    dividerText = "Today";
                }else if((dNow.getDate()-d.getDate()==1)&&(dNow.getMonth()==d.getMonth())){
                    dividerText = "YesterDay";
                }else{
                    switch(d.getMonth()){
                        case 0:
                            var mnth = "January";
                            break;
                        case 1:
                            var mnth = "February";
                            break;
                        case 2:
                            var mnth = "March";
                            break;
                        case 3:
                            var mnth = "April";
                            break;
                        case 4:
                            var mnth = "May";
                            break;
                        case 5:
                            var mnth = "June";
                            break;
                        case 6:
                            var mnth = "July";
                            break;
                        case 7:
                            var mnth = "August";
                            break;
                        case 8:
                            var mnth = "September";
                            break;
                        case 9:
                            var mnth = "October";
                            break;
                        case 10:
                            var mnth = "November";
                            break;
                        case 11:
                            var mnth = "December";
                            break;
                    }
                    dividerText = ""+mnth+" "+d.getDate()+"";
                }
                if(i>0&&i<=temp.length-1){
                    if(tempDividerText == dividerText){
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        if((tempMessages[tempMessages.length-1].sender == newMsg.sender)&&((new Date(newMsg.date).getTime()-new Date(tempMessages[tempMessages.length-1].date).getTime())<=(60*1000)))
                            newMsg.show = false;
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000);
                            p = $('#messageBoxList').scrollTop();
                            $scope.count.push([])
                        };
                    }
                    else{
                        var msgPerDay = {
                            dayDividerText : tempDividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        tempMessages = [];
                        tempDividerText = dividerText;
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000);
                            p = $('#messageBoxList').scrollTop();
                        };
                    }
                }else if(i==0){
                    var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            message : data[i].message,
                            date : data[i].date,
                            show : true
                    }
                    tempMessages.push(newMsg);
                    tempDividerText = dividerText;
                    if (data.length==1) {
                        var msgPerDay = {
                            dayDividerText : dividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.$apply();
                    };    
                }
            }
        });
        $('#messageBoxList').bind('scroll', function(){
            if($(this).scrollTop()==0){
                y+=25;
                var dat = {
                    id:sessionStorage['chatId'],
                    uid:UserDetails.user._id,
                    skip: y
                }
                Socket.emit('getMore',dat)
            }
        })    
        Socket.on('nextMessages',function(temp){
            var t = data.concat(temp);
            data = t.sort(comp);
            $scope.messages = [];
            var dividerText = null;
            var tempDividerText = null;
            var tempMessages = [];
            for(i=0;i<data.length;i++){
                var d = new Date(data[i].date);
                var dNow = new Date();
                if(d.getMonth()==dNow.getMonth()&&d.getDate()==dNow.getDate()){
                    dividerText = "Today";
                }else if((dNow.getDate()-d.getDate()==1)&&(dNow.getMonth()==d.getMonth())){
                    dividerText = "YesterDay";
                }else{
                    switch(d.getMonth()){
                        case 0:
                            var mnth = "January";
                            break;
                        case 1:
                            var mnth = "February";
                            break;
                        case 2:
                            var mnth = "March";
                            break;
                        case 3:
                            var mnth = "April";
                            break;
                        case 4:
                            var mnth = "May";
                            break;
                        case 5:
                            var mnth = "June";
                            break;
                        case 6:
                            var mnth = "July";
                            break;
                        case 7:
                            var mnth = "August";
                            break;
                        case 8:
                            var mnth = "September";
                            break;
                        case 9:
                            var mnth = "October";
                            break;
                        case 10:
                            var mnth = "November";
                            break;
                        case 11:
                            var mnth = "December";
                            break;
                    }
                    dividerText = ""+mnth+" "+d.getDate()+"";
                }
                if(i>0&&i<=data.length-1){
                    if(tempDividerText == dividerText){
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        if((tempMessages[tempMessages.length-1].sender == newMsg.sender)&&((new Date(newMsg.date).getTime()-new Date(tempMessages[tempMessages.length-1].date).getTime())<=(60*1000)))
                            newMsg.show = false;
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000)
                            var t = $('#messageBoxList').scrollTop();
                            $('#messageBoxList').scrollTop(t-p);
                            p = t;
                        };
                    }
                    else{
                        var msgPerDay = {
                            dayDividerText : tempDividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        tempMessages = [];
                        tempDividerText = dividerText;
                        var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            message : data[i].message,
                            show : true
                        }
                        tempMessages.push(newMsg);
                        if (i==data.length-1) {
                            var msgPerDay = {
                                dayDividerText : dividerText,
                                msgs : tempMessages
                            }
                            $scope.messages.push(msgPerDay);
                            $scope.$apply();
                            $('#messageBoxList').scrollTop(100000000)
                            var t = $('#messageBoxList').scrollTop();
                            $('#messageBoxList').scrollTop(t-p);
                            p = t;
                        };
                    }
                }else if(i==0){
                    var newMsg = {
                            id : data[i]._id,
                            sender : data[i].sender,
                            receiver : data[i].receiver,
                            message : data[i].message,
                            senderName : data[i].senderName,
                            date : data[i].date,
                            show : true
                    }
                    tempMessages.push(newMsg);
                    tempDividerText = dividerText;
                    if (data.length==1) {
                        var msgPerDay = {
                            dayDividerText : dividerText,
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.$apply();
                    };    
                }
            }
        })
        $scope.fn_Send = function(ev){
            var newMsg = {
                sender : UserDetails.user._id,
                receiver : sessionStorage['chatId'],
                senderName : UserDetails.user.username,
                date : new Date(),
                message : $scope.messageText,
                show : true
            }
            $scope.messageText = "";
            if($scope.messages.length >=1)
            if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == newMsg.sender)&&(new Date(newMsg.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                newMsg.show = false;
            if($scope.messages.length>=1){
            if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                $scope.messages[$scope.messages.length-1].msgs.push(newMsg);
                Socket.emit('sendMsg',newMsg);
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            $scope.count.push([]);
        }
        $scope.openSmileyMenu = function(){
            $timeout(function(){
                $scope.smileyDivStyle = {"display":"block"}
            });
            click();
        }
        function click(){
            $(document).bind('click', function (e) {
                if (($(e.target).closest(".smileySelectDiv").length === 0)&&($(e.target).closest(".smileyPopupButton").length === 0)) {
                    $scope.smileyDivStyle={"display":"none"};
                    $scope.$apply();
                    $(document).unbind('click');
                }
            });
        }
        Socket.on('recMsg',function(data){
            if((data.receiver == UserDetails.user._id)&&(data.sender == sessionStorage['chatId'])){
                var n = {
                    id : sessionStorage['chatId'],
                    uid : UserDetails.user._id
                }
                Socket.emit('allRead', n);
                var x = {
                    sender: data.sender,
                    senderName: data.senderName,
                    receiver: data.receiver,
                    message: data.message,
                    date: data.date,
                    show : true
                }
                if($scope.messages.length>=1){
                    if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                        if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == x.sender)&&(new Date(x.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                            x.show = false;
                        $scope.messages[$scope.messages.length-1].msgs.push(x);
                        $scope.count.push([]);
                    }else{
                        var tempMessages = [];
                        if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == x.sender))
                            x.show = false;
                        tempMessages.push(x);
                        var msgPerDay = {
                            dayDividerText : "Today",
                            msgs : tempMessages
                        }
                        $scope.messages.push(msgPerDay);
                        $scope.count.push([]);
                        $scope.$apply();
                    }
                }else{
                    var tempMessages = [];
                    tempMessages.push(x);
                    var msgPerDay = {
                        dayDividerText : "Today",
                        msgs : tempMessages
                    }
                    $scope.messages.push(msgPerDay);
                    $scope.count.push([]);
                    $scope.$apply();
                }
            }
        })
/*---------------------------------------Emoticons-----------------------------------*/       
        var emoticonsPeople = ["bowtie", "smile", "laughing", "blush", "smiley", "relaxed",
            "smirk", "heart_eyes", "kissing_heart", "kissing_closed_eyes", "flushed",
            "relieved", "satisfied", "grin", "wink", "stuck_out_tongue_winking_eye",
            "stuck_out_tongue_closed_eyes", "grinning", "kissing",
            "kissing_smiling_eyes", "stuck_out_tongue", "sleeping", "worried",
            "frowning", "anguished", "open_mouth", "grimacing", "confused", "hushed",
            "expressionless", "unamused", "sweat_smile", "sweat",
            "disappointed_relieved", "weary", "pensive", "disappointed", "confounded",
            "fearful", "cold_sweat", "persevere", "cry", "sob", "joy", "astonished",
            "scream", "neckbeard", "tired_face", "angry", "rage", "triumph", "sleepy",
            "yum", "mask", "sunglasses", "dizzy_face", "imp", "smiling_imp",
            "neutral_face", "no_mouth", "innocent","thumbsup", "-1", "thumbsdown",
            "ok_hand", "punch", "facepunch", "fist", "v", "wave", "hand", "raised_hand",
            "open_hands", "point_up", "point_down", "point_left", "point_right",
            "raised_hands", "pray", "point_up_2", "clap", "muscle", "metal", "fu",
            "walking", "runner", "running", "couple", "family", "two_men_holding_hands",
            "two_women_holding_hands", "dancer", "dancers", "ok_woman", "no_good",
            "information_desk_person", "raising_hand", "bride_with_veil",
            "person_with_pouting_face", "person_frowning", "bow", "couplekiss",
            "couple_with_heart", "massage", "haircut", "nail_care", "boy", "girl",
            "woman", "man", "baby", "older_woman", "older_man",
            "person_with_blond_hair", "man_with_gua_pi_mao", "man_with_turban",
            "construction_worker", "cop", "guardsman", "angel", "princess",
            "smile_cat", "heart_eyes_cat", "kissing_cat", "smirk_cat", "scream_cat",
            "crying_cat_face", "joy_cat", "pouting_cat", "japanese_ogre",
            "japanese_goblin", "see_no_evil", "hear_no_evil", "speak_no_evil",
            "skull","trollface", "poop", "fire", "sparkles", "star", "star2", "dizzy", "boom",
            "collision", "anger", , "zzz", "dash", "sweat_drops", "feet", "lips", "kiss", "droplet", "ear", "eyes",
            "nose", "tongue", "shoe", "sandal", "high_heel", "lipstick", "boot", "shirt",
            "tshirt", "necktie", "womans_clothes", "dress", "running_shirt_with_sash",
            "jeans", "kimono", "bikini", "ribbon", "tophat", "crown", "womans_hat",
            "mans_shoe", "closed_umbrella", "briefcase", "handbag", "pouch", "purse",
            "eyeglasses","yellow_heart",
            "blue_heart", "purple_heart", "heart", "green_heart", "broken_heart",
            "heartbeat", "heartpulse", "two_hearts", "revolving_hearts", "cupid",
            "sparkling_heart", "love_letter", "bust_in_silhouette",
            "busts_in_silhouette", "speech_balloon", "thought_balloon"];
        var emoticonsAnimals = ["cat", "dog",
            "mouse", "hamster", "rabbit", "wolf", "frog", "tiger", "koala", "bear",
            "pig", "pig_nose", "cow", "boar", "monkey_face", "monkey", "horse",
            "racehorse", "camel", "sheep", "elephant", "panda_face", "snake", "bird",
            "baby_chick", "hatched_chick", "hatching_chick", "chicken", "penguin",
            "turtle", "bug", "honeybee", "ant", "beetle", "snail", "octopus",
            "tropical_fish", "fish", "whale", "whale2", "dolphin", "cow2", "ram", "rat",
            "water_buffalo", "tiger2", "rabbit2", "dragon", "goat", "rooster", "dog2",
            "pig2", "mouse2", "ox", "dragon_face", "blowfish", "crocodile",
            "dromedary_camel", "leopard", "cat2", "poodle", "octocat", "squirrel",
            , "fishing_pole_and_fish", "paw_prints", "bouquet",
            "cherry_blossom", "tulip", "four_leaf_clover", "rose", "sunflower",
            "hibiscus", "maple_leaf", "leaves", "fallen_leaf", "herb", "mushroom",
            "cactus", "palm_tree", "evergreen_tree", "deciduous_tree", "chestnut",
            "seedling", "blossom", "ear_of_rice", "shell", "globe_with_meridians",
            "sun_with_face", "full_moon_with_face", "new_moon_with_face", "new_moon",
            "waxing_crescent_moon", "first_quarter_moon", "waxing_gibbous_moon",
            "full_moon", "waning_gibbous_moon", "last_quarter_moon",
            "waning_crescent_moon", "last_quarter_moon_with_face",
            "first_quarter_moon_with_face", "moon", "earth_africa", "earth_americas",
            "earth_asia", "volcano", "milky_way", "partly_sunny","sunny", "umbrella", "cloud",
            "snowflake", "snowman", "zap", "cyclone", "foggy", "ocean"];
        var emoticonsTools = ["bamboo", "gift_heart", "dolls", "school_satchel", "mortar_board", "flags",
            "fireworks", "sparkler", "wind_chime", "rice_scene", "jack_o_lantern",
            "ghost", "santa", "christmas_tree", "gift", "bell", "no_bell",
            "tanabata_tree", "tada", "confetti_ball", "balloon", "crystal_ball", "cd",
            "dvd", "floppy_disk", "camera", "video_camera", "movie_camera", "computer",
            "tv", "iphone", "phone", "telephone_receiver", "pager", "fax",
            "minidisc", "vhs", "sound", "speaker", "mute", "loudspeaker", "mega",
            "hourglass", "hourglass_flowing_sand", "alarm_clock", "watch", "radio",
            "satellite", "loop", "mag", "mag_right", "unlock", "lock",
            "lock_with_ink_pen", "closed_lock_with_key", "key", "bulb", "flashlight",
            "high_brightness", "low_brightness", "electric_plug", "battery", "calling",
            "email", "mailbox", "postbox", "bath", "bathtub", "shower", "toilet",
            "wrench", "nut_and_bolt", "hammer", "seat", "moneybag", "yen", "dollar",
            "pound", "euro", "credit_card", "money_with_wings", "e-mail", "inbox_tray",
            "outbox_tray", "envelope", "incoming_envelope", "postal_horn",
            "mailbox_closed", "mailbox_with_mail", "mailbox_with_no_mail", "door",
            "smoking", "bomb", "gun", "hocho", "pill", "syringe", "page_facing_up",
            "page_with_curl", "bookmark_tabs", "bar_chart", "chart_with_upwards_trend",
            "chart_with_downwards_trend", "scroll", "clipboard", "calendar", "date",
            "card_index", "file_folder", "open_file_folder", "scissors", "pushpin",
            "paperclip", "black_nib", "pencil2", "straight_ruler", "triangular_ruler",
            "closed_book", "green_book", "blue_book", "orange_book", "notebook",
            "notebook_with_decorative_cover", "ledger", "books", "bookmark",
            "name_badge", "microscope", "telescope", "newspaper", "football",
            "basketball", "soccer", "baseball", "tennis", "8ball", "rugby_football",
            "bowling", "golf", "mountain_bicyclist", "bicyclist", "horse_racing",
            "snowboarder", "swimmer", "surfer", "ski", "spades", "hearts", "clubs",
            "diamonds", "gem", "ring", "trophy", "notes", "musical_note", "musical_score", "musical_keyboard",
            "violin", "space_invader", "video_game", "black_joker",
            "flower_playing_cards", "game_die", "dart", "mahjong", "clapper", "memo",
            "pencil", "book", "art", "microphone", "headphones", "trumpet", "saxophone",
            "guitar","coffee", "tea", "sake",
            "baby_bottle", "beer", "beers", "cocktail", "tropical_drink", "wine_glass",
            "fork_and_knife", "pizza", "hamburger", "fries", "poultry_leg",
            "meat_on_bone", "spaghetti", "curry", "fried_shrimp", "bento", "sushi",
            "fish_cake", "rice_ball", "rice_cracker", "rice", "ramen", "stew", "oden",
            "dango", "egg", "bread", "doughnut", "custard", "icecream", "ice_cream",
            "shaved_ice", "birthday", "cake", "cookie", "chocolate_bar", "candy",
            "lollipop", "honey_pot", "apple", "green_apple", "tangerine", "lemon",
            "cherries", "grapes", "watermelon", "strawberry", "peach", "melon",
            "banana", "pear", "pineapple", "sweet_potato", "eggplant", "tomato", "corn"];
        var emoticonsVehicles = ["house", "house_with_garden", "school", "office", "post_office", "hospital",
            "bank", "convenience_store", "love_hotel", "hotel", "wedding", "church",
            "department_store", "european_post_office", "city_sunrise", "city_sunset",
            "japanese_castle", "european_castle", "tent", "factory", "tokyo_tower",
            "japan", "mount_fuji", "sunrise_over_mountains", "sunrise", "stars",
            "statue_of_liberty", "bridge_at_night", "carousel_horse", "rainbow",
            "ferris_wheel", "fountain", "roller_coaster", "ship", "speedboat", "boat",
            "sailboat", "rowboat", "anchor", "rocket", "airplane", "helicopter",
            "steam_locomotive", "tram", "mountain_railway", "bike", "aerial_tramway",
            "suspension_railway", "mountain_cableway", "tractor", "blue_car",
            "oncoming_automobile", "car", "red_car", "taxi", "oncoming_taxi",
            "articulated_lorry", "bus", "oncoming_bus", "rotating_light", "police_car",
            "oncoming_police_car", "fire_engine", "ambulance", "minibus", "truck",
            "train", "station", "train2", "bullettrain_front", "bullettrain_side",
            "light_rail", "monorail", "railway_car", "trolleybus", "ticket", "fuelpump",
            "vertical_traffic_light", "traffic_light", "warning", "construction",
            "beginner", "atm", "slot_machine", "busstop", "barber", "hotsprings",
            "checkered_flag", "crossed_flags", "izakaya_lantern", "moyai",
            "circus_tent", "performing_arts", "round_pushpin",
            "triangular_flag_on_post", "jp", "kr", "cn", "us", "fr", "es", "it", "ru",
            "gb", "uk", "de"];
        var emoticonsSymbols = ["one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "keycap_ten", "1234", "zero", "hash", "symbols",
            "arrow_backward", "arrow_down", "arrow_forward", "arrow_left",
            "capital_abcd", "abcd", "abc", "arrow_lower_left", "arrow_lower_right",
            "arrow_right", "arrow_up", "arrow_upper_left", "arrow_upper_right",
            "arrow_double_down", "arrow_double_up", "arrow_down_small",
            "arrow_heading_down", "arrow_heading_up", "leftwards_arrow_with_hook",
            "arrow_right_hook", "left_right_arrow", "arrow_up_down", "arrow_up_small",
            "arrows_clockwise", "arrows_counterclockwise", "rewind", "fast_forward",
            "information_source", "ok", "twisted_rightwards_arrows", "repeat",
            "repeat_one", "new", "top", "up", "cool", "free", "ng", "cinema", "koko",
            "signal_strength", "u5272", "u5408", "u55b6", "u6307", "u6708", "u6709",
            "u6e80", "u7121", "u7533", "u7a7a", "u7981", "sa", "restroom", "mens",
            "womens", "baby_symbol", "no_smoking", "parking", "wheelchair", "metro",
            "baggage_claim", "accept", "wc", "potable_water", "put_litter_in_its_place",
            "secret", "congratulations", "m", "passport_control", "left_luggage",
            "customs", "ideograph_advantage", "cl", "sos", "id", "no_entry_sign",
            "underage", "no_mobile_phones", "do_not_litter", "non-potable_water",
            "no_bicycles", "no_pedestrians", "children_crossing", "no_entry",
            "eight_spoked_asterisk", "eight_pointed_black_star", "heart_decoration",
            "vs", "vibration_mode", "mobile_phone_off", "chart", "currency_exchange",
            "aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpius",
            "sagittarius", "capricorn", "aquarius", "pisces", "ophiuchus",
            "six_pointed_star", "negative_squared_cross_mark", "a", "b", "ab", "o2",
            "diamond_shape_with_a_dot_inside", "recycle", "end", "on", "soon","exclamation", "question", "grey_exclamation",
            "grey_question", "clock1",
            "clock130", "clock10", "clock1030", "clock11", "clock1130", "clock12",
            "clock1230", "clock2", "clock230", "clock3", "clock330", "clock4",
            "clock430", "clock5", "clock530", "clock6", "clock630", "clock7",
            "clock730", "clock8", "clock830", "clock9", "clock930", "heavy_dollar_sign",
            "copyright", "registered", "tm", "x", "heavy_exclamation_mark", "bangbang",
            "interrobang", "o", "heavy_multiplication_x", "heavy_plus_sign",
            "heavy_minus_sign", "heavy_division_sign", "white_flower", "100",
            "heavy_check_mark", "ballot_box_with_check", "radio_button", "link",
            "curly_loop", "wavy_dash", "part_alternation_mark", "trident",
            "black_square", "white_square", "white_check_mark", "black_square_button",
            "white_square_button", "black_circle", "white_circle", "red_circle",
            "large_blue_circle", "large_blue_diamond", "large_orange_diamond",
            "small_blue_diamond", "small_orange_diamond", "small_red_triangle",
            "small_red_triangle_down"];
        $scope.smiliesPeople = [];
        $scope.smiliesAnimals = [];
        $scope.smiliesTools = [];
        $scope.smiliesVehicles = [];
        $scope.smiliesSymbols = [];
        $scope.smileyDivStyle = {"display":"none"};
        emoticonsPeople.forEach(function(data){
            var emo = {
                class : "emoticon emoticon-"+data+"",
                title : ":"+data+":"
            }
            $scope.smiliesPeople.push(emo);
        })
        emoticonsAnimals.forEach(function(data){
            var emo = {
                class : "emoticon emoticon-"+data+"",
                title : ":"+data+":"
            }
            $scope.smiliesAnimals.push(emo);
        })
        emoticonsTools.forEach(function(data){
            var emo = {
                class : "emoticon emoticon-"+data+"",
                title : ":"+data+":"
            }
            $scope.smiliesTools.push(emo);
        })
        emoticonsVehicles.forEach(function(data){
            var emo = {
                class : "emoticon emoticon-"+data+"",
                title : ":"+data+":"
            }
            $scope.smiliesVehicles.push(emo);
        })
        emoticonsSymbols.forEach(function(data){
            var emo = {
                class : "emoticon emoticon-"+data+"",
                title : ":"+data+":"
            }
            $scope.smiliesSymbols.push(emo);
        })
        $scope.showEmoPeople = true;
        $scope.showEmoAnimals = false;
        $scope.showEmoTools = false;
        $scope.showEmoVehicles = false;
        $scope.showEmoSymbols = false;
        $scope.emoPeopleStyle = {"border-bottom":"3px solid #357ebd"};
        $scope.emoAnimalsStyle = null;
        $scope.emoToolsStyle = null;
        $scope.emoVehiclesStyle = null;
        $scope.emoSymbolsStyle = null;
        $scope.emoPeople = function(){
            $scope.showEmoPeople = true;
            $scope.showEmoAnimals = false;
            $scope.showEmoTools = false;
            $scope.showEmoVehicles = false;
            $scope.showEmoSymbols = false;
            $scope.emoPeopleStyle = {"border-bottom":"3px solid #357ebd"};
            $scope.emoAnimalsStyle = null;
            $scope.emoToolsStyle = null;
            $scope.emoVehiclesStyle = null;
            $scope.emoSymbolsStyle = null;
        }
        $scope.emoAnimals = function(){
            $scope.showEmoPeople = false;
            $scope.showEmoAnimals = true;
            $scope.showEmoTools = false;
            $scope.showEmoVehicles = false;
            $scope.showEmoSymbols = false;
            $scope.emoPeopleStyle = null;
            $scope.emoAnimalsStyle = {"border-bottom":"3px solid #357ebd"};
            $scope.emoToolsStyle = null;
            $scope.emoVehiclesStyle = null;
            $scope.emoSymbolsStyle = null;
        }
        $scope.emoTools = function(){
            $scope.showEmoPeople = false;
            $scope.showEmoAnimals = false;
            $scope.showEmoTools = true;
            $scope.showEmoVehicles = false;
            $scope.showEmoSymbols = false;
            $scope.emoPeopleStyle = null;
            $scope.emoAnimalsStyle = null;
            $scope.emoToolsStyle = {"border-bottom":"3px solid #357ebd"};
            $scope.emoVehiclesStyle = null;
            $scope.emoSymbolsStyle = null;
        }
        $scope.emoVehicles = function(){
            $scope.showEmoPeople = false;
            $scope.showEmoAnimals = false;
            $scope.showEmoTools = false;
            $scope.showEmoVehicles = true;
            $scope.showEmoSymbols = false;
            $scope.emoPeopleStyle = null;
            $scope.emoAnimalsStyle = null;
            $scope.emoToolsStyle = null;
            $scope.emoVehiclesStyle = {"border-bottom":"3px solid #357ebd"};
            $scope.emoSymbolsStyle = null;
        }
        $scope.emoSymbols = function(){
            $scope.showEmoPeople = false;
            $scope.showEmoAnimals = false;
            $scope.showEmoTools = false;
            $scope.showEmoVehicles = false;
            $scope.showEmoSymbols = true;
            $scope.emoPeopleStyle = null;
            $scope.emoAnimalsStyle = null;
            $scope.emoToolsStyle = null;
            $scope.emoVehiclesStyle = null;
            $scope.emoSymbolsStyle = {"border-bottom":"3px solid #357ebd"};
        }
        $scope.addToText=function(title){
            $scope.messageText += title;
            $scope.smileyDivStyle = {"display":"none"};
            $('#cInput').focus();
        }
        var config = {};
        $scope.scrollbar = function(direction, autoResize, show) {
            config.direction = direction;
            config.autoResize = autoResize;
            config.scrollbar = {
                show: !!show,
                color: '#357ebd'
            };
            return config;
        }
/*--------------------------------------------------------------------Video Message----------------------------------------*/
    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
        }
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
    var uuid = guid();
    var recordVideoSeparately = !!navigator.webkitGetUserMedia;
    $scope.mediaStream = null;
    var startRecording =  $("#start-recording")[0];
    var stopRecording  =  $("#stop-recording")[0]; 
    stopRecording.disabled = true;
    $scope.cameraPreview = document.getElementById("camera-preview");
    $scope.sendButton = document.getElementById("send-video");
    $scope.closeButton = document.getElementById("close-video");
    $scope.sendButton.disabled = true;
    $scope.closeButton.disabled = true;
    $scope.cameraPreview.poster = 'assets/imgs/Logo Large.png';
    var recordAudio, recordVideo;
    $scope.startRec =function(){
        startRecording.disabled = true;
        navigator.getUserMedia({
            audio: true,
            video: true
        }, function(stream) {
             $scope.mediaStream = stream;
            recordAudio = RecordRTC(stream, {
                onAudioProcessStarted: function() {
                    recordVideoSeparately && recordVideo.startRecording();
                    $scope.cameraPreview.src = window.URL.createObjectURL(stream);
                    $scope.cameraPreview.play();
                    $scope.cameraPreview.muted = true;
                    $scope.cameraPreview.controls = false;
                }
            });
            recordVideo = RecordRTC(stream,{
                type: 'video'
            });
            recordAudio.startRecording();
            stopRecording.disabled = false;
        }, function(error) {
            alert(JSON.stringify(error));
        });
    };
    $scope.stopRec = function(){
        startRecording.disabled = true;
        stopRecording.disabled = true;
        recordVideoSeparately && recordAudio.stopRecording(function() {
            recordVideo.stopRecording(function() {
                recordAudio.getDataURL(function(audioDataURL) {
                    recordVideo.getDataURL(function(videoDataURL) {
                        var files = {
                            audio: {
                                type: recordAudio.getBlob().type || 'audio/wav',
                                dataURL: audioDataURL
                            },
                            video: {
                                type: recordVideo.getBlob().type || 'video/webm',
                                dataURL: videoDataURL
                            },
                            fileName:uuid
                        };
                        Socket.emit('videoMessage', files);
                         if ($scope.mediaStream) MediaStream.active
                    });
                });
                $scope.cameraPreview.src = '';
                $scope.cameraPreview.poster = 'assets/imgs/spinner.gif';
            });
        });
    };
    Socket.on('merged', function(fileName) {
        $scope.sendButton.disabled = false;
        $scope.closeButton.disabled = false;
        $scope.flName=fileName;
        $scope.cameraPreview.src = 'uploadedfiles/' +fileName
        $scope.cameraPreview.play();
        $scope.cameraPreview.poster = 'assets/imgs/Logo Large.png';
        $scope.cameraPreview.muted = false;
        $scope.cameraPreview.controls = true;
    });
    Socket.on('ffmpeg-error', function(error) {
        alert(error);
    });
    // Socket.on('vdoMsgAlert', function(data) {
    //     alert(data.fileName);
    // });
    $scope.closeRec = function(){
        $('#videoMessage').modal('hide');
        $scope.cameraPreview.src = '';
        $scope.cameraPreview.muted = true;
        $scope.cameraPreview.controls = false;
            if($scope.mediaStream.active) {
                 $scope.mediaStream.stop();
            }
    }
    $scope.sendRec = function(fName){
        $('#videoMessage').modal('hide');
        $scope.cameraPreview.src = '';
        $scope.cameraPreview.muted = true;
        $scope.cameraPreview.controls = false;
            if($scope.mediaStream.active) {
             $scope.mediaStream.stop();
            }
        var currentdate = new Date();     
        var date_time = currentdate.getDate() + "/"
                  + (currentdate.getMonth()+1)  + "/" 
                  + currentdate.getFullYear() + " @ "  
                  + currentdate.getHours() + ":"  
                  + currentdate.getMinutes() + ":" 
                  + currentdate.getSeconds();   
        var videoMsg = {
                            fileName:fName,
                            uploadedUser:UserDetails.user._id,
                            sharedUser:sessionStorage['chatId'],
                            date:date_time
                        } 
         Socket.emit('videoMessages',videoMsg);
         Socket.on('vdoMsgResponse', function(data) {
         var templat='  &nbsp;<a href="/#!/fileshare">'+  data.fileName +'</a>&nbsp Uploaded<br><video  src="uploadedfiles/'+ data.fileName +'"  width="450" height="240"  controls ></video>';            
          var newMsg = {
                    sender : UserDetails.user._id,
                    receiver : sessionStorage['chatId'],
                    senderName : UserDetails.user.username,
                    date : new Date(),
                    message : templat,
                    show : true,
                    fileid:data._id
                }
             $scope.messageText = "";
            if($scope.messages.length >=1)
            if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == newMsg.sender)&&(new Date(newMsg.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                newMsg.show = false;
            if($scope.messages.length>=1){
            if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                $scope.messages[$scope.messages.length-1].msgs.push(newMsg);
                Socket.emit('sendMsg',newMsg);
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            $scope.count.push([]);
            var alert = {
                    title :  "Sucess",
                    content: "Video Message Send Sucessfully"
                }
            $scope.showAlert(alert);
        });
    }
/*----------------------------------------------------------------------*/
//-------------------------------File Share-------------------------------------------------      
        // $scope.showAlert = function(alert){
        //     $mdDialog.show(
        //         $mdDialog.alert()
        //         .parent(angular.element(document.querySelector('#popupContainer')))
        //         .clickOutsideToClose(true)
        //         .title(alert.title)
        //         .content(alert.content)
        //         .ariaLabel('Alert Dialog')
        //         .ok('Ok!')
        //     );
        // }
        $scope.duplicte = 0;
        $scope.setFile = function(element){
            $scope.file = element.files[0];
            $scope.size = element.files[0].size;
            if($scope.size > 100000000){
                var flg=0;
            }else{
                var flg=1;
            }
            $scope.flname = $scope.file.name;
            $scope.exten = $scope.flname.split('.').pop();
            $scope.withoutExte = $scope.flname.split('.');
            var filetypes=["ppt","pptx","doc","docx","xls","xlsx","pdf","mp4","mp3","webm","wav","jpg","jpeg","png","gif","mkv","zip","rar","js","txt","css","json"];
            var flag ;
            if (filetypes.indexOf($scope.exten) > -1){
                flag=0
            }else{
                flag=-1
            }
            if(flag == 0 && flg == 1){
                if(sessionStorage['chatId'] != undefined ){ 
                    Socket.emit('recevierDetails',sessionStorage['chatId']);
                    Socket.emit("getFileNames",$scope.usrid);
                }else{
                    Socket.emit("getFileNames",$scope.usrid);
                    Socket.emit('recevierDetails',$scope.usrid);
                }
                Socket.on('fileNameList',function(data){
                    for(var i = 0 ; i < data.length;i++){
                        if(data[i] == $scope.usrid+'_'+$scope.flname || data[i] == $scope.usrid+'_'+$scope.withoutExte[0]+'.pdf' ){
                            $scope.duplicte = 1;
                        }
                    }
                })
                Socket.on('recevierDetailsResult',function(data){
                    $scope.usrName  = data.fullName;
                    $scope.mailID   = data.email;
                    $scope.sharedID = data._id;
                    $scope.$apply();
                });
                $('#fileDetails').modal('show');
            }else if(flag == -1){
                var alert = {
                    title : "Invalid Extention",
                    content: "Not a valid Extention"
                }
                 $scope.showAlert(alert);
            }else if(flg == 0){
                var alert = {
                    title : "Size Limit",
                    content: "Size Limit Exceeded"
                }
                 $scope.showAlert(alert);
            }    
        }
        $scope.uploadFile=function(){
            $('#upld').attr('disabled','disabled');
            var currentdate = new Date(); 
            var date_time = currentdate.getDate() + "/"
                      + (currentdate.getMonth()+1)  + "/" 
                      + currentdate.getFullYear() + " @ "  
                      + currentdate.getHours() + ":"  
                      + currentdate.getMinutes() + ":" 
                      + currentdate.getSeconds();
            Socket.emit('fileDetails',$scope.file);
            var fileDetails = $scope.file;
            var comments = $scope.commentText;
            $('div.progress').show();
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append('files', $scope.file);
            formData.append('userId',$scope.usrid );
            formData.append('datetime',date_time );
            formData.append('shareduserID',$scope.sharedID );
            formData.append('comments',$scope.commentText);
            formData.append('uploadext',$scope.exten);
            formData.append('duplicteStatus',$scope.duplicte);
            xhr.open('post', '/uploadFiles', true);
            xhr.upload.onprogress = function(e) {
                if (e.lengthComputable) {
                     var percentage = (e.loaded / e.total) * 100;
                             $('div.progress div').css('width', percentage.toFixed(0) + '%');
                             $('div.progress div').html(percentage.toFixed(0) + '%');
                }
            };
            xhr.onload = function(){
                    $('div.progress div').css('width','0%');
                    $('div.progress').hide();
                    $('#fileDetails').modal('hide');
                    $('#upld').removeAttr('disabled');
              var resp = xhr.responseText;
              var respArray = resp.split(',');
              if($scope.exten == "jpg" || $scope.exten == "jpeg" || $scope.exten == "gif" ||  $scope.exten == "png"){
                 var template='  &nbsp;<a href="/#!/fileshare">'+ respArray[2] +'</a>&nbsp Uploaded<br><img  src="uploadedfiles/'+respArray[2]+'"  width="300" height="200" />'
                }else if($scope.exten == "mp4" || $scope.exten == "webm" ||  $scope.exten == "mkv"  ||  $scope.exten == "wav"){
                        var template='  &nbsp;<a href="/#!/fileshare">'+ respArray[2] +'</a>&nbsp Uploaded<br><video  src="uploadedfiles/'+respArray[2]+'"  width="450" height="240"  controls ></video>'
                }else if($scope.exten == "mp3"){
                        var template='  &nbsp;<a href="/#!/fileshare">'+ respArray[2] +'</a>&nbsp Uploaded<br><audio  src="uploadedfiles/'+respArray[2]+'"  controls ></<audio>'
                } else if($scope.exten == "pptx" ||  $scope.exten == "ppt" || $scope.exten == "docx" || $scope.exten == "doc" ||  $scope.exten == "xls" || $scope.exten == "xlsx"  ||$scope.exten == "js" || $scope.exten == "json" || $scope.exten == "css" || $scope.exten == "txt" || $scope.exten == "pdf"){
                        var template='  &nbsp;<a href="/#!/fileshare">'+ respArray[2] +'</a>&nbsp Uploaded<br><embed  src="uploadedfiles/'+respArray[2]+'"  width="500" height="200" />'
                }else{
                        var template='  &nbsp;<a href="uploadedfiles/'+respArray[2]+'">'+ respArray[2] +'</a>&nbsp Uploaded<br>' 
                }
              var newMsg = {
                    sender : UserDetails.user._id,
                    receiver : sessionStorage['chatId'],
                    senderName : UserDetails.user.username,
                    date : new Date(),
                    message : template,
                    show : true,
                    fileid:respArray[4]
                }
             $scope.messageText = "";
            if($scope.messages.length >=1)
            if(($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].sender == newMsg.sender)&&(new Date(newMsg.date).getTime()-new Date($scope.messages[$scope.messages.length-1].msgs[$scope.messages[$scope.messages.length-1].msgs.length-1].date).getTime()<=(60*1000)))
                newMsg.show = false;
            if($scope.messages.length>=1){
            if($scope.messages[$scope.messages.length-1].dayDividerText == "Today"){
                $scope.messages[$scope.messages.length-1].msgs.push(newMsg);
                Socket.emit('sendMsg',newMsg);
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            }else{
                var tempMessages = [];
                tempMessages.push(newMsg);
                var msgPerDay = {
                    dayDividerText : "Today",
                    msgs : tempMessages
                }
                $scope.messages.push(msgPerDay);
                $scope.count.push([]);
                Socket.emit('sendMsg',newMsg);
            }
            $scope.count.push([]);
            var alert = {
                    title :  respArray[3],
                    content: respArray[0]
                }
             $scope.showAlert(alert);
            };
            xhr.send(formData);
            $scope.commentText="";
            $scope.duplicte = 0;
            $("#upload-file").val('');
        }
        $scope.$on('$destroy', function() {
            console.log("destroys")
            $scope.videoInProgress = false;
            sessionStorage['videoInProgress'] = false;
            $scope.messageListStyle = {
                                'max-height':'100%'
                            }
            if (webRtcPeer){
                webRtcPeer.dispose();
                webRtcPeer = null;
                    var message = {
                        id : 'stop',
                        from: UserDetails.user._id
                    }
                $scope.sendMessage(message);
            }
        })
}])