angular.module('chat').directive('scrollBottom', function ($timeout) {
  return {
    scope: {
      scrollBottom: "="
    },
    link: function (scope, element) {
      scope.$watchCollection('scrollBottom', function (newValue) {
        if (newValue)
        { $timeout(function(){
            $(element).scrollTop($(element)[0].scrollHeight);
        },100,false)
        }
      });
    }
  }
})
.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return { 'h': w.height(), 'w': w.width() };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;
            // console.log(scope.windowWidth);
            scope.style = function () {
                return { 
                    'height': (newValue.h - 160) + 'px' 
                };
            };
            scope.profEditStyle = function() {
                return {
                    'height': (newValue.h - 55) + 'px'
                }
            }
            scope.sidenavListStyle = function(){
                return {
                    'height': (newValue.h - 145) + 'px'
                }
            }
            scope.fullPageStyle = function(){
                return {
                    'height': (newValue.h - 100) + 'px'
                }
            }
            scope.viewCirclePageStyle = function(){
                return {
                    'height': (newValue.h - 205) + 'px'
                }
            }
            scope.createCirclePageStyle = function(){
                return {
                    'height': (newValue.h - 250) + 'px'
                }
            }
        }, true);
        w.bind('resize', function () {
            scope.$apply();
        });
    }
})
